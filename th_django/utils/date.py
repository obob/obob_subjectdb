# Copyright (c) 2016-2025, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project,
# see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.

import datetime

import six
from django.utils import formats
from django.utils.encoding import force_str


def parse_date(date_str):
    unicode_value = force_str(date_str, strings_only=True)
    if isinstance(unicode_value, six.text_type):
        date_str = unicode_value.strip()
    # If unicode, try to strptime against each input format.
    if isinstance(date_str, six.text_type):
        for format in formats.get_format_lazy('DATE_INPUT_FORMATS'):
            try:
                return datetime.datetime.strptime(date_str, format).date()
            except (ValueError, TypeError):
                continue

    raise ValueError
