# Copyright (c) 2016-2025, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project,
# see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.
from django.db import models
from django_currentuser.db.models import CurrentUserField

from th_django.modelfields.editablecuser import EditableCurrentUserField


class DateTimeAddedBy(models.Model):
    datetime_added = models.DateTimeField(auto_now_add=True, verbose_name='Added')
    added_by = CurrentUserField(
        on_update=False, related_name='%(app_label)s_%(class)s_added_by', on_delete=models.PROTECT
    )

    class Meta:
        abstract = True

    def added_by_name(self):
        return f'{self.added_by.last_name}, {self.added_by.first_name}'


class EditableDateTimeAddedBy(DateTimeAddedBy):
    class Meta:
        abstract = True

    added_by = EditableCurrentUserField(
        add_only=True, related_name='%(app_label)s_%(class)s_added_by', on_delete=models.PROTECT
    )


class DeleteToTrash(models.Model):
    deleted = models.BooleanField(default=False)
    datetime_deleted = models.DateTimeField(blank=True, null=True, default=None, verbose_name='Deleted')

    class Meta:
        abstract = True
