# Copyright (c) 2016-2025, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project,
# see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.

from django.apps import apps
from django.urls import resolve


class AppNamespaceInfo:
    def __init__(self, name, namespace, top_url, needs_permission=''):
        self.name = name
        self.namespace = namespace
        self.top_url = top_url
        self.needs_permission = needs_permission


class AppNamespaceInfoMixin:
    namespace_info = []


class AppList:
    def __init__(self, get_response):
        self.get_response = get_response

        self.apps_namespaces = list()
        for app_cfg in apps.get_app_configs():
            if isinstance(app_cfg, AppNamespaceInfoMixin):
                self.apps_namespaces.extend(app_cfg.namespace_info)

    def __call__(self, request):
        request.apps = dict()

        all_namespaces = [namespace.namespace for namespace in self.apps_namespaces]

        try:
            cur_namespace = (set(resolve(request.path).namespaces) & set(all_namespaces)).pop()
            request.apps['current_namespace'] = cur_namespace
        except KeyError:
            pass

        request.apps['all_apps'] = self.apps_namespaces.copy()

        response = self.get_response(request)

        return response
