# Copyright (c) 2016-2025, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project,
# see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.

from django.conf import settings
from django.db.models import ForeignKey
from django_currentuser.middleware import get_current_user


class EditableCurrentUserField(ForeignKey):
    def __init__(self, to_field=None, to=settings.AUTH_USER_MODEL, **kwargs):
        self.add_only = kwargs.pop('add_only', False)
        kwargs.update(
            {
                'editable': True,
                'null': True,
                'to': to,
                'to_field': to_field,
            }
        )
        super().__init__(**kwargs)

    def pre_save(self, model_instance, add):
        if add or not self.add_only:
            user = get_current_user()
            if user:
                setattr(model_instance, self.attname, user.pk)
                return user.pk

        return super().pre_save(model_instance, add)
