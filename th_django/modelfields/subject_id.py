# Copyright (c) 2016-2025, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project,
# see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.

import re

import django.db.models
from django.core.exceptions import ValidationError

import th_django.forms.fields


class SubjectID(django.db.models.CharField):
    description = 'Field for a subject id with checks'
    empty_strings_allowed = False
    default_error_messages = {
        'id_bad': 'The subject id is not correctly formated. It needs to be in the form of 19800908igdb'
    }

    id_regex = r'^[1-2][0-9]{7}[a-z]{4}$'

    def __init__(self, **kwargs):
        kwargs['max_length'] = 12
        super().__init__(**kwargs)
        self.fixed_length = 12

    def validate(self, value, model_instance):
        value = value.lower()
        if len(value) != self.fixed_length or not re.match(self.id_regex, value):
            raise ValidationError(self.error_messages['id_bad'], 'id_bad')
        super().validate(value, model_instance)

    def to_python(self, value):
        value = super().to_python(value)
        if isinstance(value, str):
            return value.lower()
        return value

    def formfield(self, **kwargs):
        defaults = {
            'form_class': th_django.forms.fields.SubjectID,
            'id_regex': self.id_regex.replace('[a-z]', '[a-zA-Z]'),
        }

        defaults.update(kwargs)
        return super().formfield(**defaults)

    @classmethod
    def generate_subject_id(cls, birthday, mother_first_name, mother_maiden_name):
        return '{}{}{}'.format(
            birthday.strftime('%Y%m%d'),
            mother_first_name[0:3:2].lower(),
            mother_maiden_name[0:3:2].lower(),
        )
