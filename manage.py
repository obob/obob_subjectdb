#!/usr/bin/env python

# Copyright (c) 2016-2025, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project,
# see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import warnings

warnings.filterwarnings('always', category=DeprecationWarning, module='subjectdb')
warnings.filterwarnings('always', category=DeprecationWarning, module='mridb')
warnings.filterwarnings('always', category=DeprecationWarning, module='th_bootstrap')
warnings.filterwarnings('always', category=DeprecationWarning, module='th_django')
warnings.filterwarnings('always', category=DeprecationWarning, module='user_manager')

if __name__ == '__main__':
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'obob_subjectdb.settings')

    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)
