import contextlib
import json
from pathlib import Path

import django.core.signing
from django.conf import settings
from django.core.exceptions import ValidationError
from django.http import FileResponse, JsonResponse
from django.views.generic import View
from django_auto_url import kwargs
from django_auto_url.mixins import AutoUrlMixin

import th_django.utils.fun2string


class UploadFile(View, AutoUrlMixin):
    http_method_names = ['post']
    upload_tmp = Path(settings.BOOTSTRAP_FILEINPUT['UPLOAD_TEMP'])

    def post(self, request):
        upload_tmp = self.upload_tmp
        salt = request.POST['salt']
        signer = django.core.signing.Signer(salt=salt)
        file_extensions = json.loads(signer.unsign(request.POST['allowed_fileextensions']))
        uuid = signer.unsign(request.POST['uuid'])
        single_file_check_function = None
        with contextlib.suppress(KeyError):
            single_file_check_function = th_django.utils.fun2string.string2fun(
                signer.unsign(request.POST['single_file_check_function'])
            )

        if not upload_tmp.is_dir():
            with contextlib.suppress(FileExistsError):
                upload_tmp.mkdir(parents=True)

        tmpdir = upload_tmp / uuid
        if not tmpdir.is_dir():
            with contextlib.suppress(FileExistsError):
                tmpdir.mkdir()

        response = {'files': []}
        for file in request.FILES.values():
            if file_extensions and Path(file.name).suffix[1:] not in file_extensions:
                raise ValidationError('Not allowed file extension')

            if single_file_check_function:
                rval = single_file_check_function(file)
                if rval:
                    response['error'] = rval
                    return JsonResponse(response)

            with (tmpdir / file.name).open('wb') as fid:
                file.seek(0)
                fid.write(file.read())
            response['files'].append(file.name)

        return JsonResponse(response)


class PostUploadFile(View, AutoUrlMixin):
    http_method_names = ['post']
    upload_tmp = Path(settings.BOOTSTRAP_FILEINPUT['UPLOAD_TEMP'])

    def post(self, request):
        salt = request.POST['salt']
        signer = django.core.signing.Signer(salt=salt)
        uuid = signer.unsign(request.POST['uuid'])

        multi_check_function = None
        rval = {'status': True, 'message': ''}

        if not request.POST['files']:
            rval['status'] = False
            rval['message'] = 'Did not get any files.'

            return rval

        all_files = request.POST['files'].split(';')
        all_files.pop()

        tmp_path = self.upload_tmp / uuid
        for cur_file in tmp_path.iterdir():
            if cur_file.name not in all_files:
                cur_file.unlink()

        with contextlib.suppress(KeyError):
            multi_check_function = th_django.utils.fun2string.string2fun(
                signer.unsign(request.POST['multicheck_function'])
            )

        if multi_check_function:
            rval = multi_check_function(uuid, str(self.upload_tmp), all_files)
            del rval['file_type']

        return JsonResponse(rval)


class DownloadFile(View, AutoUrlMixin):
    http_method_names = ['get', 'post']
    url_kwargs = [kwargs.string('filename'), kwargs.string('salt')]

    def dispatch(self, request, filename, salt):
        filename = django.core.signing.Signer(salt=salt).unsign(filename)
        file_path = Path(settings.PERSISTENT_DIR) / filename

        response = FileResponse(file_path.open('rb'))
        response['Content-Disposition'] = f'attachment; filename={file_path.name}'

        return response
