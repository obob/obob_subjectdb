import shutil
import zipfile
from pathlib import Path

import django.conf
import django.core.exceptions
import django.forms
from django.conf import settings

import th_bootstrap.widgets.ajaxbootstrap_fileinput


class AjaxBootstrapFileInput(django.forms.Field):
    default_error_messages = {'required': 'Please upload your File before submitting'}

    widget = th_bootstrap.widgets.ajaxbootstrap_fileinput.AjaxBootstrapFileInput  # noqa

    def __init__(self, modelfield_instance, *args, **kwargs):
        self.modelfield_instance = modelfield_instance
        self.file_storage = self.modelfield_instance.file_storage
        super().__init__(*args, **kwargs)
        self.widget.modelfield_instance = modelfield_instance

    def to_python(self, value):
        if not value['data']:
            return self.initial.file_name
        self.uuid = value['uuid']
        files_string = value['data']

        tmpdir = self._get_tmpdir()
        tmpfiles = files_string.split(';')

        final_storage = Path(settings.PERSISTENT_DIR) / self.file_storage / self.uuid

        if (final_storage / tmpfiles[0]).exists():
            return str(final_storage / tmpfiles[0])

        tmpfiles.pop()

        if (not self.modelfield_instance.allow_multiple_files) and len(tmpfiles) > 1:
            raise django.core.exceptions.ValidationError(
                'Multiple files received in Single File Widget', code='multiplefiles'
            )

        if not self.modelfield_instance.allow_multiple_files:
            src_file = tmpdir / tmpfiles[0]
            dst_file = final_storage / tmpfiles[0]
        else:
            src_file = tmpdir
            dst_file = final_storage.with_suffix('.zip')

        self.process_pre_check(value, self.uuid, tmpfiles)

        if self.modelfield_instance.single_file_check_function:
            with src_file.open('rb') as file_object:
                rval = self.modelfield_instance.single_file_check_function(file_object)
                if rval is not None:
                    raise django.core.exceptions.ValidationError('File validation failed', code='filevalidationfailed')

        if self.modelfield_instance.multi_file_check_function:
            rval = self.modelfield_instance.multi_file_check_function(self.uuid, str(tmpdir.parent), tmpfiles)
            if not rval['status']:
                raise django.core.exceptions.ValidationError('File validation failed', code='filevalidationfailed')

            self.modelfield_instance.multi_check_result = rval

        self.process_post_check(value, self.uuid, tmpfiles)

        if not (Path(settings.PERSISTENT_DIR) / self.file_storage).is_dir():
            (Path(settings.PERSISTENT_DIR) / self.file_storage).mkdir()

        if not final_storage.is_dir() and not self.modelfield_instance.allow_multiple_files:
            final_storage.mkdir()

        if not self.modelfield_instance.allow_multiple_files:
            shutil.move(str(src_file), str(dst_file))
            shutil.rmtree(tmpdir)
            return str(final_storage / tmpfiles[0])
        else:
            with dst_file.open('wb') as dst_opened, zipfile.ZipFile(dst_opened, 'w', zipfile.ZIP_DEFLATED) as dst_zip:
                for cur_file in tmpfiles:
                    dst_zip.write(str(src_file / cur_file), cur_file)
            shutil.rmtree(tmpdir)
            return f'{self.uuid}.zip'

    def process_pre_check(self, value, uuid, file_list):
        pass

    def process_post_check(self, value, uuid, file_list):
        pass

    def _get_tmpdir(self):
        return Path(self.modelfield_instance.upload_view.upload_tmp) / self.uuid

    def prepare_value(self, value):
        return value
