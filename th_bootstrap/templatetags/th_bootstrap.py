# Copyright (c) 2016-2025, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project,
# see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.

import classytags.arguments
import classytags.core
import django.template.loader
from django import template
from django.utils.html import format_html
from django.utils.safestring import mark_safe

register = template.Library()


@register.inclusion_tag('modal.html')
def create_modal(modal_id, title, content, ok_text, close_text, ok_ref='#', ok_button_class='btn-primary'):
    return {
        'modal_id': modal_id,
        'title': title,
        'content': content,
        'ok_text': ok_text,
        'close_text': close_text,
        'ok_ref': ok_ref,
        'ok_button_class': ok_button_class,
    }


@register.inclusion_tag('modal.html')
def create_confirm_delete(modal_id, pre_question, post_question='?'):
    return {
        'modal_id': modal_id,
        'title': 'Confirm Delete',
        'content': format_html('<p>{} <span id=delete_content></span>{}</p>', pre_question, post_question),
        'okText': 'Yes',
        'closeText': 'No',
        'formMethod': 'post',
    }


@register.inclusion_tag('delete_button.html')
def create_delete_button(modal_id, subject='', delete_url='#', button_class='btn-default', button_size=''):
    return {
        'modal_id': modal_id,
        'subject': subject,
        'delete_url': delete_url,
        'button_class': button_class,
        'button_size': button_size,
    }


@register.inclusion_tag('bootstrap_fileinput_downloadbutton.html')
def bootstrap_fileinput_downloadbutton(url, button_size=None, button_class=None, button_id=None, button_name=None):
    return {
        'url': url,
        'button_size': button_size,
        'button_class': button_class,
        'button_id': button_id,
        'button_name': button_name,
    }


@register.simple_tag
def object_to_table(table):
    return mark_safe(table.render())


@register.inclusion_tag('detail_fields.html', takes_context=True)
def render_detail_fields(context):
    return context


@register.tag('dropdown')
class DropDown(classytags.core.Tag):
    name = 'dropdown'
    options = classytags.core.Options(
        classytags.arguments.MultiKeywordArgument('kwargs'),
        blocks=[('enddropdown', 'nodelist')],
    )

    start_template_name = 'dropdown_start.html'

    def render_tag(self, context, kwargs, nodelist):
        start_template = django.template.loader.get_template(self.start_template_name)
        start_rendered = start_template.render(kwargs)

        end_rendered = '</ul></li>'
        return start_rendered + nodelist.render(context) + end_rendered


@register.tag('sub_dropdown')
class SubDropDown(classytags.core.Tag):
    name = 'sub_dropdown'
    options = classytags.core.Options(
        classytags.arguments.MultiKeywordArgument('kwargs'),
        blocks=[('endsub_dropdown', 'nodelist')],
    )

    start_template_name = 'subdropdown_start.html'

    def render_tag(self, context, kwargs, nodelist):
        start_template = django.template.loader.get_template(self.start_template_name)
        start_rendered = start_template.render(kwargs)

        end_rendered = '</ul></li>'
        return start_rendered + nodelist.render(context) + end_rendered


@register.inclusion_tag('render_inline_forms.html', takes_context=True)
def render_inline_forms(context, inlines='inlines'):
    return {'inlines': context.get(inlines, None)}
