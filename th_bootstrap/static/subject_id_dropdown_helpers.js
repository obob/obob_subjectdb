/*
 * Copyright (c) 2016-2025, Thomas Hartmann
*
* This file is part of the OBOB Subject Database Project, see: https://gitlab.com/obob/obob_subjectdb/
*
*    obob_subjectdb is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    obob_subjectdb is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * Created by th on 03.05.16.
 */

import $ from 'jquery';
import sprintf from 'sprintf-js';
import 'bootstrap-validator';
import '@claviska/jquery-dropdown/jquery.dropdown';

export default function(dropdownid, target, submit_form) {
    function calc_id(_mother_firstname, _mother_maidenname, _participant_bday) {
        var code='';
        var bday = new Date(_participant_bday);
        code = sprintf.sprintf("%04d%02d%02d", bday.getFullYear(), bday.getMonth()+1, bday.getDate());
        code += sprintf.sprintf("%s%s", _mother_firstname.charAt(0), _mother_firstname.charAt(2));
        code += sprintf.sprintf("%s%s", _mother_maidenname.charAt(0), _mother_maidenname.charAt(2));


        return code.toLowerCase();
    }

    var mother_firstname = $(sprintf.sprintf('#%s #motherFirstName', dropdownid));
    var mother_maidenname = $(sprintf.sprintf('#%s #motherMaidenName', dropdownid));
    var participant_bday = $(sprintf.sprintf('#%s #participantBirthday', dropdownid));
    var participant_codeform = $(sprintf.sprintf('#%s #participantCodeForm', dropdownid));
    var participant_code = $(sprintf.sprintf('#%s #participantCode', dropdownid));
    var input_for_id = $(sprintf.sprintf('#%s .input-for-id', dropdownid));


    // hack the bootstrap-validator
    var validator_constructor = $.fn.validator.Constructor;
    validator_constructor.INPUT_SELECTOR = ':input:not([type="submit"], button):enabled';
    $.fn.validator.Constructor = validator_constructor;

    participant_codeform.validator();

    input_for_id.on('keyup change', function () {
        participant_code.val(calc_id(mother_firstname.val(), mother_maidenname.val(), participant_bday.val()));
    });

    participant_codeform.submit(function(e) {
        e.preventDefault();
        target.val(participant_code.val());
        target.change();
        if(submit_form) {
            target.closest('form').submit();
        }
        target.jqDropdown('hide');
        $('#form').validator('validate');
    });
};