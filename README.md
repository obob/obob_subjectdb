# OBOB Subject Database
The OBOB Subject Database is an in-house project of the OBOB Team at the University of Salzburg to keep track of the Subjects and MRIs.
As I develop it entirely for internal usage, documentations will be sparse. However, if you find it useful, you use it under the terms of the GPL3.
