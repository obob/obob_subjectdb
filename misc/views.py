# Copyright (c) 2016-2025, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project,
# see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.

import subprocess
from pathlib import Path

import django_auto_url.mixins
import git
import orjson
from django.conf import settings
from django.views.generic.base import TemplateView

from obob_subjectdb.mixins import MainHeaderMixin


class About(django_auto_url.mixins.AutoUrlMixin, TemplateView):
    template_name = 'about.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['git'] = git.Repo(settings.BASE_DIR).active_branch.commit
        pixi_json = (
            subprocess.run('./.pixi/envs/default/bin/pixi list -x --json', shell=True, capture_output=True, check=False)
            .stdout.decode('utf-8')
            .strip()
        )
        pixi_decoded = orjson.loads(pixi_json)
        context['python_packages'] = {pkg['name']: 'UNKNOWN' for pkg in pixi_decoded}

        with Path(settings.JS_PACKAGES_JSON_FILE).open() as json_file:
            context['js_packages'] = orjson.loads(json_file.read())

        return context


class Help(MainHeaderMixin, django_auto_url.mixins.AutoUrlMixin, TemplateView):
    template_name = 'help.html'
    main_header_html = 'Help'
