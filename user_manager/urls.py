# Copyright (c) 2016-2025, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project,
# see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.

from django_auto_url.urls import get_urls_from_module

import user_manager.views.login
import user_manager.views.manage
import user_manager.views.permission_errors

app_name = 'user_manager'

urlpatterns = (
    get_urls_from_module(user_manager.views.login)
    + get_urls_from_module(user_manager.views.manage)
    + get_urls_from_module(user_manager.views.permission_errors)
)
