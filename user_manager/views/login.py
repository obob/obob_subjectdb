# Copyright (c) 2016-2025, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project,
# see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.

import django_auto_url.mixins
from django.contrib import auth, messages
from django.contrib.auth import views as auth_views
from django.http import HttpResponseRedirect
from django.views.generic import View


class Login(View, django_auto_url.mixins.AutoUrlMixin):
    http_method_names = ['post']

    def post(self, request, *args, **kwargs):
        user = auth.authenticate(request, username=request.POST['username'], password=request.POST['password'])
        if user is not None and user.extendeduser.can_login:
            auth.login(request, user)
            messages.success(request, 'Login successful.')
        else:
            messages.error(request, 'Login failed.')

        return HttpResponseRedirect(request.POST['next'])


class Logout(View, django_auto_url.mixins.AutoUrlMixin):
    def dispatch(self, request, *args, **kwargs):
        auth.logout(request)
        messages.info(request, 'Logged out')
        return HttpResponseRedirect(request.GET['next'])


class LoginForm(auth_views.LoginView, django_auto_url.mixins.AutoUrlMixin):
    template_name = 'base_form.html'
