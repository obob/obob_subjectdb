# Copyright (c) 2016-2025, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project,
# see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.
import datetime
import distutils.util

import django.shortcuts
from django.contrib import auth
from django.core.exceptions import ObjectDoesNotExist
from django.views.generic import UpdateView, View
from django_auto_url import kwargs
from django_auto_url.urls import reverse_classname, reverse_classname_lazy
from django_filters.views import FilterView
from django_tables2 import SingleTableMixin

import user_manager.authentication
import user_manager.modelforms
from user_manager.views import tables
from user_manager.views.filter import UserFilter
from user_manager.views.private_mixins import DefaultMixin


class EditUser(DefaultMixin, UpdateView):
    form_class = user_manager.modelforms.CreateOrEditUser
    success_url = reverse_classname_lazy('user_manager.views.manage.ListUsers')
    slug_url_kwarg = 'user_id'
    url_kwargs = [kwargs.string('user_id')]

    extra_context = {'extra_js': ['usermanager/editcreate_users.js']}

    def form_valid(self, form):
        model = auth.get_user_model()
        try:
            user = model.objects.get(username=form.instance.username)
            user.is_active = True
            form.instance = user
        except model.DoesNotExist:
            ldap_instance = user_manager.authentication.LDAPBackend()
            user = ldap_instance.populate_user(form.instance.username)
            form.instance.first_name = user.first_name
            form.instance.last_name = user.last_name
            form.instance.email = user.email
            form.instance.is_active = True
            form.instance.set_unusable_password()
            user.delete()
        return super().form_valid(form)

    def get_object(self, queryset=None):
        user_model = auth.get_user_model()

        if self.slug_url_kwarg in self.kwargs:
            return user_model.objects.get(pk=self.kwargs[self.slug_url_kwarg])

        if 'username' in self.request.POST:
            try:
                return user_model.objects.all().filter(username__exact=self.request.POST['username']).get()
            except ObjectDoesNotExist:
                return None
        else:
            return None

    def get_permission_required(self):
        if self.slug_field in self.kwargs:
            return ('auth.change_user',)
        else:
            return ('auth.add_user',)

    def get_context_object_name(self, obj):
        return 'edit_user'

    def get_main_header(self):
        return f'Edit User {self.object.username}'


class CreateUser(EditUser):
    url_ignore_pk = True
    url_kwargs = None

    def get_main_header(self):
        return 'Add User'


class ListUsers(DefaultMixin, SingleTableMixin, FilterView):
    model = auth.get_user_model()
    fields = ['username', 'first_name', 'last_name', 'email', 'is_superuser']
    template_name = 'usermanager/list_users.html'
    permission_required = 'user_manager.view_user'
    table_class = tables.ListUsers
    filterset_class = UserFilter

    extra_context = {'extra_js': ['usermanager/list_users.js']}

    def get_queryset(self):
        filter1 = {'is_active': False}
        filter2 = {
            'extendeduser__expiration_date__isnull': False,
            'extendeduser__expiration_date__lt': datetime.date.today(),
            'is_superuser': False,
        }
        qs = self.model.objects.all()

        if not distutils.util.strtobool(self.request.GET.get('show_inactive', 'False')):
            qs = qs.exclude(**filter1)
            qs = qs.exclude(**filter2)
        else:
            qs = qs.filter(**filter1) | qs.filter(**filter2)

        return qs

    def get_context_data(self, **kwargs):
        kwargs['show_inactive'] = distutils.util.strtobool(self.request.GET.get('show_inactive', 'False'))
        return super().get_context_data(**kwargs)

    def get_table_kwargs(self):
        kwargs = super().get_table_kwargs()
        kwargs['show_inactive'] = distutils.util.strtobool(self.request.GET.get('show_inactive', 'False'))

        return kwargs

    def get_main_header(self):
        if distutils.util.strtobool(self.request.GET.get('show_inactive', 'False')):
            return 'Inactive Users'
        else:
            return 'Active Users'


class DeleteUser(DefaultMixin, View):
    http_method_names = ['post', 'delete']
    permission_required = 'auth.delete_user'
    url_kwargs = [kwargs.int('user_id')]

    def post(self, request, user_id):
        model = auth.get_user_model()
        user = model.objects.get(id=user_id)
        user.is_active = False
        user.save()

        return django.shortcuts.redirect(reverse_classname(ListUsers))
