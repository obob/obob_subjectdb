# Copyright (c) 2016-2025, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project,
# see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.

from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django_auto_url.urls import reverse_classname, reverse_classname_lazy


class PermissionRequiredMixin(PermissionRequiredMixin):
    redirect_field_name = ''
    must_be_owner = False

    def get_login_url(self):
        import user_manager.views.permission_errors

        if self.request.user.is_authenticated:
            return reverse_classname(user_manager.views.permission_errors.PermissionDenied)
        else:
            return reverse_classname(user_manager.views.permission_errors.NotLoggedIn)

    def get_redirect_field_name(self):
        if self.request.user.is_authenticated:
            return ''
        else:
            return 'next'

    def is_owner(self):
        object = self.get_object()
        if object:
            return object.added_by == self.request.user or self.request.user.is_superuser
        else:
            return True

    def has_permission(self):
        if not self.request.user.is_authenticated:
            return False

        if self.get_permission_required() is None:
            return True

        perm = super().has_permission()

        if self.must_be_owner:
            perm = perm and self.is_owner()

        return perm

    def get_permission_required(self):
        if self.permission_required is None:
            return None
        else:
            return super().get_permission_required()


class LoginRequiredMixin(LoginRequiredMixin):
    login_url = reverse_classname_lazy('user_manager.views.permission_errors.NotLoggedIn')
