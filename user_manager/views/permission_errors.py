# Copyright (c) 2016-2025, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project,
# see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.

from django.views.generic import TemplateView
from django_auto_url.mixins import AutoUrlMixin

from obob_subjectdb.mixins import MainHeaderMixin


class NotLoggedIn(TemplateView, MainHeaderMixin, AutoUrlMixin):
    template_name = 'base.html'
    main_header_html = 'You are not logged in. Please use the Login menu at the top. '

    def get_context_data(self, **kwargs):
        if 'next' in self.request.GET:
            kwargs['next'] = self.request.GET['next']
        return super().get_context_data(**kwargs)


class PermissionDenied(TemplateView, AutoUrlMixin):
    template_name = 'usermanager/permission_denied.html'

    def post(self, *args, **kwargs):
        return self.get(*args, **kwargs)
