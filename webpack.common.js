/* eslint-disable */
const webpack = require('webpack')
const path = require('path');
const glob = require('glob');
const { execSync } = require('child_process');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const BundleTracker = require('webpack-bundle-tracker');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

const src_folders = [
    'obob_subjectdb',
    'mridb',
    'subjectdb',
    'user_manager'
]

const entries = {};

src_folders.forEach((f) => {
    const all_files = glob.sync(path.join(f, '**', 'src_assets', '+(js|css|scss)', '**', '*.+(js|css|scss)'));
    all_files.forEach((f_name) => {
        const key_parts = [];
        let was_ext = false;
        const f_name_parts = f_name.split(path.sep);
        const ext = path.extname(f_name).slice(1);

        if (! f_name_parts.includes('lib')) {
            f_name_parts.forEach((f_part) => {
                if (f_part === ext) {
                    was_ext = true;
                } else if (was_ext) {
                    key_parts.push(f_part)
                }
            });
            const key = key_parts.join('/');
            entries[key] = './' + f_name;
        }
    });
});

console.log(entries);

const python_site_packages = JSON.parse(
    execSync('python -c \'import site; print(site.getsitepackages())\'', {
        encoding: 'utf-8',
    })
        .replace(/'/g, '"')
);

module.exports = {
    entry: entries,
    output: {
        filename: "[name].js",
        chunkFilename: '[name].bundle.js',
        path: path.resolve(__dirname, 'webpack_assets', 'webpack_bundles')
    },
    plugins: [
        new CleanWebpackPlugin(),
        new webpack.ProvidePlugin({
            process: 'process/browser',
        }),
        new BundleTracker({filename: 'webpack-stats.json', path: './'}),
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            yl: ['dal/static/autocomplete_light/autocomplete_light', 'yl'],
        }),
        new MiniCssExtractPlugin(),
    ],
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                }
            },
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules/,
            },
            {
                test: RegExp('fonts/.*.(woff(2)?|ttf|eot|svg)$'),
                type: 'asset/resource',
            },
            {
                test: /\.(sa|sc|c)ss$/i,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    'postcss-loader',
                    'sass-loader',
                ],
            },
        ],
    },
    resolve: {
        modules: ['node_modules', ...python_site_packages],
    }
};
