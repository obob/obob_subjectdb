# Copyright (c) 2016-2025, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project,
# see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.
import contextlib

import django.views.generic.base
import django.views.generic.edit
import django_auto_url.mixins
import django_tables2.views
from django.core.exceptions import ImproperlyConfigured

import user_manager.views.mixins

from .main_header import MainHeaderMixin


class DefaultMixin(
    user_manager.views.mixins.PermissionRequiredMixin, MainHeaderMixin, django_auto_url.mixins.AutoUrlMixin
):
    permission_required = None

    def get_template_names(self):
        if not issubclass(self.__class__, django.views.generic.base.TemplateResponseMixin):
            return None

        template_list = []

        if self.template_name:
            template_list.append(self.template_name)

        with contextlib.suppress(ImproperlyConfigured):
            template_list.extend(super().get_template_names())

        if issubclass(self.__class__, django.views.generic.edit.FormMixin):
            template_list.append('base_form.html')

        if issubclass(self.__class__, django_tables2.views.TableMixinBase):
            template_list.append('base_table.html')

        return template_list
