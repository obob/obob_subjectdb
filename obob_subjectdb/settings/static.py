# Copyright (c) 2016-2025, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project,
# see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.

import os
import sys
from pathlib import Path
from uuid import uuid4

STATIC_URL = '/static/'

STATICFILES_DIRS = [
    Path(BASE_DIR, 'static'),  # noqa F821
    Path(BASE_DIR, 'webpack_assets'),  # noqa F821
]

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

STATIC_ROOT = Path(os.getenv('OBOB_SUBJECTDB_STATIC_DIR', Path(BASE_DIR, 'persistent_files', 'static')))  # noqa F821

if 'test' in sys.argv:
    STATIC_ROOT = Path(os.getenv('OBOB_SUBJECTDB_STATIC_DIR', Path(UNITTEST_DIR, str(uuid4()))))  # noqa F821

STATIC_ROOT.mkdir(parents=True, exist_ok=True)
