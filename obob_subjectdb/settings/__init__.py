# Copyright (c) 2016-2025, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project,
# see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.

from split_settings.tools import include, optional

include(
    'debug.py',
    'django.py',
    'email.py',
    'database.py',
    'bootstrap_fileinput.py',
    'language.py',
    'static.py',
    'requirejs.py',
    'ldap_auth.py',
    'security.py',
    'cron.py',
    'unittest.py',
    'misc.py',
    scope=globals(),
)

if False:
    INSTALLED_APPS = [
        'django.contrib.auth',
        'django.contrib.contenttypes',
        'django.contrib.sessions',
        'django.contrib.messages',
        'django.contrib.staticfiles',
        'dynamic_preferences',
        'mridb',
        'subjectdb',
        'bootstrap3',
        'th_bootstrap',
        'th_django',
        'misc',
        'user_manager',
        'django_crontab',
        'django_tables2',
        'jquery',
        'djangoformsetjs',
        'webpack_loader',
    ]
