# Copyright (c) 2016-2025, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project,
# see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.

import os

import ldap
from django_auth_ldap.config import LDAPSearch, LDAPSearchUnion  # noqa

ldap.set_option(ldap.OPT_X_TLS_REQUIRE_CERT, ldap.OPT_X_TLS_ALLOW)

AUTHENTICATION_BACKENDS = (
    'user_manager.authentication.LDAPBackend',
    'django.contrib.auth.backends.ModelBackend',
)

AUTH_LDAP_SERVER_URI = os.getenv('OBOB_SUBJECTDB_LDAP_SERVER', 'ldap://localhost:3890')
AUTH_LDAP_BIND_DN = os.getenv('OBOB_SUBJECTDB_LDAP_BIND_DN', 'cn=admin,dc=th-ht,dc=de')
AUTH_LDAP_BIND_PASSWORD = os.getenv('OBOB_SUBJECTDB_LDAP_BIND_PASSWORD', 'thht-admin')

AUTH_LDAP_USER_SEARCH = eval(
    os.getenv('OBOB_SUBJECTDB_LDAP_USER_SEARCH', 'LDAPSearch("dc=th-ht,dc=de", ldap.SCOPE_SUBTREE, "(uid=%(user)s)")')
)

AUTH_LDAP_USER_ATTR_MAP = eval(
    os.getenv('OBOB_SUBJECTDB_LDAP_USER_ATTR_MAP', "{'first_name': 'givenName', 'last_name': 'sn', 'email': 'mail'}")
)

AUTH_LDAP_START_TLS = eval(os.getenv('OBOB_SUBJECTDB_LDAP_START_TLS', 'False'))

LOGIN_REDIRECT_URL = '/'
