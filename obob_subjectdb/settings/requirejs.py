# Copyright (c) 2016-2025, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project,
# see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.

REQUIREJS_CONFIG_OLD = {
    'baseUrl': '/static',
    'nodeRequire': 'require',
    'shim': {
        'bootstrap': {'deps': ['jquery']},
        'bootstrap-validator': {'deps': ['bootstrap']},
        'bootstrap-table': {'deps': ['jquery', 'bootstrap']},
        'jquery-dropdown': {'deps': ['jquery', 'bootstrap']},
        'select2': {'deps': ['jquery']},
    },
    'paths': {
        'jquery': 'jquery/dist/jquery.min',
        'bootstrap': 'bootstrap/dist/js/bootstrap.min',
        'bootstrap-datepicker': 'bootstrap-datepicker/dist/js/bootstrap-datepicker',
        'bootstrap-datetime-picker': 'bootstrap-datetime-picker/js/bootstrap-datetimepicker.min',
        'bootstrap-fileinput': 'bootstrap-fileinput/js/fileinput',
        'js-cookie': 'js-cookie/src/js.cookie',
        'bootstrap-validator': 'bootstrap-validator/dist/validator.min',
        'bootstrap-table': 'bootstrap-table/dist/bootstrap-table',
        'sprintf-js': 'sprintf-js/src/sprintf',
        'jquery-dropdown': '@claviska/jquery-dropdown/jquery.dropdown',
        'url-params': 'url-params-helper/lib/url-params.min',
        'deepcopy': 'deepcopy/build/deepcopy.min',
        'select2': 'select2/dist/js/select2.full',
    },
}
