# Copyright (c) 2016-2025, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project,
# see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.

"""obob_subjectdb URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  re_path(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  re_path(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  re_path(r'^blog/', include('blog.urls'))
"""

from django.conf import settings
from django.urls import include, re_path
from django.views.generic.base import RedirectView

import obob_subjectdb.views.error_views
import user_manager.views.permission_errors

urlpatterns = [
    re_path(r'^mridb/', include('mridb.urls')),
    re_path(r'^subjectdb/', include('subjectdb.urls')),
    re_path(r'^user_manager/', include('user_manager.urls')),
    re_path(r'^$', RedirectView.as_view(url='mridb/')),
    re_path(r'^misc/', include('misc.urls')),
    re_path(r'th_bootstrap/', include('th_bootstrap.urls')),
]

if settings.DEBUG and settings.USE_DJANGO_DEBUG_TOOLBAR:
    import debug_toolbar

    urlpatterns = [
        re_path(r'^__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns

handler500 = obob_subjectdb.views.error_views.handler500
handler404 = obob_subjectdb.views.error_views.handler404
handler403 = user_manager.views.permission_errors.PermissionDenied.as_view()
