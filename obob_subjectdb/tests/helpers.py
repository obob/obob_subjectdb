# Copyright (c) 2016-2025, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project,
# see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.
from pathlib import Path

import polling
from django.contrib import auth
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC  # noqa N812

from th_django.test import SeleniumMixin


class UITestMixin(SeleniumMixin):
    def wait_click_and_wait(self, locator, needs_navbar=True):
        if needs_navbar:
            self.click_navbutton()

        super().wait_click_and_wait(locator)

    def click_navbutton(self):
        self.wait_until(EC.visibility_of_element_located((By.TAG_NAME, 'body')))
        navbar_toggle_button = self.selenium.find_element_by_id('navbar-button')
        if navbar_toggle_button.is_displayed():
            navbar_toggle_button.click()

    def wait_for_table(self, timeout=40):
        self.wait_until(EC.presence_of_element_located((By.CLASS_NAME, 'table')), timeout=timeout)

    def get_current_user(self):
        try:
            has_selenium = self.selenium is not None
        except ValueError:
            has_selenium = False

        if has_selenium:
            return auth.get_user_model().objects.get(username=self.find_element_by_text('Logout').text.split()[1])
        else:
            return auth.get_user_model().objects.get(id=1)

    def _wait_for_downloaded_file(self, fname):
        return polling.poll(lambda: Path(fname).open('rb'), ignore_exceptions=(IOError,), timeout=20, step=0.5)
