/*
 * Copyright (c) 2016-2025, Thomas Hartmann
 *
 * This file is part of the OBOB Subject Database Project, see: https://gitlab.com/obob/obob_subjectdb/
 *
 *    obob_subjectdb is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    obob_subjectdb is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.
 */

import 'jquery';
import 'djangoformsetjs/static/js/jquery.formset';

$('#formset').formset({
    animateForms: true
});

var inputs_to_disable = $('#data-formset-body').find(':input').not(':button, :hidden, :checkbox');

inputs_to_disable.prop('disabled', true);

$('#form').on('submit', function () {
    inputs_to_disable.prop('disabled', false);
});
