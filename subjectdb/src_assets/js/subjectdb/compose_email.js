/*
 * Copyright (c) 2016-2025, Thomas Hartmann
 *
 * This file is part of the OBOB Subject Database Project, see: https://gitlab.com/obob/obob_subjectdb/
 *
 *    obob_subjectdb is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    obob_subjectdb is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.
 */

import 'jquery';

let should_update = true;
let submit_clicked = false
let xhr = null;

function process_form_change(){
    console.log('Form changed')
    if(!should_update || submit_clicked) {
        return;
    }

    should_update = false;
    xhr = new XMLHttpRequest();
    xhr.open('POST', '.')

    const form_data = new FormData(document.getElementById('form'));
    form_data.set('draft_update', true)

    xhr.onloadend = () => {should_update = true;};
    xhr.send(form_data);
    xhr = null;
}

let timeout = null;

function delayed_form_change() {
    clearTimeout(timeout);
    timeout = setTimeout(process_form_change, 500);
}

document.getElementById('id_content').onkeyup = delayed_form_change;
document.getElementById('id_subject').onkeyup = delayed_form_change;
document.getElementById('form').onsubmit = (evt) => {
    evt.preventDefault();
    submit_clicked = true;
    clearTimeout(timeout)
    if(xhr !== null) {
        xhr.abort();
    }
    evt.target.submit();
};
