/*
 * Copyright (c) 2016-2025, Thomas Hartmann
 *
 * This file is part of the OBOB Subject Database Project, see: https://gitlab.com/obob/obob_subjectdb/
 *
 *    obob_subjectdb is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    obob_subjectdb is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.
 */

import 'jquery';
import '../../../../obob_subjectdb/src_assets/js/obob_subjectdb/init_general';
import init_extradata from "./lib/init_extradata";
import 'bootstrap-sass/assets/javascripts/bootstrap';
import 'bootstrap-validator';
import 'select2';
import 'bootstrap-datepicker';

var consent_input = $('#id_consent_personal_info');
var personal_form = $('.personal_information');
var subject_form = $('#form');
var warning_dlg = $('#remove_personal_warning');
var warning_ok = $('#btn_delete_personal_info');
var consent_given = consent_input.prop('checked');
var warning_accepted = false;
var special_groups_select = $('#id_special_groups');

var init_special_groups = function () {
    special_groups_select.select2({
        theme: 'bootstrap',
        placeholder: 'Select Special Groups'
    });
};

init_special_groups();

if (consent_given) {
    $.each(personal_form, function (index, value) {
        $(value).show();
        $(value).find('input').attr('data-validate', 'true');
    });
    subject_form.validator('update');
}
else {
    $.each(personal_form, function (index, value) {
        $(value).hide();
        $(value).find('input').attr('data-validate', 'false');
    });
    subject_form.validator('update');
}

consent_input.click(function () {
    if (consent_input.prop('checked')) {
        $.each(personal_form, function (index, value) {
            $(value).show();
            $(value).find('input').attr('data-validate', 'true');
        });
        init_special_groups();
        subject_form.validator('update');
    }
    else {
        $.each(personal_form, function (index, value) {
            $(value).hide();
            $(value).find('input').attr('data-validate', 'false');
        });
        subject_form.validator('update');
    }
});

subject_form.submit(function (event) {
    console.log(event.data);
    if (consent_given && !consent_input.prop('checked') && !warning_accepted) {
        warning_dlg.modal('show');
        warning_ok.click(function () {
            warning_accepted = true;
            subject_form.submit();
        });
        event.preventDefault();
    }
});

$('#id_birthday').datepicker({
    format: $('#date_format').val(),
    orientation: 'bottom left',
    autoclose: true
});

init_extradata();
subject_form.validator('update');
