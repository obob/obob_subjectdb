/*
 * Copyright (c) 2016-2025, Thomas Hartmann
 *
 * This file is part of the OBOB Subject Database Project, see: https://gitlab.com/obob/obob_subjectdb/
 *
 *    obob_subjectdb is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    obob_subjectdb is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.
 */
import 'jquery';
import calc_subject_id from "./lib/calc_subject_id";
import 'select2';
import 'bootstrap-datetime-picker';
import 'bootstrap-datepicker';

var subject_input = $('#id_subject');
var mother_firstname_input = $('#id_mother_first_name');
var mother_maidenname_input = $('#id_mother_maiden_name');
var birthday_input = $('#id_birthday');

var subject_options = {};
subject_input.find('option').map(function () {
    subject_options[$(this).text()] = $(this).val();
});

subject_input.select2({
    theme: "bootstrap"
});

function update_subject_input(event) {
    var subject_id = calc_subject_id(mother_firstname_input.val(), mother_maidenname_input.val(), birthday_input.val());
    console.log(subject_id);
    subject_input.val(subject_options[subject_id]).change();
}

mother_firstname_input.change(update_subject_input);
mother_maidenname_input.change(update_subject_input);
birthday_input.change(update_subject_input);

$('#id_datetime_measured').datetimepicker({
    format: $('#datetime_format').val(),
    orientation: 'bottom left',
    autoclose: true
});

$('#id_birthday').datepicker({
    format: $('#date_format').val(),
    orientation: 'bottom left',
    autoclose: true
});
