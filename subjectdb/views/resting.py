#  Copyright (c) 2016-2025, Thomas Hartmann
#
#  This file is part of the OBOB Subject Database Project,
#  see: https://gitlab.com/obob/obob_subjectdb/
#
#   obob_subjectdb is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   obob_subjectdb is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.
import zipfile
from io import BytesIO
from pathlib import Path

import django_pandas.io
from django.http import FileResponse
from django.views import View
from django_downloadview import VirtualDownloadView, VirtualFile
from django_filters.views import FilterView
from django_tables2 import A, SingleTableMixin, Table
from dynamic_preferences.registries import global_preferences_registry

import mridb.models
from obob_subjectdb.mixins import DefaultMixin
from subjectdb import models
from subjectdb.views.columns import ExperimentColumn, MriLinkColumn, SubjectColumn
from subjectdb.views.filters import RestingStateFilter
from th_bootstrap.django_tables2.columns import BooleanColumn, Column, DateTimeColumn


class RestingStateTable(Table):
    class Meta:
        fields = ['subject']

    subject = SubjectColumn(field='subject__subject_id')
    datetime_measured = DateTimeColumn()
    age_at_measurement = Column(verbose_name='Age')
    experiment = ExperimentColumn(field='session__experiment__acronym', orderable=False)
    all_data_complete = BooleanColumn(
        accessor=A('subject__has_all_info_prop'), verbose_name='Information complete', orderable=False
    )
    mri = MriLinkColumn()

    def __init__(self, data, **kwargs):
        super().__init__(data, **kwargs)
        self.mridb_list = mridb.models.Subject.objects.filter(deleted=False).all().values_list('subject_id', flat=True)


class RestingState(DefaultMixin, SingleTableMixin, FilterView):
    main_header_html = 'Resting State Measurements'
    model = models.RestingStateSession
    permission_required = 'subjectdb.view_subjectdb'
    table_class = RestingStateTable
    filterset_class = RestingStateFilter
    template_name = 'subjectdb/resting_list.html'

    def get(self, request, *args, **kwargs):
        response = super().get(request, *args, **kwargs)

        request.session['chosen_resting_state_pks'] = list(self.object_list.values_list('pk', flat=True))

        return response


class ExportToExcel(DefaultMixin, VirtualDownloadView):
    def get_file(self):
        global_preferences = global_preferences_registry.manager()
        f_name_sub_pattern = global_preferences['restingstate__restingstate_filename_sub_match']
        f_name_sub_replace = global_preferences['restingstate__restingstate_filename_sub_replace']

        mri_list = mridb.models.Subject.objects.filter(deleted=False).all().values_list('subject_id', flat=True)
        col_mapping = {
            'datetime_measured': 'Date of Measurement',
            'subject__subject_id': 'Subject ID',
            'subject__birthday': 'Birthday',
            'subject__gender': 'Gender',
            'subject__handedness': 'Handedness',
            'age_at_measurement': 'Age',
            's_freq': 'Sampling Frequency',
            'dewar_position': 'Dewar Position',
            'session__experiment__acronym': 'Experiment',
            'filename': 'Filename',
        }

        resting_states = models.RestingStateSession.objects.filter(
            pk__in=self.request.session['chosen_resting_state_pks']
        )

        df = django_pandas.io.read_frame(resting_states, fieldnames=list(col_mapping.keys()))

        df = df.rename(columns=col_mapping)

        df['Date of Measurement'] = df['Date of Measurement'].dt.tz_localize(None)

        df['Has MRI'] = [s in mri_list for s in df['Subject ID']]
        if f_name_sub_replace and f_name_sub_pattern:
            df['Filename'].replace(f_name_sub_pattern, f_name_sub_replace, regex=True, inplace=True)

        buf = BytesIO()
        df.to_excel(buf)

        return VirtualFile(buf, name='resting_data.xlsx')


class DownloadAllMris(DefaultMixin, View):
    def get(self, request, *args, **kwargs):
        resting_states = models.RestingStateSession.objects.filter(pk__in=request.session['chosen_resting_state_pks'])

        all_subject_ids = resting_states.values_list('subject__subject_id', flat=True).distinct()

        all_mris = mridb.models.MRI.objects.filter(
            subject__subject_id__in=all_subject_ids, deleted=False, mri_type__mri_type='T1'
        )

        buf = BytesIO()

        with zipfile.ZipFile(buf, 'w') as this_zip:
            for cur_mri in all_mris:
                if Path(cur_mri.mri_file.full_file_path).exists():
                    this_zip.write(cur_mri.mri_file.full_file_path, cur_mri.create_filename())

        buf.seek(0)
        response = FileResponse(buf)
        response['Content-Disposition'] = 'attachment; filename=restingstate_mris.zip'

        return response
