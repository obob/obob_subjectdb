# Copyright (c) 2016-2025, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project,
# see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.
from django.utils.html import format_html
from django_auto_url.urls import reverse_classname_lazy
from django_tables2 import TemplateColumn
from django_tables2.columns import LinkColumn, library
from django_tables2.utils import A

import subjectdb.models
from th_bootstrap.django_tables2.columns import (
    BooleanColumn,
    BootstrapColumnMixin,
    Column,
    DeleteColumn,
    LinkColumnWithID,
    UndeleteColumn,
)


@library.register
class MriLinkColumn(Column):
    def __init__(self, **kwargs):
        kwargs.update(
            {
                'accessor': A('subject_id'),
                'verbose_name': 'MRI',
                'orderable': False,
            }
        )

        super().__init__(**kwargs)

    def render(self, value, record, table, **kwargs):
        if isinstance(record, (subjectdb.models.Session, subjectdb.models.RestingStateSession)):
            record = record.subject

        if record.subject_id in table.mridb_list:
            return format_html(
                '<a href="{}">MRI</a>',
                reverse_classname_lazy('mridb.views.display.SubjectDetail', kwargs={'subject_id': record.subject_id}),
            )
        else:
            return 'N/A'


@library.register
class MriAvailableColumn(BooleanColumn):
    def __init__(self, **kwargs):
        self.table = None
        kwargs.update(
            {
                'accessor': A('subject_id'),
                'verbose_name': 'MRI',
                'orderable': False,
            }
        )

        super().__init__(**kwargs)

    def render(self, value, record, bound_column, table, **kwargs):
        self.table = table

        return super().render(value, record, bound_column)

    def _get_bool_value(self, record, value, bound_column):
        if isinstance(record, subjectdb.models.Session):
            record = record.subject

        return record.subject_id in self.table.mridb_list


@library.register
class SubjectColumn(BootstrapColumnMixin, LinkColumn):
    def __init__(self, field='subject_id', **kwargs):
        kwargs.update(
            {
                'viewname': reverse_classname_lazy('subjectdb.views.subjectbase.SubjectDetail', return_url_name=True),
                'args': [A(field)],
                'accessor': A(field),
                'verbose_name': 'Subject',
            }
        )

        super().__init__(**kwargs)


@library.register
class ExperimentColumn(BootstrapColumnMixin, LinkColumn):
    def __init__(self, field='acronym', **kwargs):
        kwargs.update(
            {
                'viewname': reverse_classname_lazy(
                    'subjectdb.views.experiments.ExperimentDetail', return_url_name=True
                ),
                'args': [A(field)],
                'accessor': A(field),
                'verbose_name': 'Experiment',
            }
        )

        super().__init__(**kwargs)


@library.register
class ExtraDataColumn(BootstrapColumnMixin, LinkColumn):
    def __init__(self, field='pk', **kwargs):
        kwargs.update(
            {
                'viewname': reverse_classname_lazy('subjectdb.views.extradata.ExtraDataDetails', return_url_name=True),
                'args': [A(field)],
                'accessor': A('name'),
                'verbose_name': 'Extra Data',
            }
        )

        super().__init__(**kwargs)


@library.register
class SpecialGroupColumn(BootstrapColumnMixin, LinkColumn):
    def __init__(self, field='pk', **kwargs):
        kwargs.update(
            {
                'viewname': reverse_classname_lazy(
                    'subjectdb.views.specialgroups.SpecialGroupsDetail', return_url_name=True
                ),
                'args': [A(field)],
                'accessor': A('group_name'),
                'verbose_name': 'Group Name',
            }
        )

        super().__init__(**kwargs)


@library.register
class SubjectToSessionColumn(BootstrapColumnMixin, LinkColumn):
    def __init__(self, field='subject_id', **kwargs):
        kwargs.update(
            {
                'viewname': reverse_classname_lazy('subjectdb.views.session.SubjectSessions', return_url_name=True),
                'args': [A(field)],
                'accessor': A(field),
                'verbose_name': 'Sessions',
                'text': 'View Sessions',
                'attrs': {
                    'a': {
                        'class': 'btn btn-default btn-sm',
                        'name': 'view-sessions',
                    }
                },
                'orderable': False,
            }
        )

        super().__init__(**kwargs)

    def render(self, record, table, value, **kwargs):
        self.attrs['a']['class'] = self.attrs['a']['class'].replace('disabled', '')
        if record.session_set.all().count() == 0:
            self.attrs['a']['class'] = self.attrs['a']['class'] + ' disabled'

        return super().render(value, record)


@library.register
class AddSessionColumn(BootstrapColumnMixin, LinkColumnWithID):
    def __init__(self, **kwargs):
        default_args = {
            'viewname': reverse_classname_lazy('subjectdb.views.session.SessionAdd', return_url_name=True),
            'args': [A('acronym')],
            'accessor': A('acronym'),
            'text': 'Add Session',
            'verbose_name': 'Add Session',
            'attrs': {
                'a': {
                    'class': 'btn btn-default btn-sm',
                    'name': 'add_session',
                }
            },
            'orderable': False,
        }

        default_args.update(kwargs)

        super().__init__(**default_args)


@library.register
class DeleteSubjectColumn(DeleteColumn):
    def is_owner(self, context, record):
        if len(record.valid_sessions) > 0:
            return False

        return super().is_owner(context, record)


@library.register
class DeleteSessionColumn(DeleteColumn):
    def is_owner(self, context, record):
        if self._must_be_owner:
            return (
                context.request.user == record.experiment.added_by
                or context.request.user.is_superuser
                or context.request.user == record.added_by
                or context.request.user.has_perm('subjectdb.view_edit_all_subjectdb')
            )

        return True


@library.register
class UndeleteSessionColumn(UndeleteColumn):
    def is_owner(self, context, record):
        if self._must_be_owner:
            return (
                context.request.user == record.experiment.added_by
                or context.request.user.is_superuser
                or context.request.user == record.added_by
                or context.request.user.has_perm('subjectdb.view_edit_all_subjectdb')
            )

        return True


@library.register
class ExtradataCompleteColumn(BooleanColumn):
    empty_values = ()

    def _get_bool_value(self, record, value, bound_column):
        rval = True
        for cur_extradata in record.concreteextradata_set.all():
            rval = rval & cur_extradata.extradata_complete

        return rval


@library.register
class MethodsColumn(BootstrapColumnMixin, TemplateColumn):
    field_to_string = {
        'uses_eeg': 'EEG',
        'uses_meg': 'MEG',
        'uses_fmri': 'fMRI',
        'uses_eyetracker': 'Eyetracker',
    }

    def __init__(self, *args, **kwargs):
        kwargs.update(
            {
                'verbose_name': 'Methods',
                'orderable': False,
                'searchable': True,
                'empty_values': (),
                'template_name': 'subjectdb/experiment_methods_column.html',
            }
        )

        super().__init__(*args, **kwargs)

    def render(self, value, record, table, **kwargs):
        self.extra_context = {
            'methods': [value for key, value in self.field_to_string.items() if getattr(record, key)],
        }

        return super().render(value, record, table, **kwargs)
