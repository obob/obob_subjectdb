# Copyright (c) 2016-2025, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project,
# see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.

import contextlib

import str2bool
from django import forms
from django.contrib import messages
from django.forms import ModelForm
from django.views.generic import DeleteView, DetailView, ListView
from django_auto_url import kwargs
from django_auto_url.urls import reverse_classname, reverse_local_classname
from django_tables2 import SingleTableMixin, Table
from extra_views import InlineFormSetFactory

from subjectdb import models
from subjectdb.views.columns import ExtraDataColumn
from subjectdb.views.mixins import DefaultMixin
from th_bootstrap.django_tables2.columns import BooleanColumn, Column, DateTimeColumn, DeleteColumn, EditColumn
from th_bootstrap.views.mixins import DetailFieldsMixin
from th_django.views.edit import CreateUpdateWithInlineView
from th_django.views.mixins import MessageAfterPostMixin


class SessionExtraDataTable(Table):
    class Meta:
        model = models.ExtraDataTemplate
        fields = ['name', 'description']

    datetime_added = DateTimeColumn()
    name = ExtraDataColumn()
    description = Column(orderable=False)
    edit = EditColumn(reverse_local_classname('SessionExtraDataEdit', return_url_name=True), 'pk')
    delete = DeleteColumn(
        'id', 'confirm_delete', reverse_local_classname('SessionExtraDataDelete', return_url_name=True)
    )


class SubjectExtraDataTable(SessionExtraDataTable):
    edit = EditColumn(reverse_local_classname('SubjectExtraDataEdit', return_url_name=True), 'pk')
    delete = DeleteColumn(
        'id', 'confirm_delete', reverse_local_classname('SubjectExtraDataDelete', return_url_name=True)
    )


class DataItemsTable(Table):
    class Meta:
        model = models.ExtraDataTemplateItem
        fields = ['label', 'description', 'is_required', 'field_options']
        orderable = False

    is_required = BooleanColumn()


class DataItemModelForm(ModelForm):
    class Meta:
        model = models.ExtraDataTemplateItem
        fields = ['label', 'type', 'is_required', 'field_options']
        can_delete = True

    label = forms.CharField(label='Label')
    type = forms.ChoiceField(label='Type', choices=models.get_datatype_choices())
    field_options = forms.CharField(widget=forms.Textarea(attrs={'rows': 1}), required=False)

    def save(self, commit=True):
        self.instance.type = self.cleaned_data['type']
        return super().save(commit)


class DataItemFormset(InlineFormSetFactory):
    model = models.ExtraDataTemplateItem
    form_class = DataItemModelForm

    factory_kwargs = {'extra': 0, 'can_delete': True}


class SessionExtraData(DefaultMixin, SingleTableMixin, ListView):
    model = models.ExtraDataTemplate
    table_class = SessionExtraDataTable
    main_header_html = 'All Optional SessionExtraData'
    template_name = 'subjectdb/sessionextradata_list.html'
    permission_required = 'subjectdb.add_dataitem'
    is_session_extradata = True
    is_default_extradata = False

    def get_queryset(self):
        qs = super().get_queryset()
        qs = qs.filter(is_session_extradata=self.is_session_extradata, is_default_extradata=self.is_default_extradata)
        return qs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['is_default_extradata'] = self.is_default_extradata

        return context


class DefaultSessionExtraData(SessionExtraData):
    main_header_html = 'All Default SessionExtraData'
    is_default_extradata = True


class SessionExtraDataCreate(DefaultMixin, CreateUpdateWithInlineView):
    model = models.ExtraDataTemplate
    fields = ['name', 'description']
    main_header_html = 'Create new SessionExtraData'
    inlines = [DataItemFormset]
    template_name = 'subjectdb/extradata_create.html'
    permission_required = 'subjectdb.add_dataitem'
    is_session_extradata = True
    create_if_nonexistent = True
    url_kwargs = [kwargs.bool('is_default_extradata')]

    extra_context = {'extra_js': ['subjectdb/extradata_form.js']}

    def get_success_url(self):
        try:
            is_default_extradata = str2bool.str2bool(self.kwargs['is_default_extradata'])
        except KeyError:
            is_default_extradata = self.object.is_default_extradata

        if is_default_extradata:
            return reverse_classname(DefaultSessionExtraData)
        else:
            return reverse_classname(SessionExtraData)

    def post(self, request, *args, **kwargs):
        rval = super().post(request, *args, **kwargs)
        self.object.is_session_extradata = self.is_session_extradata

        with contextlib.suppress(KeyError):
            self.object.is_default_extradata = str2bool.str2bool(kwargs['is_default_extradata'])

        self.object.save()
        return rval


class SessionExtraDataEdit(SessionExtraDataCreate):
    url_kwargs = []
    url_ignore_pk = False
    main_header_html = 'Edit SessionExtraData'


class ExtraDataDetails(DefaultMixin, SingleTableMixin, DetailView, DetailFieldsMixin):
    model = models.ExtraDataTemplate
    show_fields = ['name', 'description', 'is_session_extradata']
    template_name = 'subjectdb/extradata_detail.html'
    table_class = DataItemsTable
    permission_required = 'subjectdb.add_dataitem'

    def get_main_header(self):
        return f'Extra Data {self.object.name}'

    def get_table_data(self):
        return self.object.extradatatemplateitem_set.all()


class SubjectExtraData(SessionExtraData):
    is_session_extradata = False
    main_header_html = 'All Optional SubjectExtraData'
    template_name = 'subjectdb/subjectextradata_list.html'
    table_class = SubjectExtraDataTable


class DefaultSubjectExtraData(SubjectExtraData):
    is_default_extradata = True
    main_header_html = 'All Default SubjectExtraData'


class SubjectExtraDataCreate(SessionExtraDataCreate):
    is_session_extradata = False
    main_header_html = 'Create new SubjectExtraData'

    def get_success_url(self):
        try:
            is_default_extradata = str2bool.str2bool(self.kwargs['is_default_extradata'])
        except KeyError:
            is_default_extradata = self.object.is_default_extradata

        if is_default_extradata:
            return reverse_classname(DefaultSubjectExtraData)
        else:
            return reverse_classname(SubjectExtraData)


class SubjectExtraDataEdit(SubjectExtraDataCreate):
    main_header_html = 'Edit SubjectExtraData'
    url_kwargs = []
    url_ignore_pk = False


class ConcreteExtradataForm(ModelForm):
    class Meta:
        model = models.ConcreteExtraData
        fields = []

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for cur_field in self.instance.dataitem_set.all():
            cur_label_escaped = cur_field.label.replace(' ', '_')
            self.fields[cur_label_escaped] = cur_field.get_formfield()
            self.fields[cur_label_escaped].required = False
            self.fields[cur_label_escaped].label = cur_field.label
            if cur_field.value:
                self.fields[cur_label_escaped].initial = cur_field.value

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        for cur_extraitem in self.instance.dataitem_set.all():
            cur_extraitem.value = self.cleaned_data[cur_extraitem.label.replace(' ', '_')]
            cur_extraitem.save()


class ConcreteExtradataFormset(InlineFormSetFactory):
    model = models.ConcreteExtraData
    factory_kwargs = {
        'can_delete': False,
        'extra': 0,
        'fields': [],
    }

    form_class = ConcreteExtradataForm


class SessionExtraDataDelete(DefaultMixin, MessageAfterPostMixin, DeleteView):
    model = models.ExtraDataTemplate
    permission_required = 'subjectdb.delete_extradatatemplate'
    message_priority = messages.SUCCESS

    def get_success_url(self):
        if self.object.is_default_extradata:
            return reverse_classname(DefaultSessionExtraData)
        else:
            return reverse_classname(SessionExtraData)

    def get_message_text(self):
        return 'Successfully deleted the SessionExtraData'

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        if self.object.is_default_extradata:
            all_experiment_ids = list(models.Experiment.objects.all().values_list('id', flat=True))
        else:
            all_experiment_ids = list(self.object.experiment_set.all().values_list('id', flat=True))

        rval = super().post(request, *args, **kwargs)

        all_experiments = models.Experiment.objects.filter(pk__in=all_experiment_ids)

        for cur_exp in all_experiments:
            for cur_session in cur_exp.session_set.all():
                cur_session.update_extradata()

        return rval


class SubjectExtraDataDelete(DefaultMixin, MessageAfterPostMixin, DeleteView):
    model = models.ExtraDataTemplate
    permission_required = 'subjectdb.delete_extradatatemplate'
    message_priority = messages.SUCCESS

    def get_success_url(self):
        if self.object.is_default_extradata:
            return reverse_classname(DefaultSubjectExtraData)
        else:
            return reverse_classname(SubjectExtraData)

    def get_message_text(self):
        return 'Successfully deleted the SubjectExtraData'

    def post(self, request, *args, **kwargs):
        rval = super().post(request, *args, **kwargs)

        for cur_subject in models.SubjectBase.objects.all():
            cur_subject.update_extradata()

        return rval
