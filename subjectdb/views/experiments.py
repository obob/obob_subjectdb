# Copyright (c) 2016-2025, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project,
# see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.

import datetime
import distutils.util
import tempfile
import zipfile
from pathlib import Path

import django.contrib.auth.models
import django_excel
from django import forms
from django.conf import settings
from django.contrib import auth, messages
from django.db.models import Q
from django.forms.models import ModelForm
from django.http import FileResponse, Http404
from django.urls import reverse_lazy
from django.views import View
from django.views.generic import DetailView, ListView
from django.views.generic.detail import SingleObjectMixin
from django_auto_url import kwargs
from django_auto_url.urls import reverse_classname_lazy, reverse_local_classname
from django_filters.views import FilterView
from django_tables2 import SingleTableMixin, Table

import mridb.models
import user_manager.models
from subjectdb import models
from subjectdb.views.columns import (
    AddSessionColumn,
    DeleteSessionColumn,
    ExperimentColumn,
    ExtradataCompleteColumn,
    MethodsColumn,
    MriLinkColumn,
    SubjectColumn,
)
from subjectdb.views.filters import ExperimentFilter
from subjectdb.views.mixins import DefaultMixin
from th_bootstrap.django_tables2.columns import (
    BooleanColumn,
    Column,
    DateTimeColumn,
    DeleteColumn,
    EditColumn,
    UndeleteColumn,
)
from th_bootstrap.views.mixins import DetailFieldsMixin
from th_django.views.edit import CreateUpdateTrashcanView, DeleteToTrashcanView, UndeleteFromTrashcanView
from th_django.views.mixins import MessageAfterPostMixin


def get_sinuhe_projects() -> list[tuple[str, str]]:
    raw_folder = Path(settings.RAW_MEG_DIR)
    choices = [('', '---')]
    if not raw_folder.exists():
        return choices
    projects = sorted([x.name for x in raw_folder.iterdir() if x.is_dir()])

    return choices + [(x, x) for x in projects]


class ExperimentEditForm(ModelForm):
    class Meta:
        model = models.Experiment
        fields = [
            'acronym',
            'short_description',
            'uses_eeg',
            'uses_meg',
            'uses_fmri',
            'uses_eyetracker',
            'sinuhe_project',
            'paradigm_url',
            'anc_url',
            'added_by',
            'additional_investigators',
            'extra_data',
        ]

    sinuhe_project = forms.ChoiceField(choices=get_sinuhe_projects, required=False)


class ExperimentListTable(Table):
    class Meta:
        model = models.Experiment
        fields = ['acronym', 'datetime_added']

    datetime_added = DateTimeColumn()
    acronym = ExperimentColumn()
    methods = MethodsColumn()
    added_by_name = Column(orderable=False, verbose_name='Owner')
    number_of_sessions = Column(verbose_name='Number of Sessions')
    add_session = AddSessionColumn()
    edit = EditColumn(
        reverse_local_classname('ExperimentEdit', return_url_name=True),
        'pk',
        needs_permission='subjectdb.change_experiment',
    )
    delete = DeleteColumn('id', 'confirm_delete', reverse_local_classname('ExperimentDelete', return_url_name=True))


class ExperimentTrashTable(Table):
    class Meta(ExperimentListTable.Meta):
        fields = ['acronym', 'datetime_added', 'datetime_deleted']

    acronym = Column()
    datetime_added = DateTimeColumn()
    datetime_deleted = DateTimeColumn()
    undelete = UndeleteColumn('id', reverse_local_classname('ExperimentUndelete', return_url_name=True))


class SessionTable(Table):
    class Meta:
        fields = ['datetime_measured', 'subject', 'pilot_session', 'notes', 'excluded_from_experiment']

    datetime_measured = DateTimeColumn()
    subject = SubjectColumn(field='subject__subject_id')
    pilot_session = BooleanColumn(orderable=False)
    mri = MriLinkColumn()
    notes = Column(orderable=False)
    extradata_complete = ExtradataCompleteColumn(orderable=False)
    excluded_from_experiment = BooleanColumn(orderable=False)
    edit = EditColumn(
        reverse_classname_lazy('subjectdb.views.session.SessionEdit', return_url_name=True),
        'pk',
        needs_permission='subjectdb.change_session',
    )
    delete = DeleteSessionColumn(
        'id',
        'confirm_delete',
        reverse_classname_lazy('subjectdb.views.session.SessionDelete', return_url_name=True),
        must_be_owner=True,
        needs_permission='subjectdb.delete_session',
    )

    def __init__(self, data, **kwargs):
        super().__init__(data, **kwargs)
        self.mridb_list = mridb.models.Subject.objects.filter(deleted=False).all().values_list('subject_id', flat=True)

    def before_render(self, request):
        if not self.context['object'].has_extradata:
            self.columns.hide('extradata_complete')


class Experiments(DefaultMixin, SingleTableMixin, FilterView):
    model = models.Experiment
    table_class = ExperimentListTable
    is_index = True
    main_header_html = 'All Experiments'
    filterset_class = ExperimentFilter
    template_name = 'subjectdb/experiment_list.html'

    extra_context = {'extra_js': ['subjectdb/experiment_list.js']}

    def get_queryset(self):
        show_all_experiments = distutils.util.strtobool(
            self.request.GET.get('show_all_experiments', self._show_experiment_default())
        )

        if (
            self.request.user.is_superuser or self.request.user.has_perm('subjectdb.view_edit_all_subjectdb')
        ) and show_all_experiments:
            return self.model.objects.filter(deleted=False)
        else:
            return (
                self.model.objects.filter(deleted=False)
                .filter(Q(added_by=self.request.user) | Q(additional_investigators=self.request.user))
                .distinct()
            )

    def get_context_data(self, **kwargs):
        kwargs['show_all_experiments'] = distutils.util.strtobool(
            self.request.GET.get('show_all_experiments', self._show_experiment_default())
        )
        return super().get_context_data(**kwargs)

    def _show_experiment_default(self):
        lab_manager_group = django.contrib.auth.models.Group.objects.get(name='Lab Manager')
        investigator_group = django.contrib.auth.models.Group.objects.get(name='Investigator/Technical Staff')
        default_show_all_experiments = 'False'
        if (
            lab_manager_group in self.request.user.groups.all()
            and investigator_group not in self.request.user.groups.all()
        ):
            default_show_all_experiments = 'True'

        return default_show_all_experiments


class ExperimentTrash(DefaultMixin, SingleTableMixin, ListView):
    model = models.Experiment
    permission_required = 'subjectdb.delete_experiment'
    table_class = ExperimentTrashTable
    main_header_html = 'Deleted Experiments'
    template_name = 'base_table.html'

    def get_queryset(self):
        if self.request.user.is_superuser or self.request.user.has_perm('subjectdb.view_edit_all_subjectdb'):
            return self.model.objects.filter(deleted=True)
        else:
            return (
                self.model.objects.filter(deleted=True)
                .filter(Q(added_by=self.request.user) | Q(additional_investigators=self.request.user))
                .distinct()
            )


class ExperimentEdit(DefaultMixin, CreateUpdateTrashcanView):
    must_be_owner = True
    form_class = ExperimentEditForm
    model = models.Experiment

    extra_context = {'extra_js': ['subjectdb/experiment_form.js']}

    def get_success_url(self):
        return reverse_lazy('subjectdb:experiments:Experiments')

    def get_permission_required(self):
        if self.slug_field in self.kwargs:
            return ('subjectdb.change_experiment',)
        else:
            return ('subjectdb.add_experiment',)

    def get_form(self, form_class=None):
        form = super().get_form(form_class)

        form.fields['additional_investigators'].queryset = (
            form.fields['additional_investigators']
            .queryset.filter(user_manager.models.ExtendedUser.can_login_filter())
            .exclude(id=self.request.user.id)
            .filter(Q(groups__name='Investigator/Technical Staff') | Q(is_superuser=True))
            .distinct()
            .order_by('last_name', 'first_name')
        )
        form.fields['additional_investigators'].label_from_instance = lambda obj: '%s, %s' % (  # noqa
            obj.last_name,
            obj.first_name,
        )

        form.fields['added_by'].queryset = (
            form.fields['added_by']
            .queryset.filter(user_manager.models.ExtendedUser.can_login_filter())
            .filter(Q(groups__name='Investigator/Technical Staff') | Q(is_superuser=True))
            .distinct()
            .order_by('last_name', 'first_name')
        )
        form.fields['added_by'].label_from_instance = lambda obj: f'{obj.last_name}, {obj.first_name}'

        if not form.fields['added_by'].initial:
            form.fields['added_by'].initial = self.request.user.id

        form.fields['added_by'].label = 'Owner'
        form.fields['added_by'].empty_label = None

        if (
            auth.models.Group.objects.get(name='Lab Manager') not in self.request.user.groups.all()
            and not self.request.user.is_superuser
        ):
            del form.fields['added_by']

        form.fields['extra_data'].queryset = form.fields['extra_data'].queryset.filter(
            is_session_extradata=True, is_default_extradata=False
        )

        return form

    def get_main_header(self):
        return f'Experiment: {self.object.acronym}'


class ExperimentCreate(ExperimentEdit):
    url_kwargs = []
    url_ignore_pk = True

    def get_main_header(self):
        return 'New Experiment'


class ExperimentDelete(DefaultMixin, MessageAfterPostMixin, DeleteToTrashcanView):
    model = models.Experiment
    http_method_names = ['post', 'delete']
    success_url = reverse_classname_lazy(Experiments)
    permission_required = 'subjectdb.delete_experiment'
    must_be_owner = True
    message_priority = messages.SUCCESS
    url_kwargs = [kwargs.int('pk')]

    def get_message_text(self):
        return f'Successfully deleted experiment {self.get_object().acronym}.'


class ExperimentUndelete(DefaultMixin, MessageAfterPostMixin, UndeleteFromTrashcanView):
    model = models.Experiment
    http_method_names = ['post']
    permission_required = 'subjectdb.delete_experiment'
    must_be_owner = True
    success_url = reverse_classname_lazy(Experiments)
    message_priority = messages.SUCCESS

    def get_message_text(self):
        return f'Successfully undeleted experiment {self.get_object().acronym}.'


class ExperimentDetail(DefaultMixin, DetailView, SingleTableMixin, DetailFieldsMixin):
    model = models.Experiment
    permission_required = 'subjectdb.view_subjectdb'
    must_be_owner = True
    show_fields = [
        'acronym',
        'short_description',
        'uses_eeg',
        'uses_meg',
        'uses_fmri',
        'uses_eyetracker',
        'anc_url',
        'paradigm_url',
        'sinuhe_project',
        'additional_investigators',
        'extra_data',
        'nr_of_mris_nr_of_subjects',
    ]
    slug_field = 'acronym'
    slug_url_kwarg = 'acronym'
    table_class = SessionTable
    url_kwargs = [kwargs.string('acronym')]

    def get_additional_investigators_string(self):
        values = []
        for investigator in self.object.additional_investigators.all():
            values.append(f'{investigator.last_name}, {investigator.first_name}')

        return '\n'.join(values)

    def get_nr_of_mris_nr_of_subjects_string(self):
        nr_of_mris = self.object.get_all_mris().count()

        return f'{nr_of_mris}/{self.object.get_nr_of_subjects()}'

    def get_nr_of_mris_nr_of_subjects_verbose_name(self):
        return 'Number of MRIS/Subjects'

    def get_extra_data_string(self):
        values = []
        for cur_extra_data in self.object.extra_data.all():
            values.append(cur_extra_data.name)

        return '\n'.join(values)

    def get_table_data(self):
        return self.object.session_set.all().filter(deleted=False)

    def get_queryset(self):
        if models.Experiment.objects.get(acronym=self.kwargs['acronym']).deleted:
            raise Http404

        return super().get_queryset()

    def get_main_header(self):
        return f'Experiment: {self.object.acronym}'


class ExperimentToExcel(DefaultMixin, SingleObjectMixin, View):
    model = models.Experiment
    slug_field = 'acronym'
    slug_url_kwarg = 'acronym'
    permission_required = 'subjectdb.view_subjectdb'
    must_be_owner = True
    url_kwargs = [kwargs.string('acronym'), kwargs.string('file_format'), kwargs.bool('include_pilots')]

    def get(self, request, acronym, file_format, include_pilots, *args, **kwargs):
        return django_excel.make_response(
            self.get_object().export_to_excel(include_pilots == 'True'),
            file_format,
            file_name=f'{acronym}.{file_format}',
        )


class DownloadAllExperimentMRIS(DefaultMixin, SingleObjectMixin, View):
    model = models.Experiment
    slug_field = 'acronym'
    slug_url_kwarg = 'acronym'
    permission_required = 'subjectdb.view_subjectdb'
    url_kwargs = [kwargs.string('acronym')]

    def get(self, request, acronym):
        object = self.get_object()

        all_mris = object.get_all_mris()

        with tempfile.TemporaryFile() as temp_zip, zipfile.ZipFile(temp_zip, 'w') as this_zip:
            for cur_mri in all_mris:
                if Path(cur_mri.mri_file.full_file_path).exists():
                    this_zip.write(cur_mri.mri_file.full_file_path, cur_mri.create_filename())

        temp_zip.seek(0)

        file_name = f'all_mris_{object.acronym}_{datetime.datetime.now()}.zip'

        response = FileResponse(temp_zip)
        response['Content-Disposition'] = f'attachment; filename={file_name}'
        return response
