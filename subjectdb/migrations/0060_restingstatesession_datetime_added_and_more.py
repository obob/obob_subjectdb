# Generated by Django 4.0.1 on 2022-01-19 14:45

import datetime
from django.db import migrations, models
from datetime import timezone

utc = timezone.utc


class Migration(migrations.Migration):

    dependencies = [
        ('subjectdb', '0059_alter_restingstatesession_filename'),
    ]

    operations = [
        migrations.AddField(
            model_name='restingstatesession',
            name='datetime_added',
            field=models.DateTimeField(auto_now_add=True, default=datetime.datetime(2022, 1, 19, 14, 45, 5, 930278, tzinfo=utc), verbose_name='Added'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='restingstatesession',
            name='datetime_measured',
            field=models.DateTimeField(default=datetime.datetime(2022, 1, 19, 14, 45, 21, 610176, tzinfo=utc), verbose_name='Date and Time of Measurement'),
            preserve_default=False,
        ),
    ]
