# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-08-11 14:17
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [
        ('subjectdb', '0028_auto_20170810_1056'),
    ]

    operations = [
        migrations.CreateModel(
            name='DateItem',
            fields=[
                ('dataitem_ptr',
                 models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True,
                                      primary_key=True, serialize=False, to='subjectdb.DataItem')),
                ('date_value', models.DateField(null=True)),
            ],
            options={
                'abstract': False,
            },
            bases=('subjectdb.dataitem',),
        ),
    ]
