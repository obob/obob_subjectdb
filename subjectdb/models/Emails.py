# Copyright (c) 2016-2025, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project,
# see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.

from django.db import models

from th_django.models.mixins import DateTimeAddedBy

from .Experiment import Experiment
from .SubjectBase import SubjectBase


class RecruitmentEmail(DateTimeAddedBy):
    recipients = models.ManyToManyField(SubjectBase)
    subject = models.CharField(max_length=100, blank=True)
    content = models.TextField(blank=True)
    experiment = models.ForeignKey(Experiment, on_delete=models.CASCADE)
    status = models.CharField(
        max_length=20, choices=(('draft', 'Draft'), ('sent', 'Sent'), ('sending', 'sending'), ('error', 'error'))
    )
    filter_settings = models.JSONField(default=dict)
