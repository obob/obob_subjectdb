# Copyright (c) 2016-2025, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project,
# see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.
import datetime

import django.db.models.signals
from dateutil.relativedelta import relativedelta
from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.db.models import ExpressionWrapper, Q
from django.utils.functional import cached_property
from phonenumber_field.modelfields import PhoneNumberField

import mridb.models
import subjectdb.models.ExtraData
import th_django.modelfields.subject_id
from subjectdb.validators import birthday_validator
from th_django.models.mixins import DateTimeAddedBy, DeleteToTrash


class SubjectBaseManager(models.Manager):
    def get_queryset(self):
        qs = super().get_queryset()
        qs = qs.annotate(
            has_all_info=ExpressionWrapper(
                ~Q(gender__exact='') & ~Q(handedness__exact=''), output_field=models.BooleanField()
            )
        )

        return qs


class SubjectBase(DateTimeAddedBy, DeleteToTrash):
    class Meta(DateTimeAddedBy.Meta):
        permissions = (
            ('view_subjectdb', 'Can view subjectdb'),
            ('view_edit_all_subjectdb', 'Can view, edit and delete everybodies data'),
            ('change_subject_handedness_gender', 'Can change the handedness and gender of a subject'),
        )

    objects = SubjectBaseManager()

    subject_id = th_django.modelfields.subject_id.SubjectID(verbose_name='Subject ID', unique=True, blank=False)
    birthday = models.DateField('Birthday', validators=[birthday_validator])
    gender = models.CharField('Gender', max_length=20, choices=(('M', 'Male'), ('F', 'Female'), ('O', 'Other')))
    handedness = models.CharField(
        'Handedness', max_length=20, choices=(('Right', 'Right'), ('Left', 'Left'), ('Ambidexter', 'Ambidexter'))
    )
    notes = models.TextField('Notes', blank=True)
    consent_personal_info = models.BooleanField('Consent for Personal Information provided', default=False)
    available_for_experiments = models.BooleanField('Available for Experiments', default=True)
    special_groups = models.ManyToManyField(
        'SpecialSubjectGroups',
        blank=True,
        verbose_name='Special Groups',
        related_name='%(app_label)s_%(class)s_special_groups',
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def save(self, **kwargs):
        if not self.consent_personal_info:
            try:
                self.subjectpersonalinformation.delete()
                self.subjectpersonalinformation = None
            except ObjectDoesNotExist:
                pass

        rval = super().save(**kwargs)

        self.update_extradata()

        return rval

    def update_extradata(self):
        all_extradata = []
        for special_group in self.special_groups.all():
            for extra_data in special_group.extra_data.all():
                if extra_data.has_items and extra_data not in all_extradata:
                    all_extradata.append(extra_data)

        for extra_data in subjectdb.models.ExtraDataTemplate.objects.filter(
            is_default_extradata=True, is_session_extradata=False
        ):
            if extra_data.has_items and extra_data not in all_extradata:
                all_extradata.append(extra_data)

        present_concrete_extradata = self.concreteextradata_set.all().values_list('name', flat=True)

        # identify new or updated extradata
        for cur_extradata in all_extradata:
            if cur_extradata.name in present_concrete_extradata:
                current_concrete_extradata = self.concreteextradata_set.all().get(name=cur_extradata.name)
                self.concreteextradata_set.add(
                    cur_extradata.create_concrete_extradata(self, current_concrete_extradata)
                )
            else:
                self.concreteextradata_set.add(cur_extradata.create_concrete_extradata(self))

        # identify deleted extradata
        present_extradata = set(present_concrete_extradata.values_list('name', flat=True))
        new_extradata = {str(item.name) for item in all_extradata}
        deleted_extradata = present_extradata - new_extradata

        # iterate over all deleted extradatas and delete empty items
        for cur_extradata in self.concreteextradata_set.filter(name__in=deleted_extradata):
            all_empty = True
            for cur_item in cur_extradata.dataitem_set.all():
                if cur_item.is_empty:
                    cur_item.delete()
                else:
                    all_empty = False

            if all_empty:
                cur_extradata.delete()
        pass

    def __str__(self):
        return str(self.subject_id)

    def age(self, rel_to=datetime.date.today()):
        return relativedelta(rel_to, self.birthday).years

    def has_mri(self):
        return mridb.models.Subject.objects.filter(subject_id=self.subject_id).exists()

    @cached_property
    def has_valid_sessions(self):
        return self.session_set.exclude(deleted=True).count() > 0

    @property
    def has_extradata(self):
        return self.concreteextradata_set.all().exists()

    @property
    def has_personal_information(self):
        return hasattr(self, 'subjectpersonalinformation')

    @cached_property
    def experiments(self):
        from subjectdb.models import Experiment

        exp_ids = self.session_set.distinct().values_list('experiment', flat=True)
        return Experiment.objects.filter(pk__in=exp_ids)

    @property
    def has_all_info_prop(self):
        return self.gender != '' and self.handedness != ''


class SubjectPersonalInformation(models.Model):
    parent_subject = models.OneToOneField(SubjectBase, parent_link=True, on_delete=models.CASCADE)
    first_name = models.CharField(max_length=100, verbose_name='First Name')
    last_name = models.CharField(max_length=100, verbose_name='Last Name')
    email = models.EmailField(verbose_name='Email')
    mobile_phone = PhoneNumberField(blank=True, verbose_name='Phone Number')
    has_glasses = models.BooleanField(verbose_name='Subject wearing glasses')
    can_do_MEG = models.BooleanField(default=True, verbose_name='Can do MEG')  # noqa N815
    can_do_MRI = models.BooleanField(default=True, verbose_name='Can do MRI')  # noqa N815


class SpecialSubjectGroups(models.Model):
    group_name = models.CharField(unique=True, verbose_name='Group Name', max_length=30, blank=False)
    extra_data = models.ManyToManyField('ExtraDataTemplate', verbose_name='Extra Data', blank=True)

    def __str__(self):
        return self.group_name


def update_subjects_special_groups(sender, action, instance, **kwargs):
    if action == 'post_add':
        for cur_subject in instance.subjectdb_subjectbase_special_groups.all():
            cur_subject.update_extradata()


django.db.models.signals.m2m_changed.connect(
    update_subjects_special_groups, sender=SpecialSubjectGroups.extra_data.through
)
