# Copyright (c) 2016-2025, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project,
# see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.

from django.urls import include, re_path
from django_auto_url.urls import get_urls_from_module

import subjectdb.views.admin
import subjectdb.views.email
import subjectdb.views.experiments
import subjectdb.views.extradata
import subjectdb.views.resting
import subjectdb.views.session
import subjectdb.views.specialgroups
import subjectdb.views.subjectbase

app_name = 'subjectdb'

subject_patterns = ([], app_name)
experiment_pattern = ([], app_name)
specialgroups_pattern = ([], app_name)
extradata_pattern = ([], app_name)
resting_state_pattern = ([], app_name)
admin_patterns = ([], app_name)

experiment_pattern[0].extend(get_urls_from_module(subjectdb.views.experiments))
subject_patterns[0].extend(get_urls_from_module(subjectdb.views.subjectbase))
experiment_pattern[0].extend(get_urls_from_module(subjectdb.views.session))
experiment_pattern[0].extend(get_urls_from_module(subjectdb.views.email))
specialgroups_pattern[0].extend(get_urls_from_module(subjectdb.views.specialgroups))
extradata_pattern[0].extend(get_urls_from_module(subjectdb.views.extradata))
resting_state_pattern[0].extend(get_urls_from_module(subjectdb.views.resting))
admin_patterns[0].extend(get_urls_from_module(subjectdb.views.admin))

urlpatterns = [
    re_path(r'^subjects/', include(subject_patterns, namespace='subjects')),
    re_path(r'^experiments/', include(experiment_pattern, namespace='experiments')),
    re_path(r'^specialgroups/', include(specialgroups_pattern, namespace='specialgroups')),
    re_path(r'^extradata/', include(extradata_pattern, namespace='extradata')),
    re_path(r'^resting/', include(resting_state_pattern, namespace='resting')),
    re_path(r'^admin/', include(admin_patterns, namespace='admin')),
]
