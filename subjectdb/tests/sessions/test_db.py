# Copyright (c) 2016-2025, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project,
# see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.

import copy
import itertools

import django.test

import user_manager.tests.helpers
from subjectdb import models
from subjectdb.tests import base_infos
from subjectdb.tests.helpers import UITestMixin


class SessionsDBTests(django.test.TestCase, user_manager.tests.helpers.LoginLogoutMixin, UITestMixin):
    def test_excluded_sessions_age(self):
        exp_acronym = 'test'
        n_sessions = 5
        self.add_experiment_db(acronym=exp_acronym, description='test')

        for exclude_session, is_pilot in itertools.product({True, False}, {True, False}):
            self._setup_sessions(
                exclude_session=exclude_session, is_pilot=is_pilot, n_sessions=n_sessions, exp_acronym=exp_acronym
            )

        for exclude_excluded_sessions, exclude_pilot in itertools.product({True, False}, {True, False}):
            self._test_sessions(
                exclude_excluded_sessions=exclude_excluded_sessions,
                exclude_pilot=exclude_pilot,
                n_sessions=n_sessions,
                exp_acronym=exp_acronym,
            )

    def _setup_sessions(self, exclude_session, is_pilot, n_sessions, exp_acronym):
        this_info = copy.deepcopy(base_infos.session)
        this_info['pilot_session'] = is_pilot
        this_info['session_excluded'] = exclude_session
        if exclude_session:
            this_info['session_excluded_reason'] = 'test'
        self._create_sessions(exp_acronym=exp_acronym, n_sessions=n_sessions, db_direct=True, session_info=this_info)

    def _test_sessions(self, exclude_excluded_sessions, exclude_pilot, n_sessions, exp_acronym):
        cur_experiment = models.Experiment.objects.get(acronym=exp_acronym)
        this_n = n_sessions
        cur_sessions = cur_experiment.session_set.all()
        if exclude_pilot:
            cur_sessions = cur_sessions.filter(pilot_session=False)
        else:
            this_n *= 2

        if exclude_excluded_sessions:
            cur_sessions = cur_sessions.filter(excluded_from_experiment=False)
        else:
            this_n *= 2

        self.assertEqual(len(cur_sessions), this_n)

        all_ages = 0.0
        for sess in cur_sessions:
            all_ages += sess.subject_age()

        mean_age = all_ages / len(cur_sessions)
        self.assertEqual(
            mean_age,
            cur_experiment.mean_age(include_pilots=not exclude_pilot, include_excluded=not exclude_excluded_sessions),
        )
