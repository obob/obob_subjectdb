import datetime
from pathlib import Path

import django.utils.timezone
from django.conf import settings

import mridb.tests.helpers
import subjectdb.management.cronjobs
import th_django.test
import user_manager.tests.helpers
from subjectdb import models
from subjectdb.factories import SubjectBaseFactory
from subjectdb.tests import base_infos
from subjectdb.tests.helpers import ExtraAssertionMixin, UITestMixin
from th_django.templatetags.th_django import datetime_format


class SessionsUiTests(
    th_django.test.SeleniumTestCase,
    user_manager.tests.helpers.LoginLogoutMixin,
    UITestMixin,
    mridb.tests.helpers.UITestMixin,
    ExtraAssertionMixin,
):
    def test_add_session_new_subject(self):
        exp1 = base_infos.experiment.copy()
        exp2 = base_infos.experiment.copy()

        exp2['acronym'] = 'test_exp2'

        self.index()
        self.login('b1019547')
        self.add_user_db('thht', ['Lab Manager'])
        self.add_experiment_db(**exp1)
        self.add_experiment_db(**exp2)
        was_new_subject = self.add_session(exp_acronym=exp2['acronym'], **base_infos.session)

        experiment1 = models.Experiment.objects.get(acronym=exp1['acronym'])
        experiment2 = models.Experiment.objects.get(acronym=exp2['acronym'])
        session = experiment2.session_set.get()

        self.assertTrue(was_new_subject)

        self.assertEqual(
            session.datetime_measured.astimezone(django.utils.timezone.get_current_timezone()).strftime(
                datetime_format()
            ),
            base_infos.session['datetime_measurement']
            .astimezone(django.utils.timezone.get_current_timezone())
            .strftime(datetime_format()),
        )

        self.assertEqual(session.pilot_session, base_infos.session['pilot_session'])
        self.assertEqual(session.notes, base_infos.session['notes'])

        subject = session.subject
        self.assert_subject(subject, base_infos.subject)

        self.show_view('subjectdb.views.experiments.Experiments')

        self.assertEqual(experiment1.number_of_sessions(), 0)
        self.assertEqual(experiment2.number_of_sessions(), 1)

        new_session_info = base_infos.session.copy()
        new_session_info['notes'] = 'newnotes'
        new_session_info['pilot_session'] = True

        self.edit_session(exp_acronym=exp2['acronym'], session_id=session.id, new_session=new_session_info)

        new_session = experiment2.session_set.get()
        self.assertEqual(new_session.pilot_session, new_session_info['pilot_session'])
        self.assertEqual(new_session.notes, new_session_info['notes'])

    def test_add_session_exisiting_subject(self):
        exp1 = base_infos.experiment.copy()
        exp2 = base_infos.experiment.copy()

        exp2['acronym'] = 'test_exp2'

        self.index()
        self.add_user_db('thht', ['Lab Manager'])
        self.login('b1019547')
        self.add_experiment_db(**exp1)
        self.add_experiment_db(**exp2)
        self.add_subject_db(**base_infos.subject)
        was_new_subject = self.add_session(exp_acronym=exp1['acronym'], **base_infos.session)

        experiment1 = models.Experiment.objects.get(acronym=exp1['acronym'])
        experiment2 = models.Experiment.objects.get(acronym=exp2['acronym'])
        session = experiment1.session_set.get()

        self.assertFalse(was_new_subject)

        self.assertEqual(
            session.datetime_measured.astimezone(django.utils.timezone.get_current_timezone()).strftime(
                datetime_format()
            ),
            base_infos.session['datetime_measurement']
            .astimezone(django.utils.timezone.get_current_timezone())
            .strftime(datetime_format()),
        )

        self.assertEqual(session.pilot_session, base_infos.session['pilot_session'])
        self.assertEqual(session.notes, base_infos.session['notes'])

        subject = session.subject
        self.assert_subject(subject, base_infos.subject)

        self.show_view('subjectdb.views.experiments.Experiments')

        self.assertEqual(experiment1.number_of_sessions(), 1)
        self.assertEqual(experiment2.number_of_sessions(), 0)

    def test_add_session_exisiting_incomplete_subject(self):
        exp1 = base_infos.experiment.copy()
        exp2 = base_infos.experiment.copy()

        exp2['acronym'] = 'test_exp2'

        self.index()
        self.add_user_db('thht', ['Lab Manager'])
        self.login('b1019547')
        self.add_experiment_db(**exp1)
        self.add_experiment_db(**exp2)

        SubjectBaseFactory(
            mother_first_name=base_infos.subject['mother_first_name'],
            mother_maiden_name=base_infos.subject['mother_maiden_name'],
            birthday=base_infos.subject['birthday'],
            gender='',
            handedness='',
        )

        was_new_subject = self.add_session(exp_acronym=exp1['acronym'], **base_infos.session)

        experiment1 = models.Experiment.objects.get(acronym=exp1['acronym'])
        experiment2 = models.Experiment.objects.get(acronym=exp2['acronym'])
        session = experiment1.session_set.get()

        self.assertFalse(was_new_subject)

        self.assertEqual(
            session.datetime_measured.astimezone(django.utils.timezone.get_current_timezone()).strftime(
                datetime_format()
            ),
            base_infos.session['datetime_measurement']
            .astimezone(django.utils.timezone.get_current_timezone())
            .strftime(datetime_format()),
        )

        self.assertEqual(session.pilot_session, base_infos.session['pilot_session'])
        self.assertEqual(session.notes, base_infos.session['notes'])

        subject = session.subject
        self.assert_subject(subject, base_infos.subject)

        self.show_view('subjectdb.views.experiments.Experiments')

        self.assertEqual(experiment1.number_of_sessions(), 1)
        self.assertEqual(experiment2.number_of_sessions(), 0)

    def test_session_delete_and_ownership(self):
        exp1 = base_infos.experiment.copy()
        exp1['additional_investigators'] = ['thht3']

        session1 = base_infos.session.copy()
        session2 = base_infos.session.copy()
        session2['notes'] = 'Session2'

        self.index()
        self.add_user_db('thht', ['Lab Manager'])
        self.add_user_db('thht2', ['Access SubjectDB', 'Investigator/Technical Staff'])
        self.add_user_db('thht3', ['Access SubjectDB', 'Investigator/Technical Staff'])
        self.add_user_db('thht4', ['Access SubjectDB', 'Investigator/Technical Staff'])

        self.login('thht2')
        self.add_experiment_db(**exp1)
        self.add_session(exp_acronym=exp1['acronym'], **session1)

        self.logout()
        self.login('thht3')
        self.add_session(exp_acronym=exp1['acronym'], **session2)

        session1_id = (
            models.Experiment.objects.get(acronym=exp1['acronym']).session_set.all().get(notes=session1['notes']).id
        )
        session2_id = (
            models.Experiment.objects.get(acronym=exp1['acronym']).session_set.all().get(notes=session2['notes']).id
        )

        self._test_delete_undelete_session(exp1['acronym'], session1_id, True)
        self._test_delete_undelete_session(exp1['acronym'], session2_id, False)

        self.logout()
        self.login('thht2')
        self._test_delete_undelete_session(exp1['acronym'], session1_id, False)
        self._test_delete_undelete_session(exp1['acronym'], session2_id, False)

        self.logout()
        self.login('b1019547')
        self._test_delete_undelete_session(exp1['acronym'], session1_id, False)
        self._test_delete_undelete_session(exp1['acronym'], session2_id, False)

    def test_empty_trash(self):
        exp_acronym = base_infos.experiment['acronym']

        self.index()
        self.login('b1019547')
        self.add_user_db('thht')
        self.add_experiment_db(**base_infos.experiment)
        (sessions, session_ids) = self._create_sessions(exp_acronym, 3)

        self.delete_session(exp_acronym, session_ids[0])
        self.delete_session(exp_acronym, session_ids[1])

        for cur_session in sessions:
            cur_session.refresh_from_db()

        sessions[1].datetime_deleted = sessions[1].datetime_deleted - datetime.timedelta(days=31)
        sessions[1].save()

        subjectdb.management.cronjobs.empty_trash()

        models.Session.objects.get(id=session_ids[0])
        models.Session.objects.get(id=session_ids[2])

        with self.assertRaises(models.Session.DoesNotExist):
            models.Session.objects.get(id=session_ids[1])

    def test_link_to_mri(self):
        test_mri_dir = Path(settings.BASE_DIR) / 'test_datasets'

        self.index()
        self.login('b1019547')
        self.add_user_db('thht')
        subject_id = '19800908igdb'
        self.add_mri_subject_db(subject_id)
        self.upload_mri(subject_id, test_mri_dir / 'nifti_sbg' / 'test_nifti.nii')

        self.add_subject_db(**base_infos.subject)
        self.add_experiment_db(**base_infos.experiment)
        self.add_session(base_infos.experiment['acronym'], **base_infos.session)
        self.wait_for_table()
        self.assertIsNotNone(self.find_element_by_partial_href(f'mridb/SubjectDetail/{subject_id}'))

    def _test_delete_undelete_session(self, exp_acronym, session_id, should_fail):
        self.show_view('subjectdb.views.experiments.ExperimentDetail', kwargs={'acronym': exp_acronym})
        self.wait_for_table()
        if should_fail:
            self.assertEqual(
                [
                    x
                    for x in self.find_elements_by_text('Delete')
                    if x.tag_name == 'button' and x.get_attribute('data-subject') == str(session_id)
                ][0].get_attribute('disabled'),
                'true',
            )
        else:
            self.assertIsNone(
                [
                    x
                    for x in self.find_elements_by_text('Delete')
                    if x.tag_name == 'button' and x.get_attribute('data-subject') == str(session_id)
                ][0].get_attribute('disabled')
            )

        self.delete_session(exp_acronym, session_id, should_fail)
        if should_fail:
            self.assertIsNotNone(
                self.find_element_by_text('You cannot view this page because you do not have the necessary permission.')
            )
            self.assertFalse(models.Session.objects.get(id=session_id).deleted)
        else:
            self.assertTrue(models.Session.objects.get(id=session_id).deleted)

        tmp_session = models.Session.objects.get(id=session_id)
        tmp_session.deleted = True
        tmp_session.save()

        self.show_view('subjectdb.views.session.SessionTrash', kwargs={'experiment_acronym': exp_acronym})
        self.wait_for_table()
        if should_fail:
            self.assertEqual(
                [
                    x
                    for x in self.find_elements_by_text('Undelete')
                    if x.tag_name == 'button' and x.get_attribute('id') == str(session_id)
                ][0].get_attribute('disabled'),
                'true',
            )
        else:
            self.assertIsNone(
                [
                    x
                    for x in self.find_elements_by_text('Undelete')
                    if x.tag_name == 'button' and x.get_attribute('id') == str(session_id)
                ][0].get_attribute('disabled')
            )

        self.undelete_session(exp_acronym, session_id, should_fail)
        if should_fail:
            self.assertIsNotNone(
                self.find_element_by_text('You cannot view this page because you do not have the necessary permission.')
            )
            self.assertTrue(models.Session.objects.get(id=session_id).deleted)
        else:
            self.wait_for_table()
            self.assertFalse(models.Session.objects.get(id=session_id).deleted)

        tmp_session = models.Session.objects.get(id=session_id)
        tmp_session.deleted = False
        tmp_session.save()
