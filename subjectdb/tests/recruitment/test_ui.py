# Copyright (c) 2016-2025, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project,
# see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.
import itertools
import random
import time

import factory
import faker
from django.core import mail
from django.test import override_settings
from dynamic_preferences.registries import global_preferences_registry
from parameterized import parameterized
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC  # noqa N812
from selenium.webdriver.support.select import Select

import mridb.tests.helpers
import th_django.test
import user_manager.tests.helpers
from subjectdb import models
from subjectdb.factories import (
    ExperimentFactory,
    SessionFactory,
    SpecialSubjectGroupFactory,
    SubjectBaseFactory,
    SubjectPersonalInformationFactory,
)
from subjectdb.tests.helpers import ExtraAssertionMixin, UITestMixin


def parameterize_test_send_simple_email():
    handedness = (None, ('Right',), ('Right', 'Left'))
    has_mri = (None, True, False)
    experiments = (0, 1)
    has_done_other_exp = (True, False)
    age_range = (None, (20, 50))
    special_groups = (True, False)
    params1 = itertools.product(
        experiments, handedness, has_mri[0:1], has_done_other_exp[0:1], age_range[0:1], special_groups[1:2]
    )
    params2 = itertools.product(
        experiments[0:1], handedness[0:1], has_mri[0:1], has_done_other_exp[0:1], age_range, special_groups[1:2]
    )
    params3 = itertools.product(
        experiments[0:1], handedness[0:1], has_mri[0:1], has_done_other_exp[0:1], age_range[0:1], special_groups
    )
    params4 = itertools.product(
        experiments[0:1], handedness[0:1], has_mri, has_done_other_exp[0:1], age_range[0:1], special_groups[1:2]
    )
    params5 = itertools.product(
        experiments[0:1], handedness[0:1], has_mri[0:1], has_done_other_exp, age_range[0:1], special_groups[1:2]
    )
    all_params = itertools.chain(params1, params2, params3, params4, params5)

    return all_params


class RecruitmentTests(
    th_django.test.SeleniumTestCase,
    user_manager.tests.helpers.LoginLogoutMixin,
    UITestMixin,
    mridb.tests.helpers.UITestMixin,
    ExtraAssertionMixin,
):
    def setUp(self):
        self.faker = faker.Faker()

        self.index()
        self.login_fast('b1019547')
        self.index()

        exp1 = ExperimentFactory(added_by=self.get_current_user())
        exp2 = ExperimentFactory(added_by=self.get_current_user())

        self.special_groups = SpecialSubjectGroupFactory.create_batch(2)

        self.experiments = [exp1, exp2]

        n_subjects = 100
        handedness = ('Right', 'Left', 'Ambidexter')
        has_mri = (True, False)
        has_personal_info = (True, False)
        available_for_exp = (True, False)
        special_group = (None, [self.special_groups[0]], [self.special_groups[1]])
        exp = (None, exp1, exp2)

        all_subjects_description = {
            'handedness': random.choices(handedness, k=n_subjects),
            'has_mri': random.choices(has_mri, k=n_subjects),
            'has_personal_info': random.choices(has_personal_info, k=n_subjects),
            'available_for_exp': random.choices(available_for_exp, k=n_subjects),
            'special_group': random.choices(special_group, k=n_subjects, weights=[0.8, 0.1, 0.1]),
            'exp': random.choices(exp, k=n_subjects),
        }

        all_subjects = SubjectBaseFactory.create_batch(
            n_subjects,
            handedness=factory.Iterator(all_subjects_description['handedness']),
            available_for_experiments=factory.Iterator(all_subjects_description['available_for_exp']),
            consent_personal_info=factory.Iterator(all_subjects_description['has_personal_info']),
            special_groups=factory.Iterator(all_subjects_description['special_group']),
        )

        subjects_with_personal_info = [
            s for s, b in zip(all_subjects, all_subjects_description['has_personal_info']) if b
        ]  # noqa

        SubjectPersonalInformationFactory.create_batch(
            len(subjects_with_personal_info), parent_subject=factory.Iterator(subjects_with_personal_info)
        )

        final_exp = [e for e in all_subjects_description['exp'] if e is not None]  # noqa
        subjects_for_e = [s for s, e in zip(all_subjects, all_subjects_description['exp']) if e is not None]  # noqa

        SessionFactory.create_batch(
            len(final_exp), experiment=factory.Iterator(final_exp), subject=factory.Iterator(subjects_for_e)
        )

        subjects_for_mri = [s for s, m in zip(all_subjects, all_subjects_description['has_mri']) if m]  # noqa

        for s in subjects_for_mri:
            self.add_mri_subject_db(s.subject_id)

        g_preferences = global_preferences_registry.manager()
        g_preferences['recruitmentemail__recruitment_email_footer'] = """
footer_sender_email = {{ sender_email }}
footer_sender_full_name = {{ sender_full_name }}
"""

    @parameterized.expand(parameterize_test_send_simple_email)
    @override_settings(MAX_RECRUIT_SUBJECTS_PER_EMAIL=5000)
    def test_send_simple_email(self, exp_pk, handedness, has_mri, has_done_other_exp, age_range, special_groups):
        all_subjects = models.SubjectBase.objects.all()
        exp = self.experiments[exp_pk]
        other_exp = models.Experiment.objects.exclude(pk=self.experiments[exp_pk].pk).first()

        self.show_view('subjectdb.views.email.Recruit', kwargs={'experiment_pk': exp.pk})
        self.wait_for_table()
        time.sleep(0.1)
        page_html = self.selenium.page_source

        for cur_subject in all_subjects:
            should_be_visible = (
                cur_subject.has_personal_information
                and cur_subject.consent_personal_info
                and cur_subject.available_for_experiments
                and exp not in cur_subject.experiments
                and not cur_subject.special_groups.exists()
            )

            if should_be_visible:
                assert cur_subject.subject_id in page_html
            else:
                assert cur_subject.subject_id not in page_html

        if handedness is not None:
            sel = Select(self.selenium.find_element_by_id('id_handedness'))
            for h in handedness:
                sel.select_by_visible_text(h)

        if has_mri is not None:
            sel = Select(self.selenium.find_element_by_id('id_mri_available'))
            sel_text = 'Yes'
            if not has_mri:
                sel_text = 'No'

            sel.select_by_visible_text(sel_text)

        if has_done_other_exp:
            sel = Select(self.selenium.find_element_by_id('id_is_subject_in'))
            sel.select_by_visible_text(other_exp.acronym)

        if age_range is not None:
            self.selenium.find_element_by_id('id_age_0').send_keys(f'{age_range[0]:d}')
            self.selenium.find_element_by_id('id_age_1').send_keys(f'{age_range[1]:d}')

        if special_groups:
            sel = Select(self.selenium.find_element_by_id('id_special_groups'))
            sel.select_by_visible_text(self.special_groups[0].group_name)

        self.selenium.find_element_by_id('id_filterbutton').click()
        self.wait_until(EC.presence_of_element_located((By.XPATH, ".//*[contains(text(), 'Compose Email')]")))
        time.sleep(0.1)
        page_html = self.selenium.page_source
        self.wait_until(EC.presence_of_element_located((By.LINK_TEXT, 'Compose Email')))

        visible_subjects = []
        for cur_subject in all_subjects:
            should_be_visible = (
                cur_subject.has_personal_information
                and cur_subject.consent_personal_info
                and cur_subject.available_for_experiments
                and exp not in cur_subject.experiments
            )

            if handedness is not None:
                should_be_visible = should_be_visible and cur_subject.handedness in handedness

            if has_mri is not None:
                should_be_visible = should_be_visible and cur_subject.has_mri() == has_mri

            if has_done_other_exp:
                other_exp_acronyms = cur_subject.experiments.values_list('acronym', flat=True)
                should_be_visible = should_be_visible and other_exp.acronym in other_exp_acronyms

            if age_range is not None:
                should_be_visible = should_be_visible and cur_subject.age() >= age_range[0]
                should_be_visible = should_be_visible and cur_subject.age() <= age_range[1]

            this_special_groups = cur_subject.special_groups.values_list('group_name', flat=True)

            should_be_visible = should_be_visible and self.special_groups[1].group_name not in this_special_groups

            if special_groups:
                should_be_visible = should_be_visible and self.special_groups[0].group_name in this_special_groups
            else:
                should_be_visible = (
                    should_be_visible and self.special_groups[0].group_name not in this_special_groups  # noqa
                )

            if should_be_visible:
                assert cur_subject.subject_id in page_html
                visible_subjects.append(cur_subject)
            else:
                assert cur_subject.subject_id not in page_html

        self.run_script('document.getElementById("id_compose_email").click()')
        self.wait_until_visible(By.ID, 'id_subject')
        self.selenium.find_element_by_id('id_subject').send_keys(self.faker.sentence())
        self.selenium.find_element_by_id('id_content').send_keys(self.faker.paragraph())
        self.selenium.find_element_by_id('form').submit()
        time.sleep(0.1)
        self.selenium.find_element_by_id('form').submit()

        assert self.find_element_by_text(
            f'Email sent successfully to {len(visible_subjects)} participants of {exp.acronym}.'
        )
        assert len(mail.outbox) == len(visible_subjects) + 1
        for cur_mail in mail.outbox[:-1]:
            cur_subject = models.SubjectBase.objects.get(subjectpersonalinformation__email=cur_mail.to[0])
            assert cur_subject.has_personal_information
            assert cur_subject.consent_personal_info
            assert cur_subject.available_for_experiments
            assert exp not in cur_subject.experiments
            if handedness is not None:
                assert cur_subject.handedness in handedness

            if has_mri is not None:
                assert cur_subject.has_mri() == has_mri

            if has_done_other_exp:
                assert other_exp in cur_subject.experiments

            if age_range is not None:
                assert cur_subject.age() >= age_range[0]
                assert cur_subject.age() <= age_range[1]

            this_special_groups = cur_subject.special_groups.values_list('group_name', flat=True)
            assert self.special_groups[1].group_name not in this_special_groups

            if special_groups:
                assert self.special_groups[0].group_name in this_special_groups  # noqa
            else:
                assert self.special_groups[0].group_name not in this_special_groups  # noqa

        mail.outbox = []

    def test_save_draft(self):
        exp = self.experiments[0]
        self.show_view('subjectdb.views.email.Recruit', kwargs={'experiment_pk': exp.pk})
        self.wait_for_table()
        self.assert_element_not_found('id_reuse_draft', by=By.ID)
        self.click_with_js('#id_compose_email')

        subject_text = self.faker.sentence()
        content_text = self.faker.paragraph()

        self.selenium.find_element_by_id('id_subject').send_keys(subject_text)
        self.selenium.find_element_by_id('id_content').send_keys(content_text)

        time.sleep(1)

        self.show_view('subjectdb.views.email.Recruit', kwargs={'experiment_pk': exp.pk})
        self.wait_for_table()
        self.selenium.find_element_by_id('id_reuse_draft')
        self.click_with_js('#id_reuse_draft')

        assert self.selenium.find_element_by_id('id_subject').get_attribute('value') == subject_text
        assert content_text in self.selenium.find_element_by_id('id_content').get_attribute('value')

    def test_check_email_stage(self):
        exp = self.experiments[0]
        self.show_view('subjectdb.views.email.Recruit', kwargs={'experiment_pk': exp.pk})
        self.wait_for_table()
        self.assert_element_not_found('id_reuse_draft', by=By.ID)
        self.click_with_js('#id_compose_email')

        subject_text = self.faker.sentence()
        content_text = self.faker.paragraph()

        self.wait_until_visible(By.ID, 'id_subject')

        self.selenium.find_element_by_id('id_subject').send_keys(subject_text)
        self.selenium.find_element_by_id('id_content').send_keys(content_text)

        self.selenium.find_element_by_id('form').submit()
        assert self.find_element_by_text(f'Check Email for Experiment {exp.acronym}')

        subject_element = self.selenium.find_element_by_id('id_subject')
        content_element = self.selenium.find_element_by_id('id_content')

        assert subject_element.get_attribute('value') == subject_text
        assert content_text in content_element.get_attribute('value')
        assert subject_element.get_property('disabled')
        assert content_element.get_property('disabled')

        self.selenium.find_element_by_id('id_edit_email')
        self.click_with_js('#id_edit_email')

        assert self.find_element_by_text(f'Compose Email for Experiment {exp.acronym}')

        subject_element = self.selenium.find_element_by_id('id_subject')
        content_element = self.selenium.find_element_by_id('id_content')

        assert subject_element.get_attribute('value') == subject_text
        assert content_text in content_element.get_attribute('value')
        assert not subject_element.get_property('disabled')
        assert not content_element.get_property('disabled')

        self.selenium.find_element_by_id('form').submit()
        subject_element = self.selenium.find_element_by_id('id_subject')
        content_element = self.selenium.find_element_by_id('id_content')

        assert subject_element.get_attribute('value') == subject_text
        assert content_text in content_element.get_attribute('value')
        assert subject_element.get_property('disabled')
        assert content_element.get_property('disabled')

        self.selenium.find_element_by_id('form').submit()

        assert len(mail.outbox) > 0

        self.show_view('subjectdb.views.email.Recruit', kwargs={'experiment_pk': exp.pk})
        self.wait_for_table()
        self.assert_element_not_found('id_reuse_draft', by=By.ID)

    def test_reuse_email_and_filtersetting(self):
        exp = self.experiments[0]
        self.show_view('subjectdb.views.email.Recruit', kwargs={'experiment_pk': exp.pk})
        self.wait_for_table()
        time.sleep(0.1)

        sel = Select(self.selenium.find_element_by_id('id_mri_available'))
        sel.select_by_visible_text('Yes')
        sel = Select(self.selenium.find_element_by_id('id_handedness'))
        sel.select_by_visible_text('Right')
        sel.select_by_visible_text('Left')
        self.selenium.find_element_by_id('id_filterbutton').click()
        self.wait_until(EC.presence_of_element_located((By.XPATH, ".//*[contains(text(), 'Compose Email')]")))
        self.click_with_js('#id_compose_email')

        subject_text = self.faker.sentence()
        content_text = self.faker.paragraph()

        self.wait_until_visible(By.ID, 'id_subject')

        self.selenium.find_element_by_id('id_subject').send_keys(subject_text)
        self.selenium.find_element_by_id('id_content').send_keys(content_text)

        self.selenium.find_element_by_id('form').submit()
        time.sleep(0.1)
        self.selenium.find_element_by_id('form').submit()

        self.show_view('subjectdb.views.email.Recruit', kwargs={'experiment_pk': exp.pk})
        self.wait_for_table()
        time.sleep(0.1)

        sel = Select(self.selenium.find_element_by_id('id_handedness'))
        all_handedness_selected = [s.text for s in sel.all_selected_options]
        assert 'Right' in all_handedness_selected
        assert 'Left' in all_handedness_selected

        sel = Select(self.selenium.find_element_by_id('id_mri_available'))
        assert sel.all_selected_options[0].text == 'Yes'
        sel.select_by_visible_text('No')

        self.selenium.find_element_by_id('id_filterbutton').click()
        self.wait_until(EC.presence_of_element_located((By.XPATH, ".//*[contains(text(), 'Compose Email')]")))

        sel = Select(self.selenium.find_element_by_id('id_handedness'))
        all_handedness_selected = [s.text for s in sel.all_selected_options]
        assert 'Right' in all_handedness_selected
        assert 'Left' in all_handedness_selected

        sel = Select(self.selenium.find_element_by_id('id_mri_available'))
        assert sel.all_selected_options[0].text == 'No'

        self.click_with_js('#id_compose_email')

        subject_element = self.selenium.find_element_by_id('id_subject')
        content_element = self.selenium.find_element_by_id('id_content')

        assert subject_element.get_attribute('value') == subject_text
        assert content_element.get_attribute('value') == content_text

    @override_settings(MAX_RECRUIT_SUBJECTS_PER_EMAIL=5)
    def test_limit_email_sending_and_exclude_contacted(self):
        exp = self.experiments[0]
        self.show_view('subjectdb.views.email.Recruit', kwargs={'experiment_pk': exp.pk})
        self.wait_for_table()
        self.assert_element_not_found('id_reuse_draft', by=By.ID)
        self.click_with_js('#id_compose_email')

        subject_text = self.faker.sentence()
        content_text = self.faker.paragraph()

        self.wait_until_visible(By.ID, 'id_subject')

        self.selenium.find_element_by_id('id_subject').send_keys(subject_text)
        self.selenium.find_element_by_id('id_content').send_keys(content_text)

        self.selenium.find_element_by_id('form').submit()
        assert self.find_element_by_text(f'Check Email for Experiment {exp.acronym}')
        self.selenium.find_element_by_id('form').submit()
        assert len(mail.outbox) == 6  # noqa PLR2004

        subjects_written = models.RecruitmentEmail.objects.get(status='sent').recipients.all()

        exp = self.experiments[0]
        self.show_view('subjectdb.views.email.Recruit', kwargs={'experiment_pk': exp.pk})
        self.wait_for_table()
        time.sleep(0.1)
        page_html = self.selenium.page_source

        for cur_subject in subjects_written:
            assert cur_subject.subject_id not in page_html

    def test_exposed_items(self):
        exp = self.experiments[0]
        self.show_view('subjectdb.views.email.Recruit', kwargs={'experiment_pk': exp.pk})
        self.wait_for_table()
        self.assert_element_not_found('id_reuse_draft', by=By.ID)
        self.click_with_js('#id_compose_email')

        subject_text = self.faker.sentence()
        content_text = 'Full Name: {{ full_name }}'

        self.wait_until_visible(By.ID, 'id_subject')

        self.selenium.find_element_by_id('id_subject').send_keys(subject_text)
        self.selenium.find_element_by_id('id_content').send_keys(content_text)

        self.selenium.find_element_by_id('form').submit()

        this_content_text = self.selenium.find_element_by_id('id_content').get_attribute('value').splitlines()  # noqa
        assert this_content_text[0].startswith('Full Name: ')
        name_split = this_content_text[0].split(' ')[2:]
        assert models.SubjectBase.objects.filter(
            subjectpersonalinformation__first_name=name_split[0],
            subjectpersonalinformation__last_name=name_split[1],
        ).exists()

        self.selenium.find_element_by_id('form').submit()

        for cur_mail in mail.outbox[:-1]:
            this_body_lines = cur_mail.body.splitlines()
            assert this_body_lines[0].startswith('Full Name: ')
            name_split = this_body_lines[0].split(' ')[2:]
            assert f'footer_sender_email = {self.get_current_user().email}' in this_body_lines[3]
            assert (
                f'footer_sender_full_name = {self.get_current_user().first_name} {self.get_current_user().last_name}'
                in this_body_lines[4]
            )
            assert models.SubjectBase.objects.filter(
                subjectpersonalinformation__first_name=name_split[0],
                subjectpersonalinformation__last_name=name_split[1],
            ).exists()
