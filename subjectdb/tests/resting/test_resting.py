#  Copyright (c) 2016-2025, Thomas Hartmann
#
#  This file is part of the OBOB Subject Database Project,
#  see: https://gitlab.com/obob/obob_subjectdb/
#
#   obob_subjectdb is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   obob_subjectdb is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.
import datetime
import itertools
import re
import tempfile
import warnings
from collections import namedtuple
from pathlib import Path

import mne
from dynamic_preferences.registries import global_preferences_registry
from openpyxl import load_workbook
from parameterized import parameterized
from selenium.webdriver.support.select import Select

import subjectdb.management.cronjobs
import subjectdb.models
import th_django.test
import user_manager.tests.helpers
from obob_subjectdb.tests.helpers import UITestMixin
from subjectdb.factories.experiment import ExperimentFactory
from subjectdb.factories.fif import RawFif
from subjectdb.factories.session import SessionFactory
from subjectdb.factories.subject import SubjectBaseFactory

RestingStateTestData = namedtuple(
    'RestingStateTestData',
    [
        'tmp_path',
        'subjects_in_db',
        'subjects_not_in_db',
        'experiments_in_db',
        'experiments_not_in_db',
        'all_sessions',
        'all_fifs',
    ],
)


class RestingStateData:
    def __init__(
        self,
        n_subjects_in_db=0,
        n_subjects_not_in_db=0,
        n_experiments_in_db=0,
        n_experiments_not_in_db=0,
        current_user=None,
        suffixes=None,
        n_sessions_in_db=1,
        n_sessions_not_in_db=1,
        tmp_path=None,
        dewar_positions=None,
        s_rate=None,
    ):
        self.n_subjects_in_db = n_subjects_in_db
        self.n_subjects_not_in_db = n_subjects_not_in_db
        self.n_experiments_in_db = n_experiments_in_db
        self.n_experiments_not_in_db = n_experiments_not_in_db
        self.current_user = current_user
        self.suffixes = suffixes
        self.n_sessions_in_db = n_sessions_in_db
        self.n_sessions_not_in_db = n_sessions_not_in_db
        self.dewar_position = dewar_positions
        self.s_rate = s_rate

        if current_user is None:
            raise RuntimeError('current_user cannot be None')

        if tmp_path is None:
            self.tmp_path = tempfile.TemporaryDirectory()
            self.cleanup_tmp = True
        else:
            self.tmp_path = tmp_path
            self.cleanup_tmp = False

        if self.suffixes is None:
            self.suffixes = ['rest', 'REST', 'resting', 'RESTING', None, 'run_01', 'block_02']

        if self.dewar_position is None:
            self.dewar_position = [69]

        if self.s_rate is None:
            self.s_rate = [1000]

    def __enter__(self):
        self.subjects_in_db = SubjectBaseFactory.create_batch(self.n_subjects_in_db)
        subjects_not_in_db = SubjectBaseFactory.build_batch(self.n_subjects_not_in_db)

        all_subjects = self.subjects_in_db + subjects_not_in_db

        self.experiments_in_db = ExperimentFactory.create_batch(self.n_experiments_in_db, added_by=self.current_user)
        experiments_not_in_db = ExperimentFactory.build_batch(self.n_experiments_not_in_db, added_by=self.current_user)

        all_experiments = self.experiments_in_db + experiments_not_in_db

        self.all_sessions = []
        self.all_fifs = []

        for cur_sub, cur_exp, cur_srate, cur_dewar_position in itertools.product(  # noqa
            all_subjects, all_experiments, self.s_rate, self.dewar_position
        ):
            saved_sessions = [True] * self.n_sessions_in_db + [False] * self.n_sessions_not_in_db  # noqa

            for cur_s in saved_sessions:
                kwargs = {}
                cur_session = SessionFactory.build(
                    experiment=cur_exp,
                    subject=cur_sub,
                )
                if cur_s:
                    try:
                        if cur_sub.id and cur_exp.id:
                            cur_session.save()
                    except ValueError:
                        pass

                self.all_sessions.append(cur_session)
                kwargs['meas_date'] = cur_session.datetime_measured

                for cur_suffix in self.suffixes:
                    fif = RawFif(
                        save_folder=self.tmp_path.name,
                        subject_object=cur_sub,
                        project_name=cur_exp.acronym,
                        f_suffix=cur_suffix,
                        sfreq=cur_srate,
                        dewar_position=cur_dewar_position,
                        **kwargs,
                    )
                    fif.save()
                    self.all_fifs.append(fif)

        return RestingStateTestData(
            self.tmp_path,
            self.subjects_in_db,
            subjects_not_in_db,
            self.experiments_in_db,
            experiments_not_in_db,
            self.all_sessions,
            self.all_fifs,
        )

    def __exit__(self, exc_type, exc_val, exc_tb):
        for o in self.experiments_in_db:
            o.delete()
        for o in self.subjects_in_db:
            o.delete()
        for o in self.all_sessions:
            if o.id:
                o.delete()
        for o in self.all_fifs:
            o.filenames[0].unlink()

        if self.cleanup_tmp:
            self.tmp_path.cleanup()


class RestingStateTests(th_django.test.SeleniumTestCase, user_manager.tests.helpers.LoginLogoutMixin, UITestMixin):
    def test_context_manager_factory(self):
        self.index()
        self.login_fast('b1019547')
        self.index()

        n_subjects_in_db = 10
        n_subject_not_in_db = 5

        n_experiments_in_db = 1
        n_experiments_not_in_db = 2

        resting_state_suffixes = ['rest', 'REST', 'resting', 'RESTING']
        other_suffixes = [None, 'run_01', 'block_02']

        all_suffixes = resting_state_suffixes + other_suffixes

        with RestingStateData(
            n_subjects_in_db,
            n_subject_not_in_db,
            n_experiments_in_db,
            n_experiments_not_in_db,
            self.get_current_user(),
            all_suffixes,
        ) as r_data:
            n_fifs = (
                (n_subjects_in_db + n_subject_not_in_db)
                * (n_experiments_in_db + n_experiments_not_in_db)
                * (len(resting_state_suffixes) + len(other_suffixes))
                * 2
            )
            assert len(r_data.all_fifs) == n_fifs
            all_fifs_from_glob = list(Path(r_data.tmp_path.name).glob('**/*.fif'))
            assert len(all_fifs_from_glob) == n_fifs

            for exp in r_data.experiments_in_db:
                assert exp.id is not None
                assert Path(r_data.tmp_path.name, exp.acronym).exists()

            for exp in r_data.experiments_not_in_db:
                assert exp.id is None
                assert Path(r_data.tmp_path.name, exp.acronym).exists()

            for sub in r_data.subjects_in_db:
                assert sub.id is not None
                all_s_files = Path(r_data.tmp_path.name).glob(f'**/{sub.subject_id}*.fif')
                assert len(list(all_s_files)) == (n_experiments_in_db + n_experiments_not_in_db) * len(all_suffixes) * 2  # noqa

            for sub in r_data.subjects_not_in_db:
                assert sub.id is None
                all_s_files = Path(r_data.tmp_path.name).glob(f'**/{sub.subject_id}*.fif')
                assert len(list(all_s_files)) == (n_experiments_in_db + n_experiments_not_in_db) * len(all_suffixes) * 2  # noqa

            with RestingStateData(
                n_subjects_in_db=2,
                n_experiments_in_db=1,
                n_sessions_in_db=1,
                n_sessions_not_in_db=0,
                suffixes=['rest'],
                current_user=self.get_current_user(),
                tmp_path=r_data.tmp_path,
            ):
                all_fifs_from_glob = list(Path(r_data.tmp_path.name).glob('**/*.fif'))
                assert len(all_fifs_from_glob) == n_fifs + 2

            all_fifs_from_glob = list(Path(r_data.tmp_path.name).glob('**/*.fif'))
            assert len(all_fifs_from_glob) == n_fifs

        assert subjectdb.models.SubjectBase.objects.count() == 0
        assert subjectdb.models.Experiment.objects.count() == 0
        assert subjectdb.models.Session.objects.count() == 0

    def test_management_command(self):
        self.index()
        self.login_fast('b1019547')
        self.index()

        n_subjects_in_db = 2
        n_subject_not_in_db = 2

        n_experiments_in_db = 1
        n_experiments_not_in_db = 1

        resting_state_suffixes = ['rest', 'REST', 'resting', 'RESTING']
        other_suffixes = [None, 'run_01', 'block_02', 'sss_rest', 'sss_RESTING']

        all_suffixes = resting_state_suffixes + other_suffixes

        n_resting = (
            len(resting_state_suffixes)
            * (n_subjects_in_db + n_subject_not_in_db)
            * (n_experiments_in_db + n_experiments_not_in_db)
        )

        with (
            RestingStateData(
                n_subjects_in_db,
                n_subject_not_in_db,
                n_experiments_in_db,
                n_experiments_not_in_db,
                self.get_current_user(),
                all_suffixes,
                n_sessions_in_db=1,
                n_sessions_not_in_db=0,
            ) as r_data,
            self.settings(RAW_MEG_DIR=r_data.tmp_path.name),
        ):
            subjectdb.management.cronjobs.gather_resting_state_data()

            assert subjectdb.models.RestingStateSession.objects.count() == n_resting  # noqa
            assert subjectdb.models.SubjectBase.objects.count() == n_subjects_in_db + n_subject_not_in_db  # noqa

            for tmp_s in r_data.subjects_not_in_db:
                real_s = subjectdb.models.SubjectBase.objects.get(subject_id=tmp_s.subject_id)
                assert real_s.has_all_info is False

            for tmp_s in r_data.subjects_in_db:
                real_s = subjectdb.models.SubjectBase.objects.get(subject_id=tmp_s.subject_id)
                assert real_s.has_all_info

            with RestingStateData(
                n_subjects_in_db=2,
                n_experiments_in_db=1,
                n_sessions_in_db=1,
                n_sessions_not_in_db=0,
                suffixes=['rest'],
                current_user=self.get_current_user(),
                tmp_path=r_data.tmp_path,
            ):
                subjectdb.management.cronjobs.gather_resting_state_data()
                assert subjectdb.models.RestingStateSession.objects.count() == n_resting + 2  # noqa

    def test_view(self):
        self.index()
        self.login_fast('b1019547')
        self.index()

        n_subjects_in_db = 2
        n_subject_not_in_db = 2

        n_experiments_in_db = 1
        n_experiments_not_in_db = 1

        resting_state_suffixes = ['rest', 'REST', 'resting', 'RESTING']
        other_suffixes = [None, 'run_01', 'block_02']

        all_suffixes = resting_state_suffixes + other_suffixes

        with (
            RestingStateData(
                n_subjects_in_db,
                n_subject_not_in_db,
                n_experiments_in_db,
                n_experiments_not_in_db,
                self.get_current_user(),
                all_suffixes,
                n_sessions_in_db=1,
                n_sessions_not_in_db=0,
            ) as r_data,
            self.settings(RAW_MEG_DIR=r_data.tmp_path.name),
        ):
            subjectdb.management.cronjobs.gather_resting_state_data()

            self.show_view('subjectdb.views.resting.RestingState')

    def test_sessions(self):
        self.index()
        self.login_fast('b1019547')
        self.index()

        n_subjects_in_db = 2
        n_subject_not_in_db = 2

        n_experiments_in_db = 1
        n_experiments_not_in_db = 1

        resting_state_suffixes = ['rest']
        other_suffixes = [None, 'run_01']

        all_suffixes = resting_state_suffixes + other_suffixes

        with (
            RestingStateData(
                n_subjects_in_db,
                n_subject_not_in_db,
                n_experiments_in_db,
                n_experiments_not_in_db,
                self.get_current_user(),
                all_suffixes,
                n_sessions_in_db=2,
                n_sessions_not_in_db=1,
            ) as r_data,
            self.settings(RAW_MEG_DIR=r_data.tmp_path.name),
        ):
            subjectdb.management.cronjobs.gather_resting_state_data()

            assert subjectdb.models.Session.objects.filter(restingstatesession__isnull=True).count() == 0

            for cur_subj in subjectdb.models.SubjectBase.objects.all():
                assert cur_subj.restingstatesession_set.count() == 6  # noqa PLR2004
                if cur_subj in r_data.subjects_in_db:
                    assert cur_subj.restingstatesession_set.filter(session__isnull=False).count() == 2  # noqa PLR2004
                elif cur_subj.subject_id in [x.subject_id for x in r_data.subjects_not_in_db]:
                    assert cur_subj.restingstatesession_set.filter(session__isnull=False).count() == 0
                else:
                    raise RuntimeError('Subject should not exist')

    def test_session_date_match_in_db(self):
        self.index()
        self.login_fast('b1019547')
        self.index()

        n_subjects_in_db = 2
        n_subject_not_in_db = 2

        n_experiments_in_db = 1
        n_experiments_not_in_db = 1

        resting_state_suffixes = ['rest']
        other_suffixes = [None, 'run_01']

        all_suffixes = resting_state_suffixes + other_suffixes

        with (
            RestingStateData(
                n_subjects_in_db,
                n_subject_not_in_db,
                n_experiments_in_db,
                n_experiments_not_in_db,
                self.get_current_user(),
                all_suffixes,
                n_sessions_in_db=2,
                n_sessions_not_in_db=1,
            ) as r_data,
            self.settings(RAW_MEG_DIR=r_data.tmp_path.name),
        ):
            subjectdb.management.cronjobs.gather_resting_state_data()
            for cur_fif in Path(r_data.tmp_path.name).glob('**/*_rest.fif'):  # noqa
                cur_o = subjectdb.models.RestingStateSession.objects.get(filename=cur_fif)
                fname = cur_fif.name
                subject_id = fname[:12]
                fif = mne.io.read_raw_fif(cur_fif)
                m_date = fif.info['meas_date']
                exp = cur_fif.parts[-4]
                if not re.match(
                    th_django.modelfields.subject_id.SubjectID.id_regex,  # noqa
                    subject_id,
                ):
                    warnings.warn(f'File {fname} does not have a valid subject id')
                    continue

                birthday = datetime.datetime.strptime(fname[:8], '%Y%m%d').date()
                found = False
                for cur_session in r_data.all_sessions:
                    if (
                        cur_session.subject.subject_id == subject_id  # noqa
                        and cur_session.subject.birthday == birthday
                        and cur_session.datetime_measured == m_date  # noqa
                        and cur_session.experiment.acronym == exp
                    ):
                        found = True
                        break

                assert found

                if cur_session.id is None:
                    assert cur_o.session is None
                else:
                    assert cur_o.session == cur_session

    def test_dewar_position_and_sfreq(self):
        self.index()
        self.login_fast('b1019547')
        self.index()

        s_rates = [200, 2000]
        dewar_positions = [0, 69]

        with (
            RestingStateData(
                n_sessions_in_db=1,
                n_experiments_in_db=2,
                n_experiments_not_in_db=0,
                n_subjects_in_db=2,
                n_sessions_not_in_db=0,
                s_rate=s_rates,
                dewar_positions=dewar_positions,
                suffixes=['rest'],
                current_user=self.get_current_user(),
            ) as r_data,
            self.settings(RAW_MEG_DIR=r_data.tmp_path.name),
        ):
            subjectdb.management.cronjobs.gather_resting_state_data()
            self.show_view('subjectdb.views.resting.RestingState')

            for cur_s in r_data.subjects_in_db:
                assert len(self.find_elements_by_text(cur_s.subject_id)) == 8  # noqa PLR2004

            for cur_srate in s_rates:
                self.show_view('subjectdb.views.resting.RestingState')
                s = Select(self.selenium.find_element_by_id('id_s_freq'))
                s.deselect_all()
                s.select_by_visible_text(f'{cur_srate}.0')
                s._el.submit()
                for cur_s in r_data.subjects_in_db:
                    assert len(self.find_elements_by_text(cur_s.subject_id)) == 4  # noqa PLR2004

                for cur_dewar_pos in dewar_positions:
                    s = Select(self.selenium.find_element_by_id('id_dewar_position'))
                    s.deselect_all()
                    s.select_by_visible_text(f'{cur_dewar_pos}.0')
                    s._el.submit()
                    for cur_s in r_data.subjects_in_db:
                        assert len(self.find_elements_by_text(cur_s.subject_id)) == 2  # noqa PLR2004

    @parameterized.expand(
        [
            ('^/tmp/', '/blabla/'),
            ('^/tmp/', ''),
            ('', '/blabla/'),
            ('', ''),
        ]
    )
    def test_filename_substitution(self, sub_match, sub_replace):
        self.index()
        self.login_fast('b1019547')
        self.index()

        global_preferences = global_preferences_registry.manager()
        global_preferences['restingstate__restingstate_filename_sub_match'] = sub_match  # noqa
        global_preferences['restingstate__restingstate_filename_sub_replace'] = sub_replace

        with (
            RestingStateData(
                n_subjects_in_db=2,
                n_experiments_in_db=1,
                suffixes=('resting',),
                n_sessions_in_db=1,
                current_user=self.get_current_user(),
            ) as r_data,
            self.settings(RAW_MEG_DIR=r_data.tmp_path.name),
        ):
            subjectdb.management.cronjobs.gather_resting_state_data()
            self.show_view('subjectdb.views.resting.RestingState')
            xlsx_download_path = Path(self.unittest_dir, 'resting_data.xlsx')
            xlsx_download_path.unlink(missing_ok=True)
            self.selenium.find_element_by_link_text('Export To Excel').click()
            with self._wait_for_downloaded_file(xlsx_download_path):
                sheet = load_workbook(filename=str(xlsx_download_path))
                f_names = [x.value for x in sheet.worksheets[0]['k'][1:]]

                for f in f_names:
                    if sub_match and sub_replace:
                        assert f[:8] == '/blabla/'
                    else:
                        assert f[:5] == '/tmp/'
