# Copyright (c) 2016-2025, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project,
# see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.

import datetime
from pathlib import Path

from django.conf import settings
from django.utils.timezone import get_current_timezone

experiment = {
    'acronym': 'test_exp',
    'description': 'Test Description',
    'uses_eeg': False,
    'uses_meg': True,
    'uses_fmri': False,
    'uses_eyetracker': True,
    'anc_url': 'http://anc.example.de/my_exp',
    'additional_investigators': ['thht'],
    'sinuhe_project': '',
    'paradigm_url': 'https://gitlab.example.de/paradigms/my_paradigm',
}

subject = {
    'mother_first_name': 'Inge',
    'mother_maiden_name': 'Dubber',
    'birthday': datetime.date(day=8, month=9, year=1980),
    'gender': 'F',
    'handedness': 'Left',
    'personal_info_consent': False,
    'available_for_experiments': False,
    'subject_notes': 'I am a Subject',
}

personal_info = {
    'first_name': 'Thomas',
    'last_name': 'Hartmann',
    'email': 'th@th.th',
    'phone_number': '+49 4171 709070',
    'wears_glasses': True,
    'can_do_meg': False,
    'can_do_mri': False,
}

session = subject.copy()
session.update(
    {
        'datetime_measurement': datetime.datetime(
            day=13, month=2, year=2017, hour=13, minute=10, tzinfo=get_current_timezone()
        ),
        'pilot_session': False,
        'notes': 'testnotes',
        'session_excluded': False,
        'session_excluded_reason': '',
    }
)

extradata = {'name': 'TestExtraData', 'description': 'TestDescription'}

extradata_with_items = extradata.copy()
extradata_with_items.update(
    {
        'dataitems': {
            'A test Stringtype': 'StringItem',
            'A test Integertype': 'IntegerItem',
            'A test Datetype': 'DateItem',
            'A test Choice': 'SingleChoiceDataItem',
            'A test Multichoice': 'MultiChoiceDataItem',
        },
        'options': {'A test Choice': 'Yes, No, Vielleicht', 'A test Multichoice': 'Hello World, Goodbye, The last one'},
    }
)

extradata_values = {
    'A test Stringtype': 'Hello World',
    'A test Integertype': 42,
    'A test Datetype': datetime.date(day=13, month=2, year=2017),
    'A test Choice': 'Vielleicht',
    'A test Multichoice': 'Hello World, The last one',
}

extradata_with_pdf = extradata.copy()
extradata_with_pdf.update({'dataitems': {'A PDF Item': 'PDFItem'}})

extradata_pdf_values = {'A PDF Item': str(Path(settings.BASE_DIR, 'test_datasets', 'pdf', 'valid_pdf1.pdf'))}

extradata_default = extradata.copy()
extradata_default['is_default_data'] = True

extradata_with_items_default = extradata_with_items.copy()
extradata_with_items_default['is_default_data'] = True

extradata_with_pdf_default = extradata_with_pdf.copy()
extradata_with_pdf_default['is_default_data'] = True
