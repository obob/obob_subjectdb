# Copyright (c) 2016-2025, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project,
# see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.
import copy
import datetime
from itertools import product
from pathlib import Path

import pyexcel
from django.contrib.auth import get_user_model
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC  # noqa N812
from selenium.webdriver.support.select import Select

import subjectdb.management.cronjobs
import subjectdb.views.experiments
import subjectdb.views.session
import subjectdb.views.subjectbase
import th_django.test
import user_manager.tests.helpers
from subjectdb import models
from subjectdb.factories.experiment import ExperimentFactory
from subjectdb.tests import base_infos
from subjectdb.tests.fixtures import TestData
from subjectdb.tests.helpers import ExtraAssertionMixin, UITestMixin


class ExperimentsUiTests(
    th_django.test.SeleniumTestCase, user_manager.tests.helpers.LoginLogoutMixin, UITestMixin, ExtraAssertionMixin
):
    def test_add_experiment_db(self):
        self.index()
        self.login('b1019547')
        self.add_user_db('thht', ['Investigator/Technical Staff'])
        self.add_experiment_db(**base_infos.experiment)

        self.assert_experiment(base_infos.experiment)

    def test_add_experiment(self):
        self.index()
        self.login('b1019547')
        self.add_user_db('thht', ['Investigator/Technical Staff'])
        self.add_experiment(**base_infos.experiment)

        self.assert_experiment(base_infos.experiment)

    def test_delete_undelete(self):
        exp1 = base_infos.experiment.copy()
        exp2 = base_infos.experiment.copy()

        exp2['acronym'] = 'test2'
        exp2['description'] = 'Test2'

        self.index()
        self.login('b1019547')
        self.add_user_db('thht', ['Access SubjectDB'])
        self.add_experiment_db(**exp1)
        self.add_experiment_db(**exp2)

        self.delete_experiment(exp2['acronym'])
        self.assertFalse(models.Experiment.objects.get(acronym=exp1['acronym']).deleted)
        self.assertTrue(models.Experiment.objects.get(acronym=exp2['acronym']).deleted)

        self.show_view(subjectdb.views.experiments.ExperimentDetail, kwargs={'acronym': exp2['acronym']})
        self.assertEqual(self.selenium.find_element_by_tag_name('h1').text, 'OOOPS!! That should not have happened!')

        self.show_view(subjectdb.views.experiments.ExperimentDetail, kwargs={'acronym': exp1['acronym']})
        self.assertIsNotNone(self.find_element_by_text('Experiment: {}'.format(exp1['acronym'])))

        self.undelete_experiment(exp2['acronym'])

        self.assertFalse(models.Experiment.objects.get(acronym=exp1['acronym']).deleted)
        self.assertFalse(models.Experiment.objects.get(acronym=exp1['acronym']).deleted)

    def test_edit_experiment(self):
        new_baseinfo = base_infos.experiment.copy()
        new_baseinfo['acronym'] = 'test_new'
        new_baseinfo['description'] = 'new description'
        new_baseinfo['uses_eeg'] = True
        new_baseinfo['uses_meg'] = False
        new_baseinfo['uses_fmri'] = True
        new_baseinfo['additional_investigators'] = None

        self.index()
        self.login('b1019547')
        self.add_user_db('thht', ['Investigator/Technical Staff'])
        self.add_experiment_db(**base_infos.experiment)
        object = models.Experiment.objects.get(acronym=base_infos.experiment['acronym'])
        self.show_view(subjectdb.views.experiments.Experiments)
        self.wait_for_table()
        self.wait_until_notstale(lambda: self.find_elements_by_text('Edit'))
        all_edits = self.find_elements_by_text('Edit')
        for edit in all_edits:
            if edit.tag_name == 'a' and edit.get_attribute('data-id') == str(object.id):
                edit.click()
                break

        self.wait_until(EC.visibility_of_element_located((By.ID, 'id_acronym')))
        self.fill_experiment(**new_baseinfo)

        self.selenium.find_element_by_id('form').submit()

        self.assert_experiment(new_baseinfo)

    def test_experiment_ownerships(self):
        exp1 = base_infos.experiment.copy()
        exp2 = base_infos.experiment.copy()
        exp3 = base_infos.experiment.copy()

        exp1['additional_investigators'] = []

        exp2['acronym'] = 'test2'
        exp2['description'] = 'Test2'
        exp2['additional_investigators'] = ['thht', 'thht2']

        exp3['acronym'] = 'test3'
        exp3['description'] = 'Test3'
        exp3['additional_investigators'] = ['thht']

        self.index()
        self.login('b1019547')
        self.add_user_db('thht', ['Access SubjectDB', 'Investigator/Technical Staff'])
        self.add_user_db('thht2', ['Access SubjectDB', 'Investigator/Technical Staff'])
        self.add_user_db('thht3', ['Lab Manager'])

        self.add_experiment_db(**exp1)
        self.add_experiment_db(**exp2)
        self.add_experiment_db(**exp3)

        self.add_session(exp1['acronym'], **base_infos.session)
        self.add_session(exp2['acronym'], **base_infos.session)
        self.add_session(exp3['acronym'], **base_infos.session)

        session1_id = models.Experiment.objects.get(acronym=exp1['acronym']).session_set.get().id
        session2_id = models.Experiment.objects.get(acronym=exp2['acronym']).session_set.get().id
        session3_id = models.Experiment.objects.get(acronym=exp3['acronym']).session_set.get().id

        self.assert_experiment(exp1)
        self.assert_experiment(exp2)
        self.assert_experiment(exp3)

        self.show_view(subjectdb.views.experiments.Experiments)
        self.wait_for_table()
        self.assertEqual(len(self.selenium.find_elements_by_tag_name('tr')), 4)
        self.show_view(subjectdb.views.experiments.ExperimentDetail, kwargs={'acronym': exp1['acronym']})
        self.assertIsNotNone(self.find_element_by_text('Experiment: {}'.format(exp1['acronym'])))
        self.show_view(subjectdb.views.experiments.ExperimentDetail, kwargs={'acronym': exp2['acronym']})
        self.assertIsNotNone(self.find_element_by_text('Experiment: {}'.format(exp2['acronym'])))
        self.show_view(subjectdb.views.experiments.ExperimentDetail, kwargs={'acronym': exp3['acronym']})
        self.assertIsNotNone(self.find_element_by_text('Experiment: {}'.format(exp3['acronym'])))
        self.show_view(subjectdb.views.session.SessionAdd, kwargs={'experiment_acronym': exp1['acronym']})
        self.assertIsNotNone(self.find_element_by_text('New Session'))
        self.show_view(subjectdb.views.session.SessionAdd, kwargs={'experiment_acronym': exp2['acronym']})
        self.assertIsNotNone(self.find_element_by_text('New Session'))
        self.show_view(subjectdb.views.session.SessionAdd, kwargs={'experiment_acronym': exp3['acronym']})
        self.assertIsNotNone(self.find_element_by_text('New Session'))
        self.show_view(subjectdb.views.session.SessionEdit, kwargs={'pk': session1_id})
        self.assertIsNotNone(self.find_element_by_text('Edit Session'))
        self.show_view(subjectdb.views.session.SessionEdit, kwargs={'pk': session2_id})
        self.assertIsNotNone(self.find_element_by_text('Edit Session'))
        self.show_view(subjectdb.views.session.SessionEdit, kwargs={'pk': session3_id})
        self.assertIsNotNone(self.find_element_by_text('Edit Session'))

        self.logout()
        self.login('thht')
        self.show_view(subjectdb.views.experiments.Experiments)
        self.wait_for_table()
        self.assertEqual(len(self.selenium.find_elements_by_tag_name('tr')), 3)
        self.show_view(subjectdb.views.experiments.ExperimentDetail, kwargs={'acronym': exp1['acronym']})
        self.assertIsNotNone(
            self.find_element_by_text('You cannot view this page because you do not have the necessary permission')
        )
        self.show_view(subjectdb.views.experiments.ExperimentDetail, kwargs={'acronym': exp2['acronym']})
        self.assertIsNotNone(self.find_element_by_text('Experiment: {}'.format(exp2['acronym'])))
        self.show_view(subjectdb.views.experiments.ExperimentDetail, kwargs={'acronym': exp3['acronym']})
        self.assertIsNotNone(self.find_element_by_text('Experiment: {}'.format(exp3['acronym'])))
        self.show_view(subjectdb.views.session.SessionAdd, kwargs={'experiment_acronym': exp1['acronym']})
        self.assertIsNotNone(
            self.find_element_by_text('You cannot view this page because you do not have the necessary permission')
        )
        self.show_view(subjectdb.views.session.SessionAdd, kwargs={'experiment_acronym': exp2['acronym']})
        self.assertIsNotNone(self.find_element_by_text('New Session'))
        self.show_view(subjectdb.views.session.SessionAdd, kwargs={'experiment_acronym': exp3['acronym']})
        self.assertIsNotNone(self.find_element_by_text('New Session'))
        self.show_view(subjectdb.views.session.SessionEdit, kwargs={'pk': session1_id})
        self.assertIsNotNone(
            self.find_element_by_text('You cannot view this page because you do not have the necessary permission')
        )
        self.show_view(subjectdb.views.session.SessionEdit, kwargs={'pk': session2_id})
        self.assertIsNotNone(self.find_element_by_text('Edit Session'))
        self.show_view(subjectdb.views.session.SessionEdit, kwargs={'pk': session3_id})
        self.assertIsNotNone(self.find_element_by_text('Edit Session'))

        self.logout()
        self.login('thht2')
        self.show_view(subjectdb.views.experiments.Experiments)
        self.wait_for_table()
        self.assertEqual(len(self.selenium.find_elements_by_tag_name('tr')), 2)
        self.show_view(subjectdb.views.experiments.ExperimentDetail, kwargs={'acronym': exp1['acronym']})
        self.assertIsNotNone(
            self.find_element_by_text('You cannot view this page because you do not have the necessary permission')
        )
        self.show_view(subjectdb.views.experiments.ExperimentDetail, kwargs={'acronym': exp2['acronym']})
        self.assertIsNotNone(self.find_element_by_text('Experiment: {}'.format(exp2['acronym'])))
        self.show_view(subjectdb.views.experiments.ExperimentDetail, kwargs={'acronym': exp3['acronym']})
        self.assertIsNotNone(
            self.find_element_by_text('You cannot view this page because you do not have the necessary permission')
        )
        self.show_view(subjectdb.views.session.SessionAdd, kwargs={'experiment_acronym': exp1['acronym']})
        self.assertIsNotNone(
            self.find_element_by_text('You cannot view this page because you do not have the necessary permission')
        )
        self.show_view(subjectdb.views.session.SessionAdd, kwargs={'experiment_acronym': exp2['acronym']})
        self.assertIsNotNone(self.find_element_by_text('New Session'))
        self.show_view(subjectdb.views.session.SessionAdd, kwargs={'experiment_acronym': exp3['acronym']})
        self.assertIsNotNone(
            self.find_element_by_text('You cannot view this page because you do not have the necessary permission')
        )
        self.show_view(subjectdb.views.session.SessionEdit, kwargs={'pk': session1_id})
        self.assertIsNotNone(
            self.find_element_by_text('You cannot view this page because you do not have the necessary permission')
        )
        self.show_view(subjectdb.views.session.SessionEdit, kwargs={'pk': session2_id})
        self.assertIsNotNone(self.find_element_by_text('Edit Session'))
        self.show_view(subjectdb.views.session.SessionEdit, kwargs={'pk': session3_id})
        self.assertIsNotNone(
            self.find_element_by_text('You cannot view this page because you do not have the necessary permission')
        )

        self.logout()
        self.login('thht3')
        self.show_view(subjectdb.views.experiments.Experiments)
        self.wait_for_table()
        self.assertEqual(len(self.selenium.find_elements_by_tag_name('tr')), 4)
        self.selenium.find_element_by_id('show_all_experiments_cb').click()
        self.wait_for_table()
        self.assertEqual(len(self.selenium.find_elements_by_class_name('table_acronym')), 0)
        self.show_view(subjectdb.views.experiments.ExperimentDetail, kwargs={'acronym': exp1['acronym']})
        self.assertIsNotNone(self.find_element_by_text('Experiment: {}'.format(exp1['acronym'])))
        self.show_view(subjectdb.views.experiments.ExperimentDetail, kwargs={'acronym': exp2['acronym']})
        self.assertIsNotNone(self.find_element_by_text('Experiment: {}'.format(exp2['acronym'])))
        self.show_view(subjectdb.views.experiments.ExperimentDetail, kwargs={'acronym': exp3['acronym']})
        self.assertIsNotNone(self.find_element_by_text('Experiment: {}'.format(exp3['acronym'])))
        self.show_view(subjectdb.views.session.SessionAdd, kwargs={'experiment_acronym': exp1['acronym']})
        self.assertIsNotNone(self.find_element_by_text('New Session'))
        self.show_view(subjectdb.views.session.SessionAdd, kwargs={'experiment_acronym': exp2['acronym']})
        self.assertIsNotNone(self.find_element_by_text('New Session'))
        self.show_view(subjectdb.views.session.SessionAdd, kwargs={'experiment_acronym': exp3['acronym']})
        self.assertIsNotNone(self.find_element_by_text('New Session'))
        self.show_view(subjectdb.views.session.SessionEdit, kwargs={'pk': session1_id})
        self.assertIsNotNone(self.find_element_by_text('Edit Session'))
        self.show_view(subjectdb.views.session.SessionEdit, kwargs={'pk': session2_id})
        self.assertIsNotNone(self.find_element_by_text('Edit Session'))
        self.show_view(subjectdb.views.session.SessionEdit, kwargs={'pk': session3_id})
        self.assertIsNotNone(self.find_element_by_text('Edit Session'))

    def test_labmanager_show_all(self):
        exp_b1019547 = base_infos.experiment.copy()
        exp_b1019547['additional_investigators'] = []
        exp_b1019547['acronym'] = 'b1019547'

        exp_thht = exp_b1019547.copy()
        exp_thht['acronym'] = 'thht'

        exp_thht2 = exp_b1019547.copy()
        exp_thht2['acronym'] = 'thht2'

        self.index()
        self.login('b1019547')
        self.add_user_db('thht', ['Lab Manager'])
        self.add_user_db('thht2', ['Access SubjectDB', 'Investigator/Technical Staff'])

        self.add_experiment_db(**exp_b1019547)

        self.logout()
        self.login('thht')
        self.add_experiment_db(**exp_thht)

        self.logout()
        self.login('thht2')
        self.add_experiment_db(**exp_thht2)

        exp_b1019547_obj = models.Experiment.objects.get(acronym=exp_b1019547['acronym'])
        exp_thht_obj = models.Experiment.objects.get(acronym=exp_thht['acronym'])
        exp_thht2_obj = models.Experiment.objects.get(acronym=exp_thht2['acronym'])

        self.show_view(subjectdb.views.experiments.Experiments)
        self.wait_for_table()
        with self.assertRaises(NoSuchElementException):
            self.find_element_by_text('Show all experiments')
        self.assertEqual(
            len(
                [
                    x
                    for x in self.find_elements_by_text('Edit')
                    if x.tag_name == 'a' and x.get_attribute('data-id') == str(exp_b1019547_obj.id)
                ]
            ),
            0,
        )
        self.assertEqual(
            len(
                [
                    x
                    for x in self.find_elements_by_text('Edit')
                    if x.tag_name == 'a' and x.get_attribute('data-id') == str(exp_thht_obj.id)
                ]
            ),
            0,
        )
        self.assertEqual(
            len(
                [
                    x
                    for x in self.find_elements_by_text('Edit')
                    if x.tag_name == 'a' and x.get_attribute('data-id') == str(exp_thht2_obj.id)
                ]
            ),
            1,
        )

        self.logout()
        self.login('b1019547')
        self.show_view(subjectdb.views.experiments.Experiments)
        self.wait_for_table()
        self.assertIsNotNone(self.selenium.find_element_by_id('show_all_experiments_cb'))
        self.assertEqual(
            len(
                [
                    x
                    for x in self.find_elements_by_text('Edit')
                    if x.tag_name == 'a' and x.get_attribute('data-id') == str(exp_b1019547_obj.id)
                ]
            ),
            1,
        )
        self.assertEqual(
            len(
                [
                    x
                    for x in self.find_elements_by_text('Edit')
                    if x.tag_name == 'a' and x.get_attribute('data-id') == str(exp_thht_obj.id)
                ]
            ),
            0,
        )
        self.assertEqual(
            len(
                [
                    x
                    for x in self.find_elements_by_text('Edit')
                    if x.tag_name == 'a' and x.get_attribute('data-id') == str(exp_thht2_obj.id)
                ]
            ),
            0,
        )
        self.selenium.find_element_by_id('show_all_experiments_cb').click()
        self.wait_for_table()
        self.assertEqual(
            len(
                [
                    x
                    for x in self.find_elements_by_text('Edit')
                    if x.tag_name == 'a' and x.get_attribute('data-id') == str(exp_b1019547_obj.id)
                ]
            ),
            1,
        )
        self.assertEqual(
            len(
                [
                    x
                    for x in self.find_elements_by_text('Edit')
                    if x.tag_name == 'a' and x.get_attribute('data-id') == str(exp_thht_obj.id)
                ]
            ),
            1,
        )
        self.assertEqual(
            len(
                [
                    x
                    for x in self.find_elements_by_text('Edit')
                    if x.tag_name == 'a' and x.get_attribute('data-id') == str(exp_thht2_obj.id)
                ]
            ),
            1,
        )

        self.logout()
        self.login('thht')
        self.show_view(subjectdb.views.experiments.Experiments)
        self.wait_for_table()
        self.assertIsNotNone(self.selenium.find_element_by_id('show_all_experiments_cb'))
        self.assertEqual(
            len(
                [
                    x
                    for x in self.find_elements_by_text('Edit')
                    if x.tag_name == 'a' and x.get_attribute('data-id') == str(exp_b1019547_obj.id)
                ]
            ),
            1,
        )
        self.assertEqual(
            len(
                [
                    x
                    for x in self.find_elements_by_text('Edit')
                    if x.tag_name == 'a' and x.get_attribute('data-id') == str(exp_thht_obj.id)
                ]
            ),
            1,
        )
        self.assertEqual(
            len(
                [
                    x
                    for x in self.find_elements_by_text('Edit')
                    if x.tag_name == 'a' and x.get_attribute('data-id') == str(exp_thht2_obj.id)
                ]
            ),
            1,
        )
        self.selenium.find_element_by_id('show_all_experiments_cb').click()
        self.wait_for_table()
        self.assertEqual(
            len(
                [
                    x
                    for x in self.find_elements_by_text('Edit')
                    if x.tag_name == 'a' and x.get_attribute('data-id') == str(exp_b1019547_obj.id)
                ]
            ),
            0,
        )
        self.assertEqual(
            len(
                [
                    x
                    for x in self.find_elements_by_text('Edit')
                    if x.tag_name == 'a' and x.get_attribute('data-id') == str(exp_thht_obj.id)
                ]
            ),
            1,
        )
        self.assertEqual(
            len(
                [
                    x
                    for x in self.find_elements_by_text('Edit')
                    if x.tag_name == 'a' and x.get_attribute('data-id') == str(exp_thht2_obj.id)
                ]
            ),
            0,
        )

    def test_empty_trash(self):
        self.index()
        self.login('b1019547')
        self.add_user_db('thht', ['Access SubjectDB'])

        (experiments, exp_acronyms, tmp) = self._create_experiments(3, db_direct=True)
        self.delete_experiment(exp_acronyms[0])
        self.delete_experiment(exp_acronyms[1])

        for cur_exp in experiments:
            cur_exp.refresh_from_db()

        experiments[1].datetime_deleted = experiments[1].datetime_deleted - datetime.timedelta(days=31)
        experiments[1].save()

        subjectdb.management.cronjobs.empty_trash()

        models.Experiment.objects.get(acronym=exp_acronyms[0])
        models.Experiment.objects.get(acronym=exp_acronyms[2])

        with self.assertRaises(models.Experiment.DoesNotExist):
            models.Experiment.objects.get(acronym=exp_acronyms[1])

    def test_labmanager_sees_all_experiments_by_default(self):
        experiments = {}
        exp_acronyms = {}

        exp_info = base_infos.experiment
        exp_info['additional_investigators'] = []

        self.index()
        self.login('b1019547')

        self.add_user_db('thht', ['Lab Manager'])
        self.add_user_db('thht2', ['Access SubjectDB', 'Investigator/Technical Staff'])
        self.add_user_db('thht3', ['Access SubjectDB', 'Investigator/Technical Staff', 'Lab Manager'])

        (experiments['b1019547'], exp_acronyms['b1019547'], tmp) = self._create_experiments(
            2, base_info=exp_info, db_direct=True
        )

        self.logout()
        self.login('thht2')

        (experiments['thht2'], exp_acronyms['thht2'], tmp) = self._create_experiments(
            2, base_info=exp_info, db_direct=True
        )

        self.logout()
        self.login('thht')

        self.show_view(subjectdb.views.experiments.Experiments)

        for cur_acronym in exp_acronyms['b1019547'] + exp_acronyms['thht2']:
            self.assertIsNotNone(self.find_element_by_text(cur_acronym))

        self.logout()
        self.login('thht3')

        self.show_view(subjectdb.views.experiments.Experiments)
        with self.assertRaises(NoSuchElementException):
            for cur_acronym in exp_acronyms['b1019547'] + exp_acronyms['thht2']:
                self.find_element_by_text(cur_acronym)

    def test_only_labmanager_can_choose_experiment_owner(self):
        self.index()
        self.login('b1019547')

        self.add_user_db('thht', ['Lab Manager', 'Access SubjectDB'])
        self.add_user_db('thht2', ['Access SubjectDB', 'Investigator/Technical Staff'])

        self.logout()
        self.login('thht')
        self.show_view(subjectdb.views.experiments.ExperimentCreate)

        self.assertIsNotNone(self.find_element_by_text('Owner'))

        # check whether one can choose an empty value....
        owner_select = Select(self.selenium.find_element_by_id('id_added_by'))

        for cur_option in owner_select.options:
            self.assertNotEqual(cur_option.text, '---------')

        self.logout()
        self.login('thht2')

        self.show_view(subjectdb.views.experiments.ExperimentCreate)

        with self.assertRaises(NoSuchElementException):
            self.find_element_by_text('Owner')

    def test_export_to_excel(self):
        self.index()
        self.login('b1019547')

        self.add_user_db('thht', ['Lab Manager', 'Access SubjectDB'])

        self.add_experiment_db(**base_infos.experiment)

        for exclude_session in (True, False):
            session_info = copy.deepcopy(base_infos.session)
            session_info['session_excluded'] = exclude_session
            if exclude_session:
                session_info['session_excluded_reason'] = 'test'
            self._create_sessions(base_infos.experiment['acronym'], 2, session_info)
            session_info['gender'] = 'M'
            self._create_sessions(base_infos.experiment['acronym'], 2, session_info)
            session_info['handedness'] = 'Right'
            self._create_sessions(base_infos.experiment['acronym'], 2, session_info)

            pilot_session = copy.deepcopy(base_infos.session)
            pilot_session['pilot_session'] = True
            self._create_sessions(base_infos.experiment['acronym'], 1, pilot_session)

        experiment = models.Experiment.objects.get(acronym=base_infos.experiment['acronym'])

        other_experiment = copy.deepcopy(base_infos.experiment)
        other_experiment['acronym'] = 'test2'
        self.add_experiment_db(**other_experiment)
        self._create_sessions(other_experiment['acronym'], 1)

        sheet = experiment.export_to_excel(include_pilots=True)
        self._assert_excel_sheet(sheet, experiment, True)

        sheet = experiment.export_to_excel(include_pilots=False)
        self._assert_excel_sheet(sheet, experiment, False)

        self.show_view(
            subjectdb.views.experiments.ExperimentDetail, kwargs={'acronym': base_infos.experiment['acronym']}
        )
        self.wait_for_table()
        self.selenium.find_element_by_id('export_excel_dropdown').click()
        self.selenium.find_element_by_id('experiment_excel_no_pilots').click()

        self._wait_for_downloaded_file(Path(self.unittest_dir, 'test_exp.xls'))

        sheet = pyexcel.get_sheet(file_name=str(Path(self.unittest_dir, 'test_exp.xls')))
        self._assert_excel_sheet(sheet, experiment, False)
        Path(self.unittest_dir, 'test_exp.xls').unlink()

        self.selenium.find_element_by_id('export_excel_dropdown').click()
        self.selenium.find_element_by_id('experiment_excel_with_pilots').click()

        self._wait_for_downloaded_file(Path(self.unittest_dir, 'test_exp.xls'))

        sheet = pyexcel.get_sheet(file_name=str(Path(self.unittest_dir, 'test_exp.xls')))
        self._assert_excel_sheet(sheet, experiment, True)
        Path(self.unittest_dir, 'test_exp.xls').unlink()

    def test_expired_users(self):
        self.index()
        self.login('b1019547')

        thht = self.add_user_db(
            'thht',
            ['Access SubjectDB', 'Investigator/Technical Staff'],
            expiration_date=self._set_expiration_date(days=10),
        )
        thht2 = self.add_user_db(
            'thht2',
            ['Access SubjectDB', 'Investigator/Technical Staff'],
            expiration_date=self._set_expiration_date(days=-10),
        )

        self.show_view(subjectdb.views.experiments.ExperimentCreate)
        add_inv_field = Select(self.selenium.find_element_by_id('id_additional_investigators'))
        add_inv_choices = [choice.text for choice in add_inv_field.options]
        self.assertIn(f'{thht.last_name}, {thht.first_name}', add_inv_choices)
        self.assertNotIn(f'{thht2.last_name}, {thht2.first_name}', add_inv_choices)

        added_by_field = Select(self.selenium.find_element_by_id('id_added_by'))
        added_by_choices = [choice.text for choice in added_by_field.options]
        self.assertIn(f'{thht.last_name}, {thht.first_name}', added_by_choices)
        self.assertNotIn(f'{thht2.last_name}, {thht2.first_name}', added_by_choices)

    def test_experiment_detail(self):
        self.add_user_db('thht', ['Investigator/Technical Staff'])
        self.index()
        self.login_fast('b1019547')

        with TestData.use() as data:
            this_exp = copy.deepcopy(base_infos.experiment)
            this_exp['sinuhe_project'] = data.projects[0].name

            self.add_experiment_db(**this_exp)
            self.assert_experiment(this_exp)
            self.assert_experiment_detail(this_exp)

    def test_experiment_list(self):
        headers_to_check = [
            'Experiment',
            'Added',
            'Owner',
            'Number of Sessions',
            'Edit',
            'Delete',
            'Add Session',
            'Methods',
        ]

        experiment_templates = [
            dict(uses_meg=True, uses_eyetracker=True, uses_fmri=True, uses_eeg=True),
            dict(uses_meg=False, uses_eyetracker=True, uses_fmri=False, uses_eeg=False),
            dict(uses_meg=False, uses_eyetracker=False, uses_fmri=False, uses_eeg=False),
        ]

        self.index()
        self.login_fast('b1019547')
        user = get_user_model().objects.get(username='b1019547')
        experiments = [ExperimentFactory(added_by=user, **exp) for exp in experiment_templates]

        self.show_view(subjectdb.views.experiments.Experiments)
        self.wait_for_table()
        table = self.selenium.find_element(By.CLASS_NAME, 'table')
        headers = [x.text for x in table.find_elements(By.TAG_NAME, 'th')]
        assert set(headers) == set(headers_to_check)

        experiment_col_idx = headers.index('Experiment')
        methods_col_idx = headers.index('Methods')
        all_rows = table.find_elements(By.TAG_NAME, 'tr')[1:]

        for exp in experiments:
            cur_row = [
                r for r in all_rows if r.find_elements(By.TAG_NAME, 'td')[experiment_col_idx].text == exp.acronym
            ][0]
            all_elements = cur_row.find_elements(By.TAG_NAME, 'td')
            assert all_elements[experiment_col_idx].text == exp.acronym
            methods_text = all_elements[methods_col_idx].text
            assert 'MEG' in methods_text if exp.uses_meg else 'MEG' not in methods_text
            assert 'EEG' in methods_text if exp.uses_eeg else 'EEG' not in methods_text
            assert 'fMRI' in methods_text if exp.uses_fmri else 'fMRI' not in methods_text
            assert 'Eyetracker' in methods_text if exp.uses_eyetracker else 'Eyetracker' not in methods_text

    def test_experiment_filters(self):
        self.index()
        self.login_fast('b1019547')
        self.add_user_db('thht', ['Investigator/Technical Staff'])

        owners = ['b1019547', 'thht']
        uses_meg = [True, False]
        uses_eeg = [True, False]
        uses_fmri = [True, False]
        uses_eyetracker = [True, False]

        all_experiments = []
        for owner, meg, eeg, fmri, eyetracker in product(owners, uses_meg, uses_eeg, uses_fmri, uses_eyetracker):
            all_experiments.append(
                ExperimentFactory(
                    added_by=get_user_model().objects.get(username=owner),
                    uses_meg=meg,
                    uses_eeg=eeg,
                    uses_fmri=fmri,
                    uses_eyetracker=eyetracker,
                )
            )

            if owner == 'thht':
                all_experiments[-1].additional_investigators.add(get_user_model().objects.get(username='b1019547'))

        self.show_view(subjectdb.views.experiments.Experiments)
        self.wait_for_table()
        for exp in all_experiments:
            try:
                self.find_element_by_text(exp.acronym)
            except NoSuchElementException:
                self.show_view(subjectdb.views.experiments.Experiments, get_parameters={'page': 2})
                self.wait_for_table()
                self.find_element_by_text(exp.acronym)

        # test acronyms
        for exp in all_experiments[:6]:
            self.selenium.find_element_by_id('reset_filter_button').click()
            self.wait_for_table()

            self.selenium.find_element_by_id('id_acronym').send_keys(exp.acronym)
            self.selenium.find_element_by_id('filter_form').submit()
            self.wait_for_table()
            for exp2 in all_experiments:
                if exp2 == exp:
                    self.find_element_by_text(exp2.acronym)
                else:
                    with self.assertRaises(NoSuchElementException):
                        self.selenium.implicitly_wait(0)
                        self.find_element_by_text(exp2.acronym)
                        self.selenium.implicitly_wait(2)

        # test owner
        for owner in owners:
            self.selenium.find_element_by_id('reset_filter_button').click()
            self.wait_for_table()

            owner_user = get_user_model().objects.get(username=owner)
            owner_select = Select(self.selenium.find_element_by_id('id_owner'))
            owner_select.select_by_visible_text(f'{owner_user.first_name} {owner_user.last_name}')
            self.selenium.find_element_by_id('filter_form').submit()
            self.wait_for_table()

            for exp in all_experiments:
                if exp.added_by.username == owner:
                    self.find_element_by_text(exp.acronym)
                else:
                    with self.assertRaises(NoSuchElementException):
                        self.selenium.implicitly_wait(0)
                        self.find_element_by_text(exp.acronym)
                        self.selenium.implicitly_wait(2)

        # test methods
        for meg, eeg, fmri, eyetracker in product(uses_meg, uses_eeg, uses_fmri, uses_eyetracker):
            if not meg and not eeg and not fmri and not eyetracker:
                continue
            self.selenium.find_element_by_id('reset_filter_button').click()
            self.wait_for_table()

            methods_select = Select(self.selenium.find_element_by_id('id_methods'))
            if meg:
                methods_select.select_by_visible_text('MEG')
            if eeg:
                methods_select.select_by_visible_text('EEG')
            if fmri:
                methods_select.select_by_visible_text('fMRI')
            if eyetracker:
                methods_select.select_by_visible_text('Eyetracker')

            self.selenium.find_element_by_id('filter_form').submit()
            self.wait_for_table()
            good_experiments = copy.copy(all_experiments)
            if meg:
                good_experiments = [exp for exp in good_experiments if exp.uses_meg]
            if eeg:
                good_experiments = [exp for exp in good_experiments if exp.uses_eeg]
            if fmri:
                good_experiments = [exp for exp in good_experiments if exp.uses_fmri]
            if eyetracker:
                good_experiments = [exp for exp in good_experiments if exp.uses_eyetracker]

            for exp in all_experiments:
                if exp in good_experiments:
                    self.find_element_by_text(exp.acronym)
                else:
                    with self.assertRaises(NoSuchElementException):
                        self.selenium.implicitly_wait(0)
                        self.find_element_by_text(exp.acronym)
                        self.selenium.implicitly_wait(2)

    def _assert_excel_sheet(self, sheet, experiment, include_pilots):
        sheet_header = sheet.row[15]
        sheet_data_raw = sheet.row[16:]

        new_sheet = pyexcel.get_sheet(array=sheet_data_raw)
        new_sheet.colnames = sheet_header

        sheet_data = list(new_sheet.to_records())

        for cur_data in sheet_data[1:]:
            cur_session = models.Session.objects.get(subject__subject_id=cur_data['Subject ID'])
            self.assertEqual(cur_session.subject.age(cur_session.datetime_measured.date()), cur_data['Age'])
            self.assertEqual(cur_session.subject.gender, cur_data['Gender'])
            self.assertEqual(cur_session.subject.handedness, cur_data['Handedness'])
            self.assertEqual(cur_session.subject.notes, cur_data['Subject Notes'])
            self.assertEqual(cur_session.datetime_measured.isoformat(), cur_data['Date and Time of Measurement'])
            self.assertEqual(cur_session.notes, cur_data['Session Notes'])
            if include_pilots:
                self.assertEqual(cur_session.pilot_session, cur_data['Pilot Session'])

        self.assertEqual(sheet[4, 1], experiment.number_of_sessions(include_pilots, include_excluded=True))
        self.assertEqual(sheet[5, 1], experiment.number_of_females(include_pilots, include_excluded=True))
        self.assertEqual(sheet[6, 1], experiment.number_of_right_handed(include_pilots, include_excluded=True))

        self.assertEqual(sheet[7, 1], experiment.mean_age(include_pilots, include_excluded=True))

        self.assertEqual(sheet[10, 1], experiment.number_of_sessions(include_pilots, include_excluded=False))
        self.assertEqual(sheet[11, 1], experiment.number_of_females(include_pilots, include_excluded=False))
        self.assertEqual(sheet[12, 1], experiment.number_of_right_handed(include_pilots, include_excluded=False))

        self.assertEqual(sheet[13, 1], experiment.mean_age(include_pilots, include_excluded=False))
