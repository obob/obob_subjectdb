# Copyright (c) 2016-2025, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project,
# see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.

from django.contrib import auth

import th_django.test
import user_manager.tests.helpers
from subjectdb import models
from subjectdb.tests import base_infos
from subjectdb.tests.helpers import ExtraAssertionMixin, UITestMixin


class GeneralUiTests(
    th_django.test.SeleniumTestCase, user_manager.tests.helpers.LoginLogoutMixin, UITestMixin, ExtraAssertionMixin
):
    def _assert_permission_failure(self):
        self.assertIsNotNone(
            self.find_element_by_text('You cannot view this page because you do not have the necessary permission.')
        )

    def _test_all_views(self, exp, session, session_object, should_fail):
        exp_object = models.Experiment.objects.get(acronym=exp['acronym'])

        self.show_view('subjectdb.views.subjectbase.Subjects')
        if should_fail:
            self._assert_permission_failure()
        else:
            self.wait_for_table()
            self.assertIsNotNone(self.find_element_by_text('All Subjects'))

        self.show_view('subjectdb.views.subjectbase.SubjectEdit', kwargs={'subject_id': self._subject_id(session)})
        if should_fail:
            self._assert_permission_failure()
        else:
            self.assertIsNotNone(self.find_element_by_text(f'Edit Subject {self._subject_id(session)}'))

        self.show_view('subjectdb.views.subjectbase.SubjectCreate')
        if should_fail:
            self._assert_permission_failure()
        else:
            self.assertIsNotNone(self.find_element_by_text('New Subject'))

        self.show_view('subjectdb.views.subjectbase.SubjectDelete', kwargs={'subject_id': self._subject_id(session)})
        if should_fail:
            self._assert_permission_failure()
        else:
            pass

        self.show_view('subjectdb.views.subjectbase.SubjectTrash')
        if should_fail:
            self._assert_permission_failure()
        else:
            self.wait_for_table()
            self.assertIsNotNone(self.find_element_by_text('Deleted Subjects'))

        self.show_view('subjectdb.views.subjectbase.SubjectUndelete', kwargs={'subject_id': self._subject_id(session)})
        if should_fail:
            self._assert_permission_failure()
        else:
            pass

        self.show_view('subjectdb.views.subjectbase.SubjectDetail', kwargs={'subject_id': self._subject_id(session)})
        if should_fail:
            self._assert_permission_failure()
        else:
            self.assertIsNotNone(self.find_element_by_text(f'Subject: {self._subject_id(session)}'))

        self.show_view('subjectdb.views.session.SubjectSessions', kwargs={'subject_id': self._subject_id(session)})
        if should_fail:
            self._assert_permission_failure()
        else:
            self.assertIsNotNone(self.find_element_by_text(f'Sessions for {self._subject_id(session)}'))

        self.show_view('subjectdb.views.experiments.Experiments')
        if should_fail:
            self._assert_permission_failure()
        else:
            self.wait_for_table()
            self.assertIsNotNone(self.find_element_by_text('All Experiments'))

        self.show_view('subjectdb.views.experiments.ExperimentEdit', kwargs={'pk': exp_object.id})
        if should_fail:
            self._assert_permission_failure()
        else:
            self.assertIsNotNone(self.find_element_by_text('Experiment: {}'.format(exp['acronym'])))

        self.show_view('subjectdb.views.experiments.ExperimentCreate')
        if should_fail:
            self._assert_permission_failure()
        else:
            self.assertIsNotNone(self.find_element_by_text('New Experiment'))

        self.show_view('subjectdb.views.experiments.ExperimentDelete', kwargs={'pk': exp_object.id})
        if should_fail:
            self._assert_permission_failure()
        else:
            pass

        self.show_view('subjectdb.views.experiments.ExperimentTrash')
        if should_fail:
            self._assert_permission_failure()
        else:
            self.wait_for_table()
            self.assertIsNotNone(self.find_element_by_text('Deleted Experiments'))

        self.show_view('subjectdb.views.experiments.ExperimentUndelete', kwargs={'pk': exp_object.id})
        if should_fail:
            self._assert_permission_failure()
        else:
            pass

        self.show_view('subjectdb.views.experiments.ExperimentDetail', kwargs={'acronym': exp['acronym']})
        if should_fail:
            self._assert_permission_failure()
        else:
            self.assertIsNotNone(self.find_element_by_text('Experiment: {}'.format(exp['acronym'])))

        self.show_view('subjectdb.views.session.SessionAdd', kwargs={'experiment_acronym': exp['acronym']})
        if should_fail:
            self._assert_permission_failure()
        else:
            self.assertIsNotNone(self.find_element_by_text('New Session'))

        self.show_view('subjectdb.views.session.SessionEdit', kwargs={'pk': session_object.id})
        if should_fail:
            self._assert_permission_failure()
        else:
            self.assertIsNotNone(self.find_element_by_text('Edit Session of Experiment {}'.format(exp['acronym'])))

        self.show_view('subjectdb.views.session.SessionTrash', kwargs={'experiment_acronym': exp['acronym']})
        if should_fail:
            self._assert_permission_failure()
        else:
            self.assertIsNotNone(self.find_element_by_text('Deleted Sessions of Experiment {}'.format(exp['acronym'])))

        self.show_view('subjectdb.views.session.SessionDelete', kwargs={'pk': session_object.id})
        if should_fail:
            self._assert_permission_failure()
        else:
            pass

        self.show_view('subjectdb.views.session.SessionUndelete', kwargs={'pk': session_object.id})
        if should_fail:
            self._assert_permission_failure()
        else:
            pass

    def test_permissions(self):
        exp1 = base_infos.experiment.copy()
        exp1['additional_investigators'] = ['thht', 'thht2']

        self.index()
        self.login('b1019547')
        self.add_user_db('thht', ['Access SubjectDB', 'Investigator/Technical Staff'])
        self.add_user_db('thht2', ['Investigator/Technical Staff'])
        self.add_experiment_db(**exp1)
        self.add_session(exp_acronym=exp1['acronym'], **base_infos.session)

        experiment_object = models.Experiment.objects.get(acronym=exp1['acronym'])
        session_object = experiment_object.session_set.get()

        self.logout()
        self.login('thht')
        self._test_all_views(exp1, base_infos.session, session_object, False)
        self.logout()

        self.login('thht2')
        self._test_all_views(exp1, base_infos.session, session_object, True)

    def test_permissions_labmanager(self):
        exp1 = base_infos.experiment.copy()
        exp1['additional_investigators'] = []

        self.index()
        self.login('b1019547')
        self.add_user_db('thht', ['Lab Manager'])
        self.add_experiment_db(**exp1)
        self.add_session(exp_acronym=exp1['acronym'], **base_infos.session)

        experiment_object = models.Experiment.objects.get(acronym=exp1['acronym'])
        session_object = experiment_object.session_set.get()

        self.logout()
        self.login('thht')
        self._test_all_views(exp1, base_infos.session, session_object, False)

    def test_change_and_delete_user(self):
        user_model = auth.get_user_model()
        exp = base_infos.experiment.copy()
        exp['additional_investigators'] = None

        self.index()
        self.login('b1019547')
        self.add_user('thht', ['Access SubjectDB', 'Investigator/Technical Staff'])

        self.logout()
        self.login('thht')
        self.add_experiment_db(**exp)
        self.add_session(exp['acronym'], **base_infos.session)

        self.logout()
        self.login('b1019547')

        self.edit_user('thht', ['Investigator/Technical Staff'])
        thht = user_model.objects.get(username='thht')
        self.assertFalse(thht.groups.filter(name='Access SubjectDB').exists())

        exp_object = self.assert_experiment(exp)
        session_obj = exp_object.session_set.get()
        subject = session_obj.subject
        self.assert_subject(subject, base_infos.subject)

        self.delete_user('thht')
        thht = user_model.objects.get(username='thht')
        self.assertFalse(thht.is_active)

        exp_object = self.assert_experiment(exp)
        session_obj = exp_object.session_set.get()
        subject = session_obj.subject
        self.assert_subject(subject, base_infos.subject)

    def test_helpers(self):
        self.index()
        self.login('b1019547')
        self.add_user_db('thht', ['Access SubjectDB'])
        (sub_list, sub_ids, subj_baseinfos) = self._create_subjects(3, db_direct=True)

        self.show_view('subjectdb.views.subjectbase.Subjects')
        for cur_id in sub_ids:
            self.assertIsNotNone(self.find_element_by_text(cur_id))

        for cur_subject, cur_id, cur_baseinfo in zip(sub_list, sub_ids, subj_baseinfos):
            cur_object = models.SubjectBase.objects.get(subject_id=cur_id)
            self.assert_subject(cur_object, cur_baseinfo)

        (sub_list, sub_ids, subj_baseinfos) = self._create_subjects(3, db_direct=True, with_personal_info=True)

        self.show_view('subjectdb.views.subjectbase.Subjects')
        for cur_id in sub_ids:
            self.assertIsNotNone(self.find_element_by_text(cur_id))

        for cur_subject, cur_id, cur_baseinfo in zip(sub_list, sub_ids, subj_baseinfos):
            cur_object = models.SubjectBase.objects.get(subject_id=cur_id)
            self.assert_subject(cur_object, cur_baseinfo)

        (exp_objects, exp_acronyms, exp_baseinfo) = self._create_experiments(3, db_direct=True)
        for cur_exp in exp_baseinfo:
            self.assert_experiment(cur_exp)
