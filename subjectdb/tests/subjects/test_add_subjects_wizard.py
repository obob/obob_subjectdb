# Copyright (c) 2016-2025, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project,
# see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.

import th_django.test
import user_manager.tests.helpers
from subjectdb.tests import base_infos
from subjectdb.tests.helpers import ExtraAssertionMixin, UITestMixin


class AddSubjectsWizardTest(
    th_django.test.SeleniumTestCase, user_manager.tests.helpers.LoginLogoutMixin, UITestMixin, ExtraAssertionMixin
):
    def test_add_subject_simple(self):
        self.index()
        self.login('b1019547')
        self.add_subject(**base_infos.subject)

        self.find_element_by_text('All Subjects')

    def test_add_subject_with_default_empty_subjectextradata(self):
        self.index()
        self.login('b1019547')

        self.add_subject_extradata_template(**base_infos.extradata_default)
        self.add_subject(**base_infos.subject)

        self.find_element_by_text('All Subjects')

    def test_add_subject_with_default_subjectextradata(self):
        self.index()
        self.login('b1019547')

        self.add_subject_extradata_template(**base_infos.extradata_with_items_default)
        self.add_subject(**base_infos.subject)

        self.find_element_by_text('Please fill in')
        self.selenium.find_element_by_id('form').submit()

        self.find_element_by_text('All Subjects')

    def test_add_subject_without_default_subjectextradata_as_session(self):
        self.index()
        self.login('b1019547')

        self.add_user_db('thht', ['Access SubjectDB', 'Investigator/Technical Staff'])
        self.add_user_db('thht2', ['Investigator/Technical Staff'])

        self.add_experiment(**base_infos.experiment)
        self.add_session(base_infos.experiment['acronym'], **base_infos.session)

        self.find_element_by_text('Edit Session of Experiment')

    def test_add_subject_with_default_subjectextradata_as_session(self):
        self.index()
        self.login('b1019547')

        self.add_user_db('thht', ['Access SubjectDB', 'Investigator/Technical Staff'])
        self.add_user_db('thht2', ['Investigator/Technical Staff'])

        self.add_subject_extradata_template(**base_infos.extradata_with_items_default)

        self.add_experiment(**base_infos.experiment)
        self.add_session(base_infos.experiment['acronym'], **base_infos.session)

        self.find_element_by_text('Please fill in')
        self.selenium.find_element_by_id('form').submit()

        self.find_element_by_text('Edit Session of Experiment')

    def test_add_subject_with_optional_empty_subjectextradata(self):
        self.index()
        self.login('b1019547')

        extra_data_template = self.add_subject_extradata_template(**base_infos.extradata)
        new_group = self.add_specialsubjectgroup('test_group', [extra_data_template])
        self.add_subject(special_subject_groups=[new_group], **base_infos.subject)

        self.find_element_by_text('All Subjects')

    def test_add_subject_with_optional_subjectextradata(self):
        self.index()
        self.login('b1019547')

        extra_data_template = self.add_subject_extradata_template(**base_infos.extradata_with_items)
        new_group = self.add_specialsubjectgroup('test_group', [extra_data_template])
        self.add_subject(special_subject_groups=[new_group], **base_infos.subject)

        self.find_element_by_text('Please fill in')
        self.selenium.find_element_by_id('form').submit()

        self.find_element_by_text('All Subjects')

    def test_add_subject_with_optional_subjectextradata_as_session(self):
        self.index()
        self.login('b1019547')

        self.add_user_db('thht', ['Access SubjectDB', 'Investigator/Technical Staff'])
        self.add_user_db('thht2', ['Investigator/Technical Staff'])

        extra_data_template = self.add_subject_extradata_template(**base_infos.extradata_with_items)
        new_group = self.add_specialsubjectgroup('test_group', [extra_data_template])

        self.add_experiment(**base_infos.experiment)
        self.add_session(base_infos.experiment['acronym'], special_subject_groups=[new_group], **base_infos.session)

        self.find_element_by_text('Please fill in')
        self.selenium.find_element_by_id('form').submit()

        self.find_element_by_text('Edit Session of Experiment')
