# Copyright (c) 2016-2025, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project,
# see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.

import copy
import datetime

import pytest
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC  # noqa N812

import subjectdb.factories
import subjectdb.management.cronjobs
import th_django.test
import user_manager.tests.helpers
from subjectdb import models
from subjectdb.tests import base_infos
from subjectdb.tests.helpers import ExtraAssertionMixin, UITestMixin


class SubjectsUITests(
    th_django.test.SeleniumTestCase, user_manager.tests.helpers.LoginLogoutMixin, UITestMixin, ExtraAssertionMixin
):
    def test_add_subject(self):
        self.index()
        self.login('b1019547')
        self.add_subject(**base_infos.subject)

        subject_id = self._subject_id(base_infos.subject)
        object = models.SubjectBase.objects.get(subject_id=subject_id)

        self.assert_subject(object, base_infos.subject)

        self.show_view('subjectdb.views.subjectbase.SubjectDetail', kwargs={'subject_id': subject_id})
        self.find_element_by_text('Birthday')
        with self.assertRaises(NoSuchElementException):
            self.find_element_by_text('First Name')

    def test_add_subject_with_personal_info(self):
        info = base_infos.subject.copy()
        info['personal_info_consent'] = True
        info.update(base_infos.personal_info)

        self.index()
        self.login('b1019547')
        self.add_subject(**info)

        subject_id = self._subject_id(base_infos.subject)
        object = models.SubjectBase.objects.get(subject_id=subject_id)

        self.assert_subject(object, info, base_infos.personal_info)

        self.show_view('subjectdb.views.subjectbase.SubjectDetail', kwargs={'subject_id': subject_id})
        self.find_element_by_text('Birthday')
        self.find_element_by_text('First Name')

    def test_add_personal_info(self):
        self.index()
        self.login('b1019547')
        self.add_subject_db(**base_infos.subject)

        self.wait_click_and_wait((By.NAME, 'edit'))
        self.wait_until(EC.visibility_of_any_elements_located((By.ID, 'id_consent_personal_info')))
        self.selenium.find_element_by_id('id_consent_personal_info').click()
        self.add_personal_info(**base_infos.personal_info)

        self.selenium.find_element_by_id('form').submit()

        subject_id = self._subject_id(base_infos.subject)

        object = models.SubjectBase.objects.get(subject_id=subject_id)

        new_subject_info = base_infos.subject.copy()
        new_subject_info['personal_info_consent'] = True

        self.assert_subject(object, new_subject_info, base_infos.personal_info)

    def test_delete_personal_info(self):
        info = base_infos.subject.copy()
        info['personal_info_consent'] = True
        info.update(base_infos.personal_info)

        self.index()
        self.login('b1019547')
        self.add_subject_db(**info)

        self.wait_click_and_wait((By.NAME, 'edit'))
        self.wait_until(EC.visibility_of_any_elements_located((By.ID, 'id_consent_personal_info')))
        self.selenium.find_element_by_id('id_consent_personal_info').click()
        self.selenium.find_element_by_id('form').submit()
        self.wait_click_and_wait((By.ID, 'btn_delete_personal_info'))

        subject_id = self._subject_id(base_infos.subject)
        object = models.SubjectBase.objects.get(subject_id=subject_id)

        with self.assertRaises(ObjectDoesNotExist):
            object.subjectpersonalinformation

        self.assertFalse(object.consent_personal_info)

    def test_edit_personal_info(self):
        info = base_infos.subject.copy()
        info['personal_info_consent'] = True
        info.update(base_infos.personal_info)

        self.index()
        self.login('b1019547')
        self.add_subject_db(**info)

        self.wait_click_and_wait((By.NAME, 'edit'))

        info['first_name'] = 'Martin'
        info['last_name'] = 'Meier'

        self.add_personal_info(**info)
        self.selenium.find_element_by_id('form').submit()

        subject_id = self._subject_id(base_infos.subject)

        personal_info = models.SubjectBase.objects.get(subject_id=subject_id).subjectpersonalinformation
        self.assertEqual(personal_info.first_name, info['first_name'])
        self.assertEqual(personal_info.last_name, info['last_name'])

    def test_delete_undelete(self):
        sub1 = base_infos.subject.copy()
        sub2 = base_infos.subject.copy()

        sub2['mother_first_name'] = 'Heidi'
        sub2['mother_maiden_name'] = 'Fischer'

        self.index()
        self.login('b1019547')
        self.add_subject_db(**sub1)
        self.add_subject_db(**sub2)

        self.delete_subject(self._subject_id(sub2))
        self.assertFalse(models.SubjectBase.objects.get(subject_id=self._subject_id(sub1)).deleted)
        self.assertTrue(models.SubjectBase.objects.get(subject_id=self._subject_id(sub2)).deleted)

        self.show_view('subjectdb.views.subjectbase.SubjectDetail', kwargs={'subject_id': self._subject_id(sub2)})
        self.assertEqual(self.selenium.find_element_by_tag_name('h1').text, 'OOOPS!! That should not have happened!')

        self.undelete_subject(self._subject_id(sub2))

        self.assertFalse(models.SubjectBase.objects.get(subject_id=self._subject_id(sub1)).deleted)
        self.assertFalse(models.SubjectBase.objects.get(subject_id=self._subject_id(sub2)).deleted)

    def test_delete_undelete_with_personal_data(self):
        sub1 = base_infos.subject.copy()
        sub1['personal_info_consent'] = True
        sub1.update(base_infos.personal_info)
        sub2 = sub1.copy()

        sub2['mother_first_name'] = 'Heidi'
        sub2['mother_maiden_name'] = 'Fischer'
        sub2['first_name'] = 'Heinz'

        self.index()
        self.login('b1019547')
        self.add_subject_db(**sub1)
        self.add_subject_db(**sub2)

        self.delete_subject(self._subject_id(sub2))
        self.assertFalse(models.SubjectBase.objects.get(subject_id=self._subject_id(sub1)).deleted)
        self.assertTrue(models.SubjectBase.objects.get(subject_id=self._subject_id(sub2)).deleted)

        self.show_view('subjectdb.views.subjectbase.SubjectDetail', kwargs={'subject_id': self._subject_id(sub2)})
        self.assertEqual(self.selenium.find_element_by_tag_name('h1').text, 'OOOPS!! That should not have happened!')

        self.undelete_subject(self._subject_id(sub2))

        self.assertFalse(models.SubjectBase.objects.get(subject_id=self._subject_id(sub1)).deleted)
        self.assertFalse(models.SubjectBase.objects.get(subject_id=self._subject_id(sub2)).deleted)

    def test_edit_notes(self):
        sub = base_infos.subject.copy()
        sub['subject_notes'] = 'Test Note 1'

        self.index()
        self.login('b1019547')
        self.add_subject_db(**sub)

        subject_id = self._subject_id(base_infos.subject)
        object = models.SubjectBase.objects.get(subject_id=subject_id)

        self.assertEqual(object.notes, sub['subject_notes'])

        self.show_view('subjectdb.views.subjectbase.SubjectEdit', kwargs={'subject_id': subject_id})

        new_note = 'New Test Note'

        self.selenium.find_element_by_id('id_notes').clear()
        self.selenium.find_element_by_id('id_notes').send_keys(new_note)
        self.selenium.find_element_by_id('form').submit()

        object = models.SubjectBase.objects.get(subject_id=subject_id)

        self.assertEqual(object.notes, new_note)

    def test_delete_subject_in_session(self):
        sub_lonely = copy.deepcopy(base_infos.subject)
        sub_lonely['subject_notes'] = 'lonely'
        sub_lonely['mother_first_name'] = 'Maria'

        exp = copy.deepcopy(base_infos.experiment)
        exp['additional_investigators'] = []

        self.index()
        self.login('b1019547')
        self.add_subject_db(**sub_lonely)
        self.add_experiment_db(**exp)
        self.add_session(exp['acronym'], **base_infos.session)

        self.show_view('subjectdb.views.subjectbase.Subjects')
        self.wait_for_table()

        good_subject_obj = models.SubjectBase.objects.get(notes=base_infos.subject['subject_notes'])
        alone_subject_obj = models.SubjectBase.objects.get(notes=sub_lonely['subject_notes'])

        good_delete_button = [
            x
            for x in self.find_elements_by_text('Delete')
            if x.tag_name == 'button' and x.get_attribute('data-subject') == good_subject_obj.subject_id
        ][0]

        lonely_delete_button = [
            x
            for x in self.find_elements_by_text('Delete')
            if x.tag_name == 'button' and x.get_attribute('data-subject') == alone_subject_obj.subject_id
        ][0]

        self.assertIsNone(lonely_delete_button.get_attribute('disabled'))
        self.assertEqual(good_delete_button.get_attribute('disabled'), 'true')

        self.delete_subject(alone_subject_obj.subject_id)
        self.wait_for_table()
        self.assertIsNotNone(self.find_element_by_text(f'Successfully deleted subject {alone_subject_obj.subject_id}.'))

        self.selenium.execute_script("document.querySelector('.deletebutton').disabled = false")
        self.wait_until(EC.element_to_be_clickable((By.CLASS_NAME, 'deletebutton')))
        self.delete_subject(good_subject_obj.subject_id, True)
        self.assertIsNotNone(self.find_element_by_text('OOOPS!! That should not have happened!'))

    def test_empty_trash(self):
        self.index()
        self.login('b1019547')

        (subjects, subject_ids, tmp) = self._create_subjects(3, db_direct=True)
        self.delete_subject(subjects[0].subject_id)
        self.delete_subject(subjects[1].subject_id)

        for cur_sub in subjects:
            cur_sub.refresh_from_db()

        subjects[1].datetime_deleted = subjects[1].datetime_deleted - datetime.timedelta(days=31)
        subjects[1].save()

        subjectdb.management.cronjobs.empty_trash()

        models.SubjectBase.objects.get(subject_id=subject_ids[0])
        models.SubjectBase.objects.get(subject_id=subject_ids[2])

        with self.assertRaises(models.SubjectBase.DoesNotExist):
            models.SubjectBase.objects.get(subject_id=subject_ids[1])

    def test_delete_subject_with_trashed_sessions(self):
        self.index()
        self.login('b1019547')
        self.add_user_db('thht', ['Investigator/Technical Staff'])

        (experiments, experiment_acronyms, experiment_baseinfo) = self._create_experiments(1, db_direct=True)
        (sessions, session_ids) = self._create_sessions(experiment_acronyms[0], 1)

        subject = sessions[0].subject
        session = sessions[0]

        self.show_view('subjectdb.views.subjectbase.Subjects')
        self.wait_for_table()

        subject_delete_button = [
            x
            for x in self.find_elements_by_text('Delete')
            if x.tag_name == 'button' and x.get_attribute('data-subject') == subject.subject_id
        ][0]

        self.assertEqual(subject_delete_button.get_attribute('disabled'), 'true')

        self.delete_session(experiment_acronyms[0], session_ids[0])

        self.show_view('subjectdb.views.subjectbase.Subjects')
        self.wait_for_table()

        subject_delete_button = [
            x
            for x in self.find_elements_by_text('Delete')
            if x.tag_name == 'button' and x.get_attribute('data-subject') == subject.subject_id
        ][0]

        self.assertIsNone(subject_delete_button.get_attribute('disabled'))

        self.delete_subject(subject.subject_id)

        self.add_subject(
            f'{subject.subject_id[8]}n{subject.subject_id[9]}',
            f'{subject.subject_id[10]}n{subject.subject_id[11]}',
            datetime.datetime.strptime(subject.subject_id[0:8], '%Y%m%d'),
            'F',
            'Left',
            True,
        )

        subject.refresh_from_db()
        self.assertFalse(subject.deleted)

        self.delete_subject(subject.subject_id)
        subject.refresh_from_db()
        self.assertTrue(subject.deleted)

        self.undelete_session(experiment_acronyms[0], session_ids[0])
        subject.refresh_from_db()
        self.assertFalse(subject.deleted)

        self.delete_session(experiment_acronyms[0], session_ids[0])

        session.refresh_from_db()
        session.datetime_deleted = session.datetime_deleted - datetime.timedelta(days=31)
        session.save()

        models.Session.objects.get(id=session_ids[0])

        subjectdb.management.cronjobs.empty_trash()

        with self.assertRaises(models.Session.DoesNotExist):
            models.Session.objects.get(id=session_ids[0])

        subject.refresh_from_db()
        self.assertEqual(subject.session_set.all().count(), 0)

    def test_add_subject_wrong_birthday(self):
        self.index()
        self.login('b1019547')

        this_subject = base_infos.subject
        this_subject['birthday'] = datetime.date(year=2016, month=3, day=1)

        with self.assertRaises(ValidationError):
            self.add_subject_db(**this_subject)

        self.add_subject(**this_subject)

        self.assertIsNone(models.SubjectBase.objects.first())

        this_subject['birthday'] = datetime.date(year=1980, month=3, day=1)
        self.add_subject_db(**this_subject)

        self.assertIsNotNone(models.SubjectBase.objects.first())

        models.SubjectBase.objects.first().delete()
        self.assertIsNone(models.SubjectBase.objects.first())

        self.add_subject(**this_subject)
        self.assertIsNotNone(models.SubjectBase.objects.first())

    def test_edit_gender_normal(self):
        self._test_edit_gender(False)

    def test_edit_gender_all_fields(self):
        self._test_edit_gender(True)

    def test_edit_handedness_normal(self):
        self._test_edit_handedness(False)

    def test_edit_handedness_all_fields(self):
        self._test_edit_handedness(True)

    def _test_edit_gender(self, undo_disable_fields):
        self.index()
        self.login('b1019547')

        self.add_user_db('thht', ['Access SubjectDB', 'Investigator/Technical Staff'])
        self.add_user_db('thht2', ['Lab Manager'])

        subject = self.add_subject_db(**base_infos.subject)

        self.logout()
        self.login('thht2')
        self.set_subject_gender(subject.subject_id, 'Male', undo_disable_fields)

        subject.refresh_from_db()
        self.assertEqual(subject.gender, 'M')

        self.logout()
        self.login('thht')

        if undo_disable_fields:
            self.set_subject_gender(subject.subject_id, 'Female', undo_disable_fields)
        else:
            with pytest.raises(NotImplementedError):
                self.set_subject_gender(subject.subject_id, 'Female', undo_disable_fields)

        subject.refresh_from_db()
        self.assertEqual(subject.gender, 'M')

    def _test_edit_handedness(self, undo_disable_fields):
        self.index()
        self.login('b1019547')

        self.add_user_db('thht', ['Access SubjectDB', 'Investigator/Technical Staff'])
        self.add_user_db('thht2', ['Lab Manager'])

        subject = self.add_subject_db(**base_infos.subject)

        self.logout()
        self.login('thht2')
        self.set_subject_handedness(subject.subject_id, 'Left', undo_disable_fields)

        subject.refresh_from_db()
        self.assertEqual(subject.handedness, 'Left')

        self.logout()
        self.login('thht')

        if undo_disable_fields:
            self.set_subject_handedness(subject.subject_id, 'Right', undo_disable_fields)
        else:
            with pytest.raises(NotImplementedError):
                self.set_subject_handedness(subject.subject_id, 'Right', undo_disable_fields)

        subject.refresh_from_db()
        self.assertEqual(subject.handedness, 'Left')

    def test_do_not_display_incomplete_subjects(self):
        self.index()
        self.login_fast('b1019547')
        self.index()

        good_subjects = subjectdb.factories.SubjectBaseFactory.create_batch(10)
        bad_subjects = subjectdb.factories.SubjectBaseFactory.create_batch(10, handedness='', gender='')

        self.show_view('subjectdb.views.subjectbase.Subjects')
        for s in good_subjects:
            assert self.find_element_by_text(s.subject_id)

        for s in bad_subjects:
            with self.assertRaises(NoSuchElementException):
                assert self.find_element_by_text(s.subject_id)
