import contextlib
import tempfile
from collections.abc import Generator
from pathlib import Path

from attrs import define, field
from faker import Faker

from subjectdb.factories import SubjectBaseFactory
from subjectdb.factories.fif import RawFif

faker = Faker()


@define
class TestProject:
    tmp_path: Path
    name: str
    fif_files: list[RawFif] = field(factory=list)

    @classmethod
    def create(cls, n_fif_files: int, tmp_path: Path, name: str) -> 'TestProject':
        fif_files = [
            RawFif(save_folder=tmp_path, project_name=name, subject=SubjectBaseFactory.build())
            for _ in range(n_fif_files)
        ]
        [f.save() for f in fif_files]
        return cls(tmp_path=tmp_path, name=name, fif_files=fif_files)


@define
class TestData:
    tmp_path: Path
    projects: list[TestProject] = field(factory=list)
    __tmp_path_folder: tempfile.TemporaryDirectory | None = field(init=False)

    @classmethod
    def create(cls, n_projects: int, fif_files_per_project: int, tmp_path: Path) -> 'TestData':
        projects = [TestProject.create(fif_files_per_project, tmp_path, faker.word()) for _ in range(n_projects)]
        return cls(tmp_path=tmp_path, projects=projects)

    @classmethod
    @contextlib.contextmanager
    def use(cls, n_projects: int = 2, fif_files_per_project: int = 3) -> Generator['TestData']:
        with tempfile.TemporaryDirectory() as tmp_path:
            yield cls.create(n_projects, fif_files_per_project, Path(tmp_path))
