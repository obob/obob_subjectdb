# Copyright (c) 2016-2025, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project,
# see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.

import mridb.forms.fields
import th_bootstrap.models.fields


class MRIFiles(th_bootstrap.models.fields.AjaxBootstrapFileInput):
    form_class = mridb.forms.fields.MRIFiles

    def __init__(self, file_type_field=None, *args, **kwargs):
        self.file_type_field = file_type_field
        if 'js_attrs' in kwargs:
            del kwargs['js_attrs']

        if 'upload_url' in kwargs:
            del kwargs['upload_url']

        super().__init__(*args, **kwargs)

    def validate(self, value, model_instance):
        setattr(model_instance, self.file_type_field.name, self.multi_check_result['file_type'])
        return super().validate(value, model_instance)
