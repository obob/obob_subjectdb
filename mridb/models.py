# Copyright (c) 2016-2025, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project,
# see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.

from django.db import models
from django_currentuser.db.models import CurrentUserField

import mridb.checkmri.new_checkmri
import mridb.modelfields.mri_files
import th_django.modelfields.subject_id
from th_django.models.mixins import DateTimeAddedBy, DeleteToTrash


# Create your models here.
class Subject(DateTimeAddedBy, DeleteToTrash):
    class Meta(DateTimeAddedBy.Meta):
        permissions = (('view_edit_all_mri', 'Can view, edit and delete everybodies data'),)

    datetime_added = models.DateTimeField(auto_now_add=True)
    subject_id = th_django.modelfields.subject_id.SubjectID(unique=True, blank=False)
    added_by = CurrentUserField(on_update=False, on_delete=models.PROTECT)
    deleted = models.BooleanField(default=False)
    datetime_deleted = models.DateTimeField(blank=True, null=True, default=None)

    def get_active_mris(self):
        return self.mri_set.filter(deleted=False)

    def get_deleted_mris(self):
        return self.mri_set.filter(deleted=True)


class MRIType(models.Model):
    mri_type = models.CharField(unique=True, blank=False, max_length=30)

    def __str__(self):
        return self.mri_type


class FileType(models.Model):
    file_type = models.CharField(unique=True, blank=False, max_length=30)

    def __str__(self):
        return self.file_type


class MRI(DateTimeAddedBy, DeleteToTrash):
    datetime_added = models.DateTimeField(auto_now_add=True)
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE)
    mri_type = models.ForeignKey(MRIType, on_delete=models.CASCADE)
    file_type = models.ForeignKey(FileType, on_delete=models.CASCADE)
    acquisition_date = models.DateField('Date of Acquisition')
    note = models.TextField(blank=True)
    mri_file = mridb.modelfields.mri_files.MRIFiles(
        file_storage='mri_files',
        auto_upload=True,
        show_messages=True,
        allow_multiple_files=True,
        multi_file_check_function=mridb.checkmri.new_checkmri.check_uploaded_mri,  # noqa
        messages={
            'upload_started': 'Uploading MRI Files...',
            'post_upload': 'MRI Upload Successful...',
            'check_multi': 'Checking uploaded MRI...',
            'check_multi_success': 'Uploaded MRI looks good!',
        },
        blank=False,
        file_type_field=file_type,
    )
    added_by = CurrentUserField(on_update=False, on_delete=models.PROTECT)
    deleted = models.BooleanField(default=False)
    datetime_deleted = models.DateTimeField(blank=True, null=True, default=None)

    def create_filename(self):
        f_name = (
            f'{self.subject.subject_id}_{str(self.file_type)}_{str(self.mri_type)}_{str(self.acquisition_date)}.zip'
        )

        return f_name.replace(' ', '_')
