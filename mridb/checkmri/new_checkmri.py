from pathlib import Path

from .checkmri import detect


def check_uploaded_mri(uuid, upload_tmp, files):
    rval = {'status': False, 'message': 'Failed to detect the MRI Format', 'file_type': None}

    file_path = Path(upload_tmp) / uuid / files[0]
    checker_instance = detect(file_path)
    if checker_instance:
        tmp = checker_instance.check(file_path)
        rval['status'] = tmp.status
        if tmp.status:
            rval['message'] = f'Upload successful. Detected MRI type: {str(checker_instance)}'
            rval['file_type'] = checker_instance.file_type()
        else:
            rval['message'] = tmp.message

    return rval
