import tarfile
from abc import ABCMeta, abstractmethod
from pathlib import Path

import django.core.exceptions
import nibabel
import pydicom
import pydicom.errors

import mridb.models


class CheckReturn:
    status = False
    message = ''

    def __init__(self, **kwargs):
        if 'status' in kwargs:
            self.status = kwargs['status']

        if 'message' in kwargs:
            self.message = kwargs['message']

    def __str__(self):
        return str({'status': self.status, 'message': self.message})


class CheckMRIBase(metaclass=ABCMeta):
    @abstractmethod
    def check(self, filename) -> CheckReturn:
        pass

    @abstractmethod
    def detect(self, filename) -> bool:
        pass

    def file_type(self):
        try:
            return mridb.models.FileType.objects.get(file_type=self.__str__())
        except django.core.exceptions.ObjectDoesNotExist:
            return mridb.models.FileType.objects.create(file_type=self.__str__())

    def __str__(self):
        return self.__class__.__name__


class Dicom(CheckMRIBase):
    def _detect_file(self, filename):
        if filename.suffix.lower() in ('.dcm', '.dicom'):
            is_dicom = True
        else:
            is_dicom = self.detect(filename.parent)

        return is_dicom

    def _detect_folder(self, filename):
        is_dicom = False
        dcm_files = list(filename.glob('*.dcm'))
        dcm_files.extend(filename.glob('*.dicom'))
        dcm_files.extend(filename.glob('*.DCM'))
        dcm_files.extend(filename.glob('*.DICOM'))

        if dcm_files:
            is_dicom = self.detect(dcm_files[0])

        return is_dicom

    def _get_dcm_list(self, filename):
        # check for sorted_file_list.txt to build filelist
        sorted_file_list = filename / 'sorted_file_list.txt'
        if sorted_file_list.is_file():
            with sorted_file_list.open() as f:
                dcm_list = f.readlines()

            dcm_list = [file.strip() for file in dcm_list if Path(file.strip()).suffix.lower() in ('.dcm', '.dicom')]

        else:
            # build filelist from available files
            dcm_list = list(filename.glob('*.dcm'))
            dcm_list.extend(filename.glob('*.dicom'))
            dcm_list.extend(filename.glob('*.DCM'))
            dcm_list.extend(filename.glob('*.DICOM'))

        return dcm_list

    def detect(self, filename):
        is_dicom = False

        if filename.is_file():
            is_dicom = self._detect_file(filename)
        elif filename.is_dir():
            is_dicom = self._detect_folder(filename)

        return is_dicom

    def check(self, filename):
        if filename.is_file():
            return self.check(filename.parent)

        dcm_list = self._get_dcm_list(filename)

        if not dcm_list:
            return CheckReturn(status=False, message='No dicom files found')

        for file in dcm_list:
            try:
                pydicom.read_file(file)
            except:  # noqa
                return CheckReturn(status=False, message='Could not read all dicom files')

        return CheckReturn(status=False, message='DICOM upload is disabled!')


class CDKMRI(CheckMRIBase):
    def detect(self, filename):
        all_file_extensions = ('bz2', 'xz')
        file_ext = filename.suffix.lower()[1:]
        if (not filename.is_file()) or file_ext not in all_file_extensions:
            return False

        with tarfile.open(filename, f'r:{file_ext}') as dicom_file:
            dicom_file.next()

            try:
                test_dicom = pydicom.dcmread(dicom_file.extractfile(dicom_file.next()))
                return test_dicom.InstitutionName in ('NSI MR', 'NSI MR RESEARCH', 'Neurokognitives MRT', 'CDK')
            except:  # noqa
                return False

    def _check_single_file(self, file):
        try:
            pydicom.dcmread(file, stop_before_pixels=True)
        except pydicom.errors.InvalidDicomError:
            return False
        except Exception as e:
            raise (e)
        return True

    def _check_dicom_file(self, dicom_file):
        message = 'DICOM seems to be invalid'
        for this_file in dicom_file:
            if this_file.isfile() and not self._check_single_file(dicom_file.extractfile(this_file)):
                return CheckReturn(status=False, message=message)

        return CheckReturn(status=True)

    def _check_tarfile(self, filename):
        file_ext = filename.suffix.lower()[1:]
        try:
            with tarfile.open(filename, f'r:{file_ext}') as dicom_file:
                return self._check_dicom_file(dicom_file)

        except (tarfile.ReadError, IsADirectoryError):
            return CheckReturn(status=False)

    def check(self, filename):
        all_file_extensions = ('bz2', 'xz')
        file_ext = filename.suffix.lower()[1:]

        if file_ext not in all_file_extensions:
            return CheckReturn(status=False)

        return self._check_tarfile(filename)

    def __str__(self):
        return 'Converted CDK MRI'


class NIFTI(CheckMRIBase):
    def detect(self, filename):
        if not filename.is_file():
            return False

        if filename.suffix.lower() == '.nii':
            return True
        elif filename.suffix.lower() == '.gz':
            tmp = filename.stem
            if Path(tmp).suffix.lower() == '.nii':
                return True

        return False

    def check(self, filename):
        from nibabel.filebasedimages import ImageFileError

        try:
            img = nibabel.load(filename)
        except ImageFileError:
            return CheckReturn(status=False, message='Failed to load nifti file')

        if isinstance(img, (nibabel.nifti1.Nifti1Image, nibabel.nifti2.Nifti2Image)):
            return CheckReturn(status=True)
        else:
            return CheckReturn(status=False, message='Nifti file seems to be of wrong filetype')


class Analyze(CheckMRIBase):
    def detect(self, filename):
        return bool(filename.is_file() and filename.suffix.lower() in ('.hdr', '.img'))

    def check(self, filename):
        from nibabel.filebasedimages import ImageFileError

        try:
            img = nibabel.load(filename)
            img.get_fdata()
        except ImageFileError:
            return CheckReturn(status=False, message='Failed to load analyze file')

        if isinstance(img, (nibabel.spm2analyze.Spm2AnalyzeImage, nibabel.spm99analyze.Spm99AnalyzeImage)):
            return CheckReturn(status=True)
        else:
            return CheckReturn(status=False, message='Analyze file seems to be of wrong filetype')


def detect(filename) -> CheckMRIBase:
    all_checkers = CheckMRIBase.__subclasses__()
    for checker_class in all_checkers:
        checker = checker_class()
        if checker.detect(filename):
            return checker
