/*
 * Copyright (c) 2016-2025, Thomas Hartmann
*
* This file is part of the OBOB Subject Database Project, see: https://gitlab.com/obob/obob_subjectdb/
*
*    obob_subjectdb is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    obob_subjectdb is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.
*/

import 'jquery';
import init_fileinput from '../../../../th_bootstrap/static/ajaxbootstrap_fileinput';
import bs_alerts from '../../../../th_bootstrap/static/bootstrap_alerts';
import 'bootstrap-datepicker';
import 'bootstrap-validator';

var input_element = $('#id_mri_file');

init_fileinput(input_element);

var messages_div = $('#' + input_element.attr('name') + '_messages');

$('#form').submit(function (evt) {
    if (this.checkValidity()) {
        bs_alerts(messages_div, 'alert-info', 'Processing MRI. Please wait....');
        $(':submit').prop('disabled', true);
    }
});

$('#id_acquisition_date').datepicker({
    format: $('#date_format').val(),
    orientation: 'bottom left',
    autoclose: true
});

$('#form').validator('update');
