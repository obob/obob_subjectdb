# Copyright (c) 2016-2025, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project,
# see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.

from django.apps import AppConfig
from django.db.models.signals import post_migrate
from django_auto_url.urls import reverse_classname_lazy

from th_django.middleware.applist import AppNamespaceInfo, AppNamespaceInfoMixin

from .management import apps, create_b1019547_user, create_groups, create_mritypes


class MridbConfig(AppConfig, AppNamespaceInfoMixin):
    name = 'mridb'

    namespace_info = [
        AppNamespaceInfo(
            name='MRIs', namespace='mridb', top_url=reverse_classname_lazy('mridb.views.display.Subjects')
        ),
    ]

    def ready(self):
        post_migrate.connect(create_mritypes, sender=self, dispatch_uid='mridb.apps.create_mritypes')
        post_migrate.connect(
            create_b1019547_user, sender=apps.get_app_config('auth'), dispatch_uid='mridb.apps.create_b1019547'
        )
        post_migrate.connect(create_groups, sender=self, dispatch_uid='mridb.apps.create_groups')
