from pathlib import Path

import django.conf
import django.forms
import django.forms.widgets

import mridb.utils.mri
import th_bootstrap.forms.fields
import th_bootstrap.widgets.ajaxbootstrap_fileinput


class MRIFiles(th_bootstrap.forms.fields.AjaxBootstrapFileInput):
    def process_post_check(self, value, uuid, file_list):
        cur_filetype = str(self.modelfield_instance.multi_check_result['file_type'])
        if cur_filetype == 'Converted CDK MRI':
            tmpdir = self._get_tmpdir()

            mridb.utils.mri.convert_cdk_mri(Path(tmpdir) / file_list[0], tmpdir)
            (Path(tmpdir) / file_list[0]).unlink()
            file_list[0] = 'mri.nii'


class SubjectID(django.forms.CharField):
    def __init__(self, id_regex, **kwargs):
        self._id_regex = id_regex
        super().__init__(**kwargs)

    def widget_attrs(self, widget):
        if isinstance(widget, django.forms.widgets.TextInput):
            return {'pattern': self._id_regex}
