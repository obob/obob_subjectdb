# Copyright (c) 2016-2025, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project,
# see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.

from django.contrib import messages
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import requires_csrf_token
from django_auto_url import kwargs
from django_auto_url.urls import reverse_classname, reverse_classname_lazy

import mridb.views.display
from mridb import models
from mridb.views.mixins import DefaultMixin
from th_django.views.edit import (
    CreateUpdateTrashcanView,
    CreateWithForeignKeyView,
    DeleteToTrashcanView,
    UndeleteFromTrashcanView,
)
from th_django.views.mixins import MessageAfterPostMixin


class SubjectEdit(DefaultMixin, CreateUpdateTrashcanView):
    model = models.Subject
    slug_field = 'subject_id'
    slug_url_kwarg = 'subject_id'
    fields = ['subject_id']
    must_be_owner = True
    url_kwargs = [kwargs.string('subject_id')]
    template_name = 'mridb/subject_form.html'

    extra_context = {'extra_js': ['mridb/subject_form.js']}

    def get_success_url(self):
        return reverse_classname_lazy(mridb.views.display.SubjectDetail, kwargs={'subject_id': self.object.subject_id})

    def get_permission_required(self):
        if self.slug_field in self.kwargs:
            return ('mridb.change_subject',)
        else:
            return ('mridb.add_subject',)

    def get_main_header(self):
        return 'Subject: {}'.format(self.kwargs['subject_id'])


class SubjectCreate(SubjectEdit):
    url_kwargs = None
    url_ignore_pk = True

    def get_main_header(self):
        return 'New Subject'


class SubjectDelete(DefaultMixin, MessageAfterPostMixin, DeleteToTrashcanView):
    model = models.Subject
    slug_field = 'subject_id'
    slug_url_kwarg = 'subject_id'
    http_method_names = ['post', 'delete']
    success_url = reverse_classname_lazy(mridb.views.display.Subjects)
    permission_required = 'mridb.delete_subject'
    must_be_owner = True
    message_priority = messages.SUCCESS
    url_kwargs = [kwargs.string('subject_id')]

    def get_message_text(self):
        return f'Successfully deleted subject {self.get_object().subject_id}.'


class SubjectUndelete(DefaultMixin, MessageAfterPostMixin, UndeleteFromTrashcanView):
    model = models.Subject
    slug_field = 'subject_id'
    slug_url_kwarg = 'subject_id'
    http_method_names = ['post']
    permission_required = 'mridb.delete_subject'
    must_be_owner = True
    success_url = reverse_classname_lazy(mridb.views.display.Subjects)
    message_priority = messages.SUCCESS
    url_kwargs = [kwargs.string('subject_id')]

    def get_message_text(self):
        return f'Successfully undeleted subject {self.get_object().subject_id}.'


class MRIDelete(DefaultMixin, MessageAfterPostMixin, DeleteToTrashcanView):
    model = models.MRI
    slug_url_kwarg = 'mri_id'
    slug_field = 'id'
    http_method_names = ['post', 'delete']
    permission_required = 'mridb.delete_mri'
    must_be_owner = True
    message_priority = messages.SUCCESS
    message_text = 'Successfully deleted MRI.'
    url_kwargs = [kwargs.int('mri_id')]

    def get_success_url(self):
        return reverse_classname(
            mridb.views.display.SubjectDetail, kwargs={'subject_id': self.object.subject.subject_id}
        )


class MRIUndelete(DefaultMixin, MessageAfterPostMixin, UndeleteFromTrashcanView):
    model = models.MRI
    slug_url_kwarg = 'mri_id'
    slug_field = 'id'
    http_method_names = ['post']
    permission_required = 'mridb.delete_mri'
    must_be_owner = True
    message_priority = messages.SUCCESS
    message_text = 'Successfully undeleted MRI.'

    def get_success_url(self):
        return reverse_classname(
            mridb.views.display.SubjectDetail, kwargs={'subject_id': self.object.subject.subject_id}
        )


@method_decorator(requires_csrf_token, name='dispatch')
class MriNew(DefaultMixin, CreateWithForeignKeyView):
    model = models.MRI
    foreign_model = models.Subject
    foreign_model_field = 'subject_id'
    parameter_url_kwarg = 'subject_id'
    form_field = 'subject'
    fields = ['mri_type', 'acquisition_date', 'note', 'mri_file']
    permission_required = 'mridb.add_mri'
    url_kwargs = [kwargs.string('subject_id')]

    extra_context = {'extra_js': ['mridb/mri_form.js']}

    def get_success_url(self):
        return reverse_classname(mridb.views.display.SubjectDetail, kwargs={'subject_id': self.kwargs['subject_id']})

    def get_main_header(self):
        return f'New MRI for {self.foreign_object.subject_id}'
