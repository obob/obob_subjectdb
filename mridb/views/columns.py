# Copyright (c) 2016-2025, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project,
# see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.

from django_auto_url.urls import reverse_classname_lazy
from django_tables2.columns import LinkColumn, library
from django_tables2.utils import A

import th_bootstrap.django_tables2.columns
from th_bootstrap.django_tables2.columns import BootstrapColumnMixin, Column


@library.register
class MriCountColumn(Column):
    def __init__(self, **kwargs):
        kwargs.update({'accessor': A('subject_id'), 'verbose_name': 'Number of MRIs', 'orderable': False})

        super().__init__(**kwargs)

    def render(self, value, record, **kwargs):
        return len(record.active_mris)


@library.register
class MriUploadColumn(BootstrapColumnMixin, LinkColumn):
    def __init__(self, **kwargs):
        field = 'subject_id'

        kwargs.update(
            {
                'viewname': reverse_classname_lazy('mridb.views.edit.MriNew', return_url_name=True),
                'args': [A(field)],
                'accessor': A(field),
                'text': 'Upload',
                'verbose_name': 'Upload MRI',
                'attrs': {
                    'a': {
                        'class': 'btn btn-default btn-sm',
                        'name': 'upload',
                    }
                },
                'orderable': False,
            }
        )

        super().__init__(**kwargs)


@library.register
class SubjectColumn(BootstrapColumnMixin, LinkColumn):
    def __init__(self, **kwargs):
        field = 'subject_id'

        kwargs.update(
            {
                'viewname': reverse_classname_lazy('mridb.views.display.SubjectDetail', return_url_name=True),
                'args': [A(field)],
                'accessor': A(field),
                'verbose_name': 'Subject',
            }
        )

        super().__init__(**kwargs)


@library.register
class EditColumn(th_bootstrap.django_tables2.columns.EditColumn):
    def is_owner(self, context, record):
        if context.request.user.has_perm('mridb.view_edit_all_mri'):
            return True
        else:
            return super().is_owner(context, record)


@library.register
class DeleteColumn(th_bootstrap.django_tables2.columns.DeleteColumn):
    def is_owner(self, context, record):
        if context.request.user.has_perm('mridb.view_edit_all_mri'):
            return True
        else:
            return super().is_owner(context, record)
