# Copyright (c) 2016-2025, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project,
# see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.
import datetime
from pathlib import Path

import six
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC  # noqa N812
from selenium.webdriver.support.ui import Select

import obob_subjectdb.tests.helpers
from mridb import models


class UITestMixin(obob_subjectdb.tests.helpers.UITestMixin):
    def add_mri_subject(self, subject_id):
        self.show_view('mridb.views.display.Subjects')
        self.click_navbutton()
        self.wait_until(EC.visibility_of_element_located((By.ID, 'add_subject')))
        self.selenium.find_element_by_id('add_subject').click()
        self.wait_until(EC.visibility_of_element_located((By.ID, 'id_subject_id')))
        self.selenium.find_element_by_id('id_subject_id').send_keys(subject_id)

        if 'disabled' in self.selenium.find_element_by_id('form').find_element(By.TAG_NAME, 'button').get_attribute(
            'class'
        ):
            raise ValueError(f'Could not add subject with ID: {subject_id}')

        self.selenium.find_element_by_id('form').submit()

        try:
            models.Subject.objects.get(subject_id=subject_id)
        except models.Subject.DoesNotExist:
            raise ValueError(f'Could not add subject with ID: {subject_id}')

    def add_mri_subject_db(self, subject_id):
        new_subject = models.Subject(subject_id=subject_id)
        new_subject.added_by = self.get_current_user()
        new_subject.datetime_added = datetime.datetime.now()
        new_subject.save()

    def upload_mri(self, subject_id, mri_file, mri_type='T1', click_day='12', notes=None):
        if isinstance(mri_file, Path):
            mri_file = str(mri_file)
        elif isinstance(mri_file, list):
            mri_file = [str(x) for x in mri_file]

        self.show_view('mridb.views.display.SubjectDetail', {'subject_id': subject_id})
        self.wait_click_and_wait((By.ID, 'upload'))
        self.wait_until(EC.presence_of_element_located((By.ID, 'id_mri_file')))

        if isinstance(mri_file, six.string_types):
            self.selenium.find_element_by_id('id_mri_file').send_keys(mri_file)
        else:
            self.selenium.find_element_by_id('id_mri_file').send_keys('\n'.join(mri_file))

        select = Select(self.selenium.find_element_by_id('id_mri_type'))
        select.select_by_visible_text(mri_type)
        self.wait_until(EC.element_to_be_clickable((By.ID, 'id_acquisition_date')))
        self.selenium.find_element_by_id('id_acquisition_date').click()
        self.wait_until(EC.presence_of_element_located((By.CLASS_NAME, 'datepicker-days')))
        self.find_element_by_text(click_day, self.selenium.find_element_by_class_name('datepicker-days')).click()
        if notes:
            self.selenium.find_element_by_id('id_note').send_keys(notes)
        self.wait_until(EC.presence_of_element_located((By.CLASS_NAME, 'alert-success')), timeout=120)
        old_page = self.selenium.find_element_by_tag_name('html')
        self.selenium.find_element_by_id('form').submit()
        self.wait_until(EC.staleness_of(old_page), timeout=120)
        self.wait_until(EC.visibility_of_element_located((By.TAG_NAME, 'body')), timeout=120)
        return models.MRI.objects.order_by('-id')[0].create_filename()

    def delete_mri(self, subject_id, which=0):
        self.show_view('mridb.views.display.SubjectDetail', {'subject_id': subject_id})
        self.wait_for_table()
        self.wait_until(EC.element_to_be_clickable((By.NAME, 'delete_button')))
        all_delete_buttons = [x for x in self.find_elements_by_text('Delete') if x.tag_name == 'button']
        all_delete_buttons[which].click()
        self.wait_click_and_wait((By.ID, 'okButton'), False)

    def undelete_mri(self, subject_id, which=0):
        self.show_view('mridb.views.display.SubjectDetail', {'subject_id': subject_id})
        self.wait_click_and_wait((By.ID, 'undelete'))
        self.wait_until(EC.element_to_be_clickable((By.NAME, 'undelete')))
        all_undelete_buttons = [x for x in self.selenium.find_elements_by_name('undelete') if x.tag_name == 'button']
        old_page = self.selenium.find_element_by_tag_name('html')
        all_undelete_buttons[which].click()
        self.wait_until(EC.staleness_of(all_undelete_buttons[which]))
        self.wait_until(EC.staleness_of(old_page))

    def delete_subject(self, subject_id):
        self.index()
        self.wait_for_table()

        delete_button = [
            x
            for x in self.find_elements_by_text('Delete')
            if x.tag_name == 'button' and x.get_attribute('data-subject') == subject_id
        ][0]
        delete_button.click()

        self.wait_click_and_wait((By.ID, 'okButton'))

    def delete_subject_db(self, subject_id):
        subject = models.Subject.objects.get(subject_id=subject_id)
        subject.deleted = True
        subject.datetime_deleted = datetime.datetime.now()
        subject.save()

    def undelete_subject(self, subject_id):
        self.index()
        self.wait_click_and_wait((By.ID, 'undelete_subjects'))
        self.wait_until(EC.element_to_be_clickable((By.ID, subject_id)))
        self.wait_click_and_wait((By.ID, subject_id))
