from pathlib import Path

from django.conf import settings

import convert_cdk_mri_filetypes
import mridb.models
import th_django.test
import user_manager.tests.helpers

from .helpers import UITestMixin


class ConvertCDKMRIFiletypeTests(
    th_django.test.SeleniumTestCase, user_manager.tests.helpers.LoginLogoutMixin, UITestMixin
):
    def test_convert_cdkmri_filetypes(self):
        subject_id = '19800908igdb'
        self.index()
        self.login('b1019547')

        self.add_mri_subject_db(subject_id)
        self.upload_mri(
            subject_id, Path(settings.BASE_DIR) / 'test_datasets' / 'cdktarbz2' / 'cdktarbz_testmri.tar.bz2'
        )
        self.upload_mri(subject_id, Path(settings.BASE_DIR) / 'test_datasets' / 'cdktarxz' / 'cdktarxz_testmri.tar.xz')
        self.upload_mri(
            subject_id, Path(settings.BASE_DIR) / 'test_datasets' / 'cdktarxz_norm' / 'cdktarxz_norm.tar.xz'
        )
        self.upload_mri(subject_id, Path(settings.BASE_DIR) / 'test_datasets' / 'nifti_sbg' / 'test_nifti.nii')
        self.upload_mri(subject_id, list(Path(settings.BASE_DIR, 'test_datasets', 'analyze').glob('*')))
        self.upload_mri(
            subject_id, list(Path(settings.BASE_DIR, 'test_datasets', 'nifti_sbg').glob('test_nifti.nii.gz'))
        )

        MRI = mridb.models.MRI  # noqa N806
        FileType = mridb.models.FileType  # noqa N806

        self.assertEqual(MRI.objects.filter(file_type__file_type='Converted CDK MRI').count(), 3)
        self.assertEqual(MRI.objects.filter(file_type__file_type='NIFTI').count(), 2)

        original_converted_types = list(MRI.objects.filter(file_type__file_type='Converted CDK MRI').all())
        original_nonconverted_types = list(MRI.objects.exclude(file_type__file_type='Converted CDK MRI').all())

        for cur_mri in MRI.objects.filter(file_type__file_type='Converted CDK MRI').all():
            cur_mri.file_type = FileType.objects.get(file_type='NIFTI')
            cur_mri.save()

        self.assertEqual(MRI.objects.filter(file_type__file_type='Converted CDK MRI').count(), 0)
        self.assertEqual(MRI.objects.filter(file_type__file_type='NIFTI').count(), 5)

        for cur_obj in original_converted_types:
            cur_obj.refresh_from_db()
            self.assertEqual(str(cur_obj.file_type), 'NIFTI')

        for cur_obj in original_nonconverted_types:
            cur_obj.refresh_from_db()
            self.assertNotEqual(str(cur_obj.file_type), 'Converted CDK MRI')

        convert_cdk_mri_filetypes.convert_cdk_mri_filetypes()

        self.assertEqual(MRI.objects.filter(file_type__file_type='Converted CDK MRI').count(), 3)
        self.assertEqual(MRI.objects.filter(file_type__file_type='NIFTI').count(), 2)

        for cur_obj in original_converted_types:
            cur_obj.refresh_from_db()
            self.assertEqual(str(cur_obj.file_type), 'Converted CDK MRI')

        for cur_obj in original_nonconverted_types:
            cur_obj.refresh_from_db()
            self.assertNotEqual(str(cur_obj.file_type), 'Converted CDK MRI')
